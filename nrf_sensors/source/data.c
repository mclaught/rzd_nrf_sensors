#include "data.h"

volatile uint8_t test_rssi[2];
uint16_t i2s_buf[MAX_BUF_CNT][FRAME_SIZE_16_MAX];
uint8_t i2s_data[2];
int16_t batt_data[BATT_ADC_CH_CNT] = {
    0
    #if (BATT_ADC_CH_CNT>1)
    ,0
    #endif
};
