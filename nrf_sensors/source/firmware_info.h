#ifndef FIRMWARE_INFO_H
#define FIRMWARE_INFO_H

#include <stdint.h>

typedef __packed struct PROGMEM
{
    uint32_t hdr;
    uint32_t hw_ver;
    uint32_t sw_ver;
}firmware_info_t;

uint32_t firmware_init();

#endif