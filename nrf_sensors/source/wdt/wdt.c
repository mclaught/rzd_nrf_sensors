#include "wdt.h"

#include "nrf52.h"
#include "nrf_wdt.h"
#include "nrf_drv_clock.h"

void wdt_init(void)
{
    nrf_drv_clock_lfclk_request(NULL);
    
    nrf_wdt_behaviour_set(NRF_WDT_BEHAVIOUR_PAUSE_SLEEP_HALT);// NRF_WDT_BEHAVIOUR_PAUSE_SLEEP_HALT  NRF_WDT_BEHAVIOUR_RUN_SLEEP <-SLEEP_TIMER
    uint32_t period = WDT_PERIOD_MSEC;
    nrf_wdt_reload_value_set(32768UL*WDT_PERIOD_MSEC/1000-1);//<-SLEEP_TIMER
    nrf_wdt_reload_request_enable(NRF_WDT_RR0);
    nrf_wdt_task_trigger(NRF_WDT_TASK_START);
}

void wdt_reinit(uint32_t period)
{
    nrf_wdt_reload_value_set(32768UL*period/1000-1);
    nrf_wdt_reload_request_set(NRF_WDT_RR0);
}

void wdt_stop()
{
    nrf_wdt_reload_request_disable(NRF_WDT_RR0);
    nrf_wdt_reload_value_set(32768UL*5-1);
    nrf_wdt_reload_request_set(NRF_WDT_RR0);
    nrf_wdt_reload_request_enable(NRF_WDT_RR0);
//    nrf_wdt_reload_request_set(NRF_WDT_RR0);
//    nrf_wdt_reload_request_disable(NRF_WDT_RR0);
}

void wdt_step(void)
{
    nrf_wdt_reload_request_set(NRF_WDT_RR0);
}
