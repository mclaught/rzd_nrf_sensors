#ifndef _WDT_H
#define _WDT_H

#include <stdint.h>

#define WDT_PERIOD_MSEC  5000  
#define WDT_PERIOD_PARK_MSEC  20000

void wdt_init(void);
void wdt_reinit(uint32_t period);
void wdt_stop();
void wdt_step(void);

#endif
