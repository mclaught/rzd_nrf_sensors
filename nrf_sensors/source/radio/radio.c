#include "radio.h"
#include "nrf.h"
#include "nrf52.h"
#include "nrf52_bitfields.h"
#include "radio_config.h"
#include "radio_prefs.h"
#include "settings/settings.h"
#include "radio/radio_rfx2411.h"

extern bool setup_mode;

radio_sensors_t beacon_tx = {
    .ble_header = {
        .pdu_header0    = 0x42,
        .length    = sizeof(radio_sensors_t)-2,
        .bluetooth_mac  = {0x1c, 0x01, 0xe9, 0x28, 0x8e, 0x61},
    },
    .beacon = {
        .length         = sizeof(bluetooth_ad_sensors_t)-1,
        .flags_ad_type  = 0xff,
        .ident          = 0x0080,
        .tag            = 0xDACE,
        .t1             = 2150
    },
//    .flags = {
//        .length         = sizeof(bluetooth_ad_flags_t)-1,
//        .flags_ad_type  = 0x1,
//        .flags          = 0x05
//    },
};

static radio_settings_t radio_settings;

void radio_init(){
    for(int i=0; i<6; i++){
        NRF_RNG->EVENTS_VALRDY = 0;
        NRF_RNG->TASKS_START = 1;
        while(NRF_RNG->EVENTS_VALRDY == 0){__NOP();}
        NRF_RNG->TASKS_STOP = 1;
        beacon_tx.ble_header.bluetooth_mac[i] = NRF_RNG->VALUE & 0xFF;
    }
    
    radio_rfx2411_init();
    radio_configure(RADIO_ADDR1, 39, sizeof(radio_sensors_t), false);
}

void radio_deinit(){
    radio_disable();
}

void radio_beacon_send(uint16_t id, uint8_t channel, double t1, uint8_t pack_n)
{
    if (!radio_is_disabled())
        return;
    
    radio_configure(RADIO_ADDR1, channel, sizeof(radio_sensors_t), false);
    
    beacon_tx.beacon.ident = 0x8000 | settings.master_id;
    beacon_tx.beacon.beacon_id = settings.beacon_id;
    beacon_tx.beacon.t1 = (int16_t)(t1*100);
    beacon_tx.beacon.pack_n = pack_n;

    
    radio_write_packet((uint8_t*)&beacon_tx);
    
//    uint32_t tm_ = get_sys_time();
//    while(!fl_send && (get_sys_time()-tm_)<10);//
}

void radio_read_settings(){
    radio_configure(RADIO_ADDR1, 39, sizeof(radio_settings_t), true);//
    radio_read_packet((uint8_t*)&radio_settings);
}

static uint16_t ntohs(uint16_t x){
    return ((x & 0xFF00) >> 8) | ((x & 0x00FF) << 8);
}

void RADIO_IRQHandler(void)
{
    
    if(NRF_RADIO->EVENTS_DISABLED)
    {
        NRF_RADIO->EVENTS_DISABLED = 0;
        NRF_RADIO->EVENTS_READY = 0;
        NRF_RADIO->EVENTS_ADDRESS = 0;

        if ( (NRF_RADIO->CRCSTATUS >> RADIO_CRCSTATUS_CRCSTATUS_Pos) == RADIO_CRCSTATUS_CRCSTATUS_CRCOk )//(NRF_RADIO->CRCSTATUS >> RADIO_CRCSTATUS_CRCSTATUS_Pos) == RADIO_CRCSTATUS_CRCSTATUS_CRCOk || __IN_STATE(mch_pairing, PAIR_SEARCH_CH)
        {
            if(radio_settings.beacon.tag == 0xACBE){
                settings.power_flag = 1;
                settings.master_id = ntohs(radio_settings.beacon.majorId);
                settings.beacon_id = ntohs(radio_settings.beacon.minorId);
                settings.excursion = radio_settings.beacon.excursion;
                settings.beacon_rssi = radio_settings.beacon.rssi;
                settings.repeat = radio_settings.beacon.repeat;
                settings.short_period = ntohs(radio_settings.beacon.short_period);
                settings.long_period = ntohs(radio_settings.beacon.long_period);
                settings.channel = radio_settings.beacon.channel;
                settings.power_level = radio_settings.beacon.power_level;
                
                write_settings();
                
                setup_mode = false;
            }else{
                radio_read_packet((uint8_t*)&radio_settings);
            }
        }
        else
        {
            radio_read_packet((uint8_t*)&radio_settings);
        }
         
    }
}
