#include "radio_config.h"

#include "nrf.h"
#include "nrf52.h"
#include "nrf52_bitfields.h"

#include "support/xcat.h"
#include "radio_prefs.h"
#include "support/static_assert.h"
#include "settings/settings.h"

#define BITFIELD_VALUE(_prefix, _value) \
    (_value << XCAT(_prefix, _Pos))

#define BITFIELD_DEFINE(_prefix, _value)                \
    BITFIELD_VALUE(_prefix, XCAT3(_prefix, _, _value))

#define PACKET0_S1_SIZE                  (0UL)  //!< S1 size in bits
#define PACKET0_S0_SIZE                  (1UL)
#define PACKET0_PAYLOAD_SIZE             (8UL)  //!< payload size in bits

#define PACKET1_BASE_ADDRESS_LENGTH      (3UL)  //!< base address length in bytes
#define PACKET1_STATIC_LENGTH            (0)  //!< static length in bytes

COMPILE_TIME_ASSERT(RADIO_PACKET_PREFIX * 8 ==
                    PACKET0_S0_SIZE * 8 +
                    PACKET0_PAYLOAD_SIZE +
                    PACKET0_S1_SIZE);
                    
#if(RADIO_MOD == 0)
void radio_configure(uint32_t addr, uint8_t channel, uint8_t len, bool intr)
{
    switch(settings.power_level){
        case 1:
            NRF_RADIO->TXPOWER = BITFIELD_VALUE(RADIO_TXPOWER_TXPOWER, RADIO_TXPOWER_TXPOWER_Neg40dBm);
            break;
        case 2:
            NRF_RADIO->TXPOWER = BITFIELD_VALUE(RADIO_TXPOWER_TXPOWER, RADIO_TXPOWER_TXPOWER_Neg20dBm);
            break;
        case 3:
            NRF_RADIO->TXPOWER = BITFIELD_VALUE(RADIO_TXPOWER_TXPOWER, RADIO_TXPOWER_TXPOWER_Neg16dBm);
            break;
        case 4:
            NRF_RADIO->TXPOWER = BITFIELD_VALUE(RADIO_TXPOWER_TXPOWER, RADIO_TXPOWER_TXPOWER_Neg12dBm);
            break;
        case 5:
            NRF_RADIO->TXPOWER = BITFIELD_VALUE(RADIO_TXPOWER_TXPOWER, RADIO_TXPOWER_TXPOWER_Neg8dBm);
            break;
        case 6:
            NRF_RADIO->TXPOWER = BITFIELD_VALUE(RADIO_TXPOWER_TXPOWER, RADIO_TXPOWER_TXPOWER_Neg4dBm);
            break;
        case 7:
            NRF_RADIO->TXPOWER = BITFIELD_VALUE(RADIO_TXPOWER_TXPOWER, RADIO_TXPOWER_TXPOWER_0dBm);
            break;
        case 8:
            NRF_RADIO->TXPOWER = BITFIELD_VALUE(RADIO_TXPOWER_TXPOWER, RADIO_TXPOWER_TXPOWER_Pos3dBm);
            break;
        case 9:
            NRF_RADIO->TXPOWER = BITFIELD_VALUE(RADIO_TXPOWER_TXPOWER, RADIO_TXPOWER_TXPOWER_Pos4dBm);
            break;
        default:
            NRF_RADIO->TXPOWER = BITFIELD_VALUE(RADIO_TXPOWER_TXPOWER, RADIO_TXPOWER_TXPOWER_Pos4dBm);
        
    }
    
    switch(channel){
        case 37:
            NRF_RADIO->FREQUENCY = 2;
            break;
        case 38:
            NRF_RADIO->FREQUENCY = 26;
            break;
        default:
            NRF_RADIO->FREQUENCY = (channel << 1) + 2;
    }
    
    NRF_RADIO->DATAWHITEIV = channel | 0x40;
    NRF_RADIO->MODE = BITFIELD_DEFINE(RADIO_MODE_MODE, RADIO_MODE);

    // Radio address config
    NRF_RADIO->PREFIX0 = addr >> 24;
    NRF_RADIO->PREFIX1 = 0x0UL;
    NRF_RADIO->BASE0   = addr << 8;
    NRF_RADIO->BASE1   = 0x0UL;

    NRF_RADIO->TXADDRESS    = 0;
    NRF_RADIO->RXADDRESSES  = 0x01;//(1 << settings.lang);

    // Packet configuration
    NRF_RADIO->PCNF0 =
            BITFIELD_VALUE(RADIO_PCNF0_S1LEN, PACKET0_S1_SIZE) |
            BITFIELD_VALUE(RADIO_PCNF0_S0LEN, PACKET0_S0_SIZE) |
            BITFIELD_VALUE(RADIO_PCNF0_LFLEN, PACKET0_PAYLOAD_SIZE);


    // Packet configuration
    NRF_RADIO->PCNF1 =
            BITFIELD_DEFINE(RADIO_PCNF1_WHITEEN, Enabled) | //Enabled
            BITFIELD_DEFINE(RADIO_PCNF1_ENDIAN,  Little) |
            BITFIELD_VALUE(RADIO_PCNF1_BALEN,    PACKET1_BASE_ADDRESS_LENGTH) |
            BITFIELD_VALUE(RADIO_PCNF1_STATLEN,  PACKET1_STATIC_LENGTH) |
            BITFIELD_VALUE(RADIO_PCNF1_MAXLEN,   len);

    // CRC Config

    // Configure CRC calculation
    NRF_RADIO->CRCCNF =
            BITFIELD_DEFINE(RADIO_CRCCNF_SKIPADDR, Skip) |
            BITFIELD_DEFINE(RADIO_CRCCNF_LEN,      Three);//RADIO_CRCCNF_LEN_Three

    NRF_RADIO->SHORTS =
            BITFIELD_DEFINE(RADIO_SHORTS_READY_START,       Enabled) |
            BITFIELD_DEFINE(RADIO_SHORTS_END_DISABLE,       Enabled) |
            BITFIELD_DEFINE(RADIO_SHORTS_DISABLED_RXEN,     Disabled) |
            BITFIELD_DEFINE(RADIO_SHORTS_DISABLED_TXEN,     Disabled) |
            BITFIELD_DEFINE(RADIO_SHORTS_ADDRESS_RSSISTART, Disabled) | //RSSI
            BITFIELD_DEFINE(RADIO_SHORTS_DISABLED_RSSISTOP, Disabled);

    NRF_RADIO->CRCINIT = 0x00555555UL;
    NRF_RADIO->CRCPOLY = 0x0000065B;
    
    if(intr){
        NRF_RADIO->INTENSET =
                BITFIELD_DEFINE(RADIO_INTENSET_DISABLED, Enabled);

        NVIC_SetPriority(RADIO_IRQn, 1);
        NVIC_ClearPendingIRQ(RADIO_IRQn);
        NVIC_EnableIRQ(RADIO_IRQn);
    }
}
#else


void radio_select_address(uint8_t addr_n){
    NRF_RADIO->TXADDRESS = addr_n;
}

static uint32_t swap_bits(uint32_t inp)
{
    uint32_t i;
    uint32_t retval = 0;

    inp = (inp & 0x000000FFUL);

    for (i = 0; i < 8; i++)
    {
        retval |= ((inp >> i) & 0x01) << (7 - i);
    }

    return retval;
}

static uint32_t bytewise_bitswap(uint32_t inp)
{
      return (swap_bits(inp >> 24) << 24)
           | (swap_bits(inp >> 16) << 16)
           | (swap_bits(inp >> 8) << 8)
           | (swap_bits(inp));
}

void radio_configure(uint32_t addr, uint8_t channel, uint8_t len)
{    
  NRF_RADIO->TXPOWER   = (RADIO_TXPOWER_TXPOWER_0dBm << RADIO_TXPOWER_TXPOWER_Pos);
  NRF_RADIO->FREQUENCY = channel;  // Frequency: 2400MHz + channel * 1MHz
  NRF_RADIO->MODE      = (RADIO_MODE_MODE_Nrf_250Kbit << RADIO_MODE_MODE_Pos);

  // Radio address config
  NRF_RADIO->PREFIX0 =
        ((uint32_t)swap_bits(0x00) << 24) // Prefix byte of address 3 converted to nRF24L series format
      | ((uint32_t)swap_bits(0x00) << 16) // Prefix byte of address 2 converted to nRF24L series format
      | ((uint32_t)swap_bits(0x00) << 8)  // Prefix byte of address 1 converted to nRF24L series format
      | ((uint32_t)swap_bits((addr & 0x000000FFUL)) << 0); // Prefix byte of address 0 converted to nRF24L series format

  NRF_RADIO->PREFIX1 =
        ((uint32_t)swap_bits(0x00) << 24) // Prefix byte of address 7 converted to nRF24L series format
      | ((uint32_t)swap_bits(0x00) << 16) // Prefix byte of address 6 converted to nRF24L series format
      | ((uint32_t)swap_bits(0x00) << 16) // Prefix byte of address 5 converted to nRF24L series format
      | ((uint32_t)swap_bits(0x00) << 0); // Prefix byte of address 4 converted to nRF24L series format
    
  NRF_RADIO->BASE0 =
            ((uint32_t)swap_bits(((addr >> 8) & 0x000000FFUL)) << 24) // Base address byte 0 converted to nRF24L series format
          | ((uint32_t)swap_bits((addr >> 16)) << 16) // Base address byte 1 converted to nRF24L series format
          | ((uint32_t)swap_bits(0x00) << 8) // Base address byte 2 converted to nRF24L series format
          | ((uint32_t)swap_bits(0x00) << 0); // Base address byte 3 converted to nRF24L series format
    
  NRF_RADIO->BASE1 = bytewise_bitswap(0x00000000UL);  // Base address for prefix 1-7 converted to nRF24L series format

  NRF_RADIO->TXADDRESS   = 0x00UL;  // Set device address 0 to use when transmitting
  NRF_RADIO->RXADDRESSES = 0x01UL;  // Enable device address 0 to use to select which addresses to receive

  // Packet configuration
  NRF_RADIO->PCNF0 = (PACKET_S1_FIELD_SIZE     << RADIO_PCNF0_S1LEN_Pos) |
                     (PACKET_S0_FIELD_SIZE     << RADIO_PCNF0_S0LEN_Pos) |
                     (PACKET_LENGTH_FIELD_SIZE << RADIO_PCNF0_LFLEN_Pos); //lint !e845 "The right argument to operator '|' is certain to be 0"

  // Packet configuration
  NRF_RADIO->PCNF1 = (RADIO_PCNF1_WHITEEN_Disabled << RADIO_PCNF1_WHITEEN_Pos) |
                     (RADIO_PCNF1_ENDIAN_Big       << RADIO_PCNF1_ENDIAN_Pos)  |
                     (PACKET_BASE_ADDRESS_LENGTH   << RADIO_PCNF1_BALEN_Pos)   |
                     (PACKET_STATIC_LENGTH         << RADIO_PCNF1_STATLEN_Pos) |
                     (len                          << RADIO_PCNF1_MAXLEN_Pos); //lint !e845 "The right argument to operator '|' is certain to be 0"

  NRF_RADIO->SHORTS =
            BITFIELD_DEFINE(RADIO_SHORTS_READY_START,       Enabled) |
            BITFIELD_DEFINE(RADIO_SHORTS_END_DISABLE,       Enabled) |
            BITFIELD_DEFINE(RADIO_SHORTS_DISABLED_RXEN,     Disabled) |
            BITFIELD_DEFINE(RADIO_SHORTS_DISABLED_TXEN,     Disabled) |
            BITFIELD_DEFINE(RADIO_SHORTS_ADDRESS_RSSISTART, Enabled) |
            BITFIELD_DEFINE(RADIO_SHORTS_DISABLED_RSSISTOP, Enabled);
      
  /*
     * nRF24LE1_Product_Specification..., p 25
     * The polynomial for 1 byte CRC is X 8 + X 2 + X + 1. Initial value 0xFF.
     * The polynomial for 2 byte CRC is X 16 + X 12 + X 5 + 1. Initial value 0xFFFF.
     *
     * 1 byte
  */
  NRF_RADIO->CRCCNF = (RADIO_CRCCNF_LEN_One << RADIO_CRCCNF_LEN_Pos); // Number of checksum bits
  NRF_RADIO->CRCINIT = 0xFFUL;   // Initial value
  NRF_RADIO->CRCPOLY = 0x107UL;  // CRC poly: x^8+x^2+x^1+1
    
  NRF_RADIO->INTENSET = BITFIELD_DEFINE(RADIO_INTENSET_DISABLED, Enabled);

  NVIC_SetPriority(RADIO_IRQn, 1);
  NVIC_ClearPendingIRQ(RADIO_IRQn);
  NVIC_EnableIRQ(RADIO_IRQn);
}
#endif

void radio_read_packet(uint8_t* packet)
{
    NRF_RADIO->EVENTS_READY = 0;
    NRF_RADIO->EVENTS_ADDRESS = 0;
    NRF_RADIO->EVENTS_PAYLOAD = 0;
    NRF_RADIO->EVENTS_END = 0;
    NRF_RADIO->EVENTS_DISABLED = 0;
    
    NRF_RADIO->PACKETPTR = (uint32_t)packet;
    NRF_RADIO->TASKS_RXEN = 1;
}

void radio_write_packet(uint8_t* packet)
{
    NRF_RADIO->PACKETPTR = (uint32_t)packet;
    NRF_RADIO->TASKS_TXEN = 1;
}

void radio_disable(void)
{
    NVIC_DisableIRQ(RADIO_IRQn);
    NRF_RADIO->TASKS_DISABLE = 1U;
    while(NRF_RADIO->EVENTS_DISABLED ==0){};
}

bool
radio_is_disabled(void)
{
    switch ((NRF_RADIO->STATE >> RADIO_STATE_STATE_Pos) & RADIO_STATE_STATE_Msk)
    {
    case RADIO_STATE_STATE_Disabled:
//    case RADIO_STATE_STATE_RxDisable:
//    case RADIO_STATE_STATE_TxDisable:
        return true;

    default:
        break;
    }
    return false;
}
