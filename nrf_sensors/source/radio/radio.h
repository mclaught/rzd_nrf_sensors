#ifndef RADIO_H
#define RADIO_H

#include <stdint.h>

typedef __packed struct {
    uint8_t pdu_header0;
    uint8_t length;
    uint8_t bluetooth_mac[6];
} ble_header_t;

typedef __packed struct {
    uint8_t length;
    uint8_t flags_ad_type;
    uint8_t name[16];
} bluetooth_ad_name_t;

typedef __packed struct {
    uint8_t length;
    uint8_t flags_ad_type;
    uint8_t flags;
} bluetooth_ad_flags_t;

typedef __packed struct {
    uint8_t  length;
    uint8_t  flags_ad_type;
    uint16_t ident;
    uint16_t tag;
//    uint8_t data[16];
    
    uint8_t excursion;
    uint8_t rssi;
    uint8_t repeat;
    uint8_t channel;
    
    uint16_t short_period;
    uint16_t long_period;
    uint8_t  power_level;
    uint8_t reserved2;
    
    uint32_t reserved3;
    uint16_t reserved4;
    
    uint16_t majorId;
    uint16_t minorId;
    uint16_t ex;
} bluetooth_ad_setts_t;

typedef __packed struct {
    uint8_t  length;
    uint8_t  flags_ad_type;
    uint16_t ident;
    uint16_t tag;
    
    uint16_t beacon_id;
    int16_t  t1; 
    uint8_t  pack_n;
} bluetooth_ad_sensors_t;

typedef __packed struct {
    ble_header_t            ble_header;
    bluetooth_ad_setts_t   beacon;
} radio_settings_t;

typedef __packed struct {
    ble_header_t            ble_header;
    bluetooth_ad_sensors_t   beacon;
//    bluetooth_ad_flags_t    flags;
} radio_sensors_t;


void radio_init();
void radio_deinit();
void radio_beacon_send(uint16_t id, uint8_t channel, double t1, uint8_t pack_n);
void radio_read_settings();

#endif