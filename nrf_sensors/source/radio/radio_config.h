#ifndef RADIO_CONFIG_H
#define RADIO_CONFIG_H

#include <stdbool.h>
#include <stdint.h>

#define RADIO_PACKET_PREFIX     2

void radio_configure(uint32_t addr, uint8_t channel, uint8_t len, bool intr);
void radio_select_address(uint8_t addr_n);

void radio_read_packet(uint8_t* packet);
void radio_write_packet(uint8_t* packet);

void radio_disable(void);
bool radio_is_disabled(void);

#endif
