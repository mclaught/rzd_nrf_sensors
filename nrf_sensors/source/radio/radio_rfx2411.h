#ifndef RADIO_RFX2411_H
#define RADIO_RFX2411_H

void radio_rfx2411_init(void);
void radio_rfx2411_deinit(void);
void radio_rfx2411_shutdown(void);
void radio_rfx2411_bypass(void);
void radio_rfx2411_tx_low_idq(void);
void radio_rfx2411_tx_high_idq(void);
void radio_rfx2411_rx_low_noise(void);
void radio_rfx2411_rx_low_current(void);
void radio_rfx2411_anta(void);
void radio_rfx2411_antb(void);

void radio_rfx2411_mode(void);
void radio_rfx2411_ant(void);

#endif /* RADIO_RFX2411_H */
