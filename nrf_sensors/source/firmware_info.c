#include "firmware_info.h"

#pragma push
#pragma O0
const firmware_info_t firmware_info = {
    .hdr =0xA5A5A5A5,
    .hw_ver = 1,
    .sw_ver = 0x20171226
};

uint32_t firmware_init()
{
    return firmware_info.hdr == 0xA5A5A5A5;
}
