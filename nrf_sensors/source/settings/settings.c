#include "nrf.h"
#include "settings/settings.h"
#include "radio_prefs.h"
#include <stdio.h>
#include "nrf_wdt.h"
#include "config.h"
#include "radio/radio.h"

#ifndef OTK_FIRM
    #error "OTK_FIRM is undefined!!!"
#endif

#define TEMP_BOOT   0x7D000
#define TEMP_BOOT_FW   (*(uint32_t*)0x7D020)
#define TEMP_BOOT_TP   (*(uint32_t*)0x7D028)
#define BOOT_SIZE   0x2000
#define BOOT_HW    (*(uint32_t*)0x0020)
#define BOOT_VER    (*(uint32_t*)0x0024)

uint32_t   pg_size;
uint32_t   pg_num;
settings_t settings;

extern radio_sensors_t beacon_tx;

void flash_bootloader(void);

static void flash_page_erase(uint32_t * page_address)
{
    // Turn on flash erase enable and wait until the NVMC is ready:
    NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Een << NVMC_CONFIG_WEN_Pos);

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }

    // Erase page:
    NRF_NVMC->ERASEPAGE = (uint32_t)page_address;

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }

    // Turn off flash erase enable and wait until the NVMC is ready:
    NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Ren << NVMC_CONFIG_WEN_Pos);

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }
}

static void flash_word_write(uint32_t * address, uint32_t value)
{
    // Turn on flash write enable and wait until the NVMC is ready:
    NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Wen << NVMC_CONFIG_WEN_Pos);

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }

    *address = value;

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }

    // Turn off flash write enable and wait until the NVMC is ready:
    NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Ren << NVMC_CONFIG_WEN_Pos);

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }
}

static uint32_t rnd_get_uint32(void)
{
    NRF_RNG->SHORTS = RNG_SHORTS_VALRDY_STOP_Enabled << RNG_SHORTS_VALRDY_STOP_Pos;
    NRF_RNG->TASKS_START = 1;
    while (NRF_RNG->EVENTS_VALRDY == 0)
      __WFE();
    NRF_RNG->EVENTS_VALRDY = 0;
    NVIC_ClearPendingIRQ(RNG_IRQn);

    return NRF_RNG->VALUE;
}

static uint8_t rnd_get_uint8(uint8_t a, uint8_t b)
{
    uint8_t v = rnd_get_uint32();
    while (v < a || v > b)
        v = rnd_get_uint32();
    return v;
}

void setts_init(void)
{
    pg_size = NRF_FICR->CODEPAGESIZE;
    pg_num = NRF_FICR->CODESIZE - 1;
    settings = *(settings_t*)(pg_size * pg_num);
    
    if(settings.power_flag == 0xFF){
        settings.power_flag = 1;
        settings.master_id = 111;
        settings.beacon_id = 1;
        settings.repeat = 3;
        settings.short_period = 5;
        settings.long_period = 500;
        settings.power_level = 9;
        
        write_settings();
    }
    
//    uint8_t mac1[4];
//    uint8_t mac2[4];
//    *(uint32_t*)mac1 = NRF_FICR->DEVICEID[0];
//    *(uint32_t*)mac2 = NRF_FICR->DEVICEID[1];
//    for(int i=0;i<4;i++)
//        beacon_tx.ble_header.bluetooth_mac[i] = mac1[i];
//    for(int i=0;i<2;i++)
//        beacon_tx.ble_header.bluetooth_mac[i+4] = mac2[i];
    
    flash_bootloader();
    
}

uint32_t* ptr_tmp;
uint32_t* ptr_setts;
int32_t cnt;

void write_settings(void)
{
    flash_page_erase((uint32_t*)(pg_size * pg_num));
    
    ptr_tmp = (uint32_t*)&settings;
    ptr_setts = (uint32_t*)(pg_size * pg_num);
    cnt = sizeof(settings_t);
    while(cnt > 0)
    {
        flash_word_write(ptr_setts, *ptr_tmp);
        ptr_tmp++;
        ptr_setts++;
        cnt -= 4;
    }
}


static uint32_t from_addr;
static uint32_t to_addr;
static uint32_t to_pg;
void flash_bootloader(void)
{
    #if (WITH_BOOT == 1)
    uint32_t new_boot_hw = *(uint32_t*)&boot_dump[0x20];
    uint32_t new_boot_ver = *(uint32_t*)&boot_dump[0x24];
    #if (FORCE_BOOT==0)
    if(new_boot_hw != BOOT_HW || new_boot_ver == BOOT_VER)
        return;
    #else
    if(new_boot_ver == BOOT_VER)
        return;
    #endif
    
    for(int addr=0; addr<boot_size; addr+=4)
    {
        if((addr % 0x1000) == 0)
            flash_page_erase((uint32_t*)addr);
        flash_word_write((uint32_t*)addr, *(uint32_t*)&boot_dump[addr]);
        nrf_wdt_reload_request_set(NRF_WDT_RR0);
    }
    #endif
}

void settings_step(void){
    settings_t* perm_setts = (settings_t*)(pg_size * pg_num);
}