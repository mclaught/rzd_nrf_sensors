#ifndef _SETTINGS_H
#define _SETTINGS_H

#include <stdint.h>
#include "config.h"

typedef __packed struct
{
    uint8_t     power_flag;
    uint16_t    master_id;
    uint8_t     excursion;
    uint16_t    beacon_id;
    uint8_t     beacon_command;
    uint8_t     beacon_rssi;
    uint8_t     repeat;
    uint16_t    short_period;
    uint16_t    long_period;
    uint8_t     channel;
    uint8_t     power_level;
}settings_t;

void setts_init(void);
void write_settings(void);
void settings_step(void);

extern settings_t settings;

#endif
