#ifndef POWER_H
#define POWER_H

#include <stdint.h>

#define POWER_BIG_TIMEOUT (20000)//(5*60000)
#define POWER_SMALL_TIMEOUT (20000)//(1*60000)//
#define POWER_NF_TIMEOUT  5000
#define POWER_SMALL_TIMEOUT_TSH 10000
#define NO_JACK_TIMEOUT 30000
#define POWER_WAKE_PERIOD 5
#define POWER_PARK_PERIOD 30

void oscillator_init(void);
void system_off();
void power_off();
void power_on(void);
void detect_poweroff(void);
void power_check(void);
void power_step(void);
void power_system_reset(void);
void power_max_shdn();

//extern uint8_t power_is_on;
extern uint8_t fl_power_on;

#endif
