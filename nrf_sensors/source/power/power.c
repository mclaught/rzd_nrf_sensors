#include "power/power.h"

#include "nrf52.h"
#include "nrf_gpio.h"
#include "nrf_gpiote.h"
#include "nrf_drv_twi.h"
#include "nrf_drv_clock.h"
#include "gpio/gpio.h"
#include "board_pin.h"
#include "keyboard/keyboard.h"
#include "system/system_timer.h"
#include "radio/radio_rfx2411.h"
#include "wdt/wdt.h"
#include "battery/battery.h"
#include "gpio/gpio.h"
#include "settings/settings.h"

#ifndef OTK_FIRM
    #error "OTK_FIRM is undefined!!!"
#endif

static uint32_t tm_power_last_check = 0;
static uint32_t tm_jack_last_check = 0;
uint8_t fl_power_on = 0;

void oscillator_init(void)
{
    /* Start 16 MHz crystal oscillator */
    NRF_CLOCK->EVENTS_HFCLKSTARTED = 0;
    NRF_CLOCK->TASKS_HFCLKSTART = 1;

    /* Wait for the external oscillator to start up */
    while (NRF_CLOCK->EVENTS_HFCLKSTARTED == 0)
    {
    }
}



void system_off()
{
    //gpio_led_red_off();
    //gpio_led_green_off();
    
    //lis2hh12_twi_deinit();
    
    //__disable_irq();
    //wdt_init();
    
    //wdt_stop();
    
    gpio_uninit(); 
    battery_deinit(); 
    //keyboard_uninit();
    radio_deinit(); 
    radio_rfx2411_shutdown();
    radio_rfx2411_deinit();
   
    system_timer_deinit(); 
    
    nrf_gpio_cfg_input(PIN_BTN1, NRF_GPIO_PIN_PULLUP);
    nrf_gpio_cfg_sense_input(PIN_BTN1,
                             NRF_GPIO_PIN_PULLUP,
                             NRF_GPIO_PIN_SENSE_LOW);
    nrf_gpio_cfg_input(PIN_BTN2, NRF_GPIO_PIN_PULLUP);
    nrf_gpio_cfg_sense_input(PIN_BTN2,
                             NRF_GPIO_PIN_PULLUP,
                             NRF_GPIO_PIN_SENSE_LOW); 
    #ifdef PIN_BTN3
    nrf_gpio_cfg_input(PIN_BTN3, NRF_GPIO_PIN_PULLUP);
    nrf_gpio_cfg_sense_input(PIN_BTN3,
                             NRF_GPIO_PIN_PULLUP,
                             NRF_GPIO_PIN_SENSE_LOW); 
    #endif
    
    #ifdef PIN_CH_DET
    if(battery_charge_state() > BAT_NORMAL)//
    {
        nrf_gpio_cfg_output(PIN_LED_GREEN);
        gpio_led_green_on();
        
        #if (PIN_CH_DET_PULL == 1)
        nrf_gpio_cfg_input(PIN_CH_DET, NRF_GPIO_PIN_PULLUP);
        nrf_gpio_cfg_sense_input(PIN_CH_DET,
                                 NRF_GPIO_PIN_PULLUP,
                                 NRF_GPIO_PIN_SENSE_LOW);
        #else
        nrf_gpio_cfg_input(PIN_CH_DET, NRF_GPIO_PIN_NOPULL);
        nrf_gpio_cfg_sense_input(PIN_CH_DET,
                                 NRF_GPIO_PIN_NOPULL,
                                 NRF_GPIO_PIN_SENSE_LOW);
        #endif
    }
    else
    {
        #if (PIN_CH_DET_PULL == 1)
        nrf_gpio_cfg_input(PIN_CH_DET, NRF_GPIO_PIN_PULLUP);
        nrf_gpio_cfg_sense_input(PIN_CH_DET,
                                 NRF_GPIO_PIN_PULLUP,
                                 NRF_GPIO_PIN_SENSE_HIGH);
        #else
        nrf_gpio_cfg_input(PIN_CH_DET, NRF_GPIO_PIN_NOPULL);
        nrf_gpio_cfg_sense_input(PIN_CH_DET,
                                 NRF_GPIO_PIN_NOPULL,
                                 NRF_GPIO_PIN_SENSE_HIGH);
        #endif
    }
    #else
    #endif
    
    
            
//    #ifdef PIN_INT_ACCEL2
//    nrf_gpio_cfg_input(PIN_INT_ACCEL2, NRF_GPIO_PIN_NOPULL);
//    nrf_gpio_cfg_sense_input(PIN_INT_ACCEL2,
//                             NRF_GPIO_PIN_NOPULL,
//                             NRF_GPIO_PIN_SENSE_HIGH);
//    #endif //PIN_INT_ACCEL2
    

    //nrf_drv_clock_uninit();
    
    while(1) {
        wdt_step();
        NRF_POWER->SYSTEMOFF = 1;
        __NOP();
    };
}
#if (USE_SHDN==1)
void power_off(uint8_t max_shdn){
    system_off(max_shdn);
}
#else //!USE_SHDN
void power_off()
{
    gpio_led_green_off();
    gpio_led_red_off();
    gpio_uninit(); 
    
//    keyboard_uninit();
    
//    batt_charge_state_t bt_ch_st = battery_charge_state();
//    if(bt_ch_st == BAT_CHARGING){
//        nrf_gpio_cfg_output(PIN_LED_GREEN);
//        gpio_led_green_on();
//    }else if(bt_ch_st == BAT_CHARGED){
//        nrf_gpio_cfg_output(PIN_LED_GREEN);
//        gpio_led_green_on();
//        system_timer_wait(10);
//        gpio_led_green_off();
//    }
    
    battery_deinit(); 
    radio_deinit();
    radio_rfx2411_shutdown();
    radio_rfx2411_deinit();

    NVIC_EnableIRQ(GPIOTE_IRQn);
    nrf_gpiote_event_configure(0, PIN_BTN1, NRF_GPIOTE_POLARITY_HITOLO);
    nrf_gpiote_event_configure(1, PIN_BTN2, NRF_GPIOTE_POLARITY_HITOLO);
//    #ifdef PIN_BTN3
//    nrf_gpiote_event_configure(3, PIN_BTN3, NRF_GPIOTE_POLARITY_HITOLO);
//    #endif

    nrf_gpiote_int_enable(
        (GPIOTE_INTENSET_IN0_Enabled << GPIOTE_INTENSET_IN0_Pos) | 
        (GPIOTE_INTENSET_IN1_Enabled << GPIOTE_INTENSET_IN1_Pos)// | 
//        (GPIOTE_INTENSET_IN2_Enabled << GPIOTE_INTENSET_IN2_Pos) | 
//        (GPIOTE_INTENSET_IN3_Enabled << GPIOTE_INTENSET_IN3_Pos) | 
//        (GPIOTE_INTENSET_IN4_Enabled << GPIOTE_INTENSET_IN4_Pos)
    );
    nrf_gpiote_event_enable(0);
    nrf_gpiote_event_enable(1);
//    nrf_gpiote_event_enable(2);
//    nrf_gpiote_event_enable(3);
//    nrf_gpiote_event_enable(4);
    
    NRF_POWER->RESETREAS = 0xF000F;
    
    system_timer_deinit(); 
    
    NRF_RTC1->TASKS_STOP = 1;

    NRF_CLOCK->TASKS_HFCLKSTOP = 1;
    nrf_drv_clock_uninit();
    
    uint32_t period = settings.long_period;
    
    NRF_TIMER0->MODE = TIMER_MODE_MODE_Timer << TIMER_MODE_MODE_Pos; // <- SLEEP_TIMER
    NRF_TIMER0->BITMODE = TIMER_BITMODE_BITMODE_32Bit << TIMER_BITMODE_BITMODE_Pos;
    NRF_TIMER0->PRESCALER = 9 << TIMER_PRESCALER_PRESCALER_Pos;
    NRF_TIMER0->SHORTS = TIMER_SHORTS_COMPARE0_CLEAR_Enabled << TIMER_SHORTS_COMPARE0_CLEAR_Pos;
    NRF_TIMER0->INTENSET = TIMER_INTENSET_COMPARE0_Set << TIMER_INTENSET_COMPARE0_Pos;
    NRF_TIMER0->CC[0] = 31250*period/1000 << TIMER_CC_CC_Pos;
    NRF_TIMER0->TASKS_START = 1;
    NVIC_EnableIRQ(TIMER0_IRQn);

    while(1) {
        NRF_POWER->TASKS_LOWPWR = 1;
        __WFE();
        __SEV();
//        wdt_step();
        __WFE();
//        wdt_step();
        __NOP();
    };
}
#endif

void power_on(void)
{
    radio_init();
    gpio_init();
    keyboard_init();
    battery_init();
    
}

static uint8_t p_on_fl = 0;
void detect_poweroff(void)
{
    if(settings.power_flag)
        return;
    
//    MAX9867_PACKET(0x00, status, 0);
//    MAX9867_REG_GET(status);
//    
//    MAX9867_PACKET(0x01, jack, 0);
//    MAX9867_REG_GET(jack);
    #if (OTK_FIRM == 1)
        if(!otk_complete())
            return;
        
        if(button_pressed(0))
            return;
        if(button_pressed(1))
            return;
        #ifdef PIN_BTN3
        if(button_pressed(2))
            return;
        #endif
        
        power_off(false);
        return;
    #endif
    
    #ifdef PIN_JACK
    nrf_gpio_cfg_input(PIN_JACK, NRF_GPIO_PIN_NOPULL);
//    if((NRF_POWER->RESETREAS & 2)==0 && gpio_jack())//status.fields.value.JDET && jack.fields.value.JKSNS;
//        p_on_fl |= (1<<0);
    #endif
    
    if(button_pressed(0))
        p_on_fl |= (1<<1);
    if(button_pressed(1))
        p_on_fl |= (1<<2);
    #ifdef PIN_BTN3
    if(button_pressed(2))
        p_on_fl |= (1<<3);
    #endif
    
    if(p_on_fl == 0)// && battery_charge_state()==BAT_NOCHARGE
    {
        fl_power_on = false;
        uint32_t tm_start = system_timer_value;
        while( (system_timer_diff(tm_start) < 100 || !batt_control_is_ok() ) && !fl_power_on){
            wdt_step();
            //radio_step();
            battery_step();
        }
        
        
        radio_disable();
        
        if(!fl_power_on){
            power_off(false);
        }
    }
    
    if(settings.power_flag == 0){
        settings.power_flag = 1;
        write_settings();
    }
}

void power_check(void)
{
    tm_power_last_check = system_timer_value;
}

void power_step(void)
{
    bool is_jack = gpio_jack();
    if(is_jack)
        tm_jack_last_check = system_timer_value;
    
    uint32_t d_tm_radio = system_timer_diff(tm_power_last_check);
    uint32_t d_tm_jack = system_timer_diff(tm_jack_last_check);
    
    static uint32_t cur_timeout = POWER_BIG_TIMEOUT;
    if(tm_power_last_check < POWER_SMALL_TIMEOUT_TSH)
        cur_timeout = POWER_SMALL_TIMEOUT;
    if(!is_jack)
        cur_timeout = NO_JACK_TIMEOUT;
    
    #if (OTK_FIRM == 0)
    uint8_t do_off = (d_tm_radio >= cur_timeout);//power_is_on &&  && battery_charge_state()==BAT_NOCHARGE
    #else
    uint8_t do_off = system_timer_value >= 60000;//(system_timer_value >= 30000);//
    #endif
    
    if(do_off)
        power_off(true);
    
//    if(fl_power_on && !power_is_on)
//        power_on();
}

void power_system_reset(void)
{
    SCB->AIRCR = (0x5FA<<SCB_AIRCR_VECTKEY_Pos)|SCB_AIRCR_SYSRESETREQ_Msk; //System reset
}

void GPIOTE_IRQHandler(void)
{
//    power_on();
    if(NRF_GPIOTE->EVENTS_IN[4]){
        NRF_GPIOTE->EVENTS_IN[4] = 0;
        settings.power_flag = 1;
        write_settings();
    }
    
    NRF_POWER->RESETREAS = POWER_RESETREAS_DOG_Msk | POWER_RESETREAS_SREQ_Msk;
    power_system_reset();
    //NVIC_SystemReset();
}

void TIMER0_IRQHandler(void){
    if(NRF_TIMER0->EVENTS_COMPARE[0]){
        NRF_TIMER0->EVENTS_COMPARE[0] = 0;
        NVIC_SystemReset();
    }
}
