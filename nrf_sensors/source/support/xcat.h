#ifndef SUPPORT_XCAT_H
#define SUPPORT_XCAT_H

#define XCAT_(a, b) a##b
#define XCAT(a, b) XCAT_(a,b)
#define XCAT3(a, b, c) XCAT(XCAT(a, b), c)
#define XCAT4(a, b, c, d) XCAT(XCAT(a, b), XCAT(c, d))
#define XCAT5(a, b, c, d, e) XCAT3(XCAT(a, b), XCAT(c, d), e)

#endif /* SUPPORT_XCAT_H */
