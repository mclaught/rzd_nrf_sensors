#ifndef SUPPORT_CONVERT_H
#define SUPPORT_CONVERT_H

#define SWAP_UINT16(x) (((x & 0xff) << 8) | ((x & 0xff00) >> 8))

#endif /* SUPPORT_CONVERT_H */
