#ifndef GPIO_H
#define GPIO_H

#include <stdint.h>
#include <stdbool.h>

void gpio_init(void);
void gpio_uninit(void);

void gpio_leds_touch(uint32_t delta);

void gpio_led_green_on(void);
void gpio_led_green_off(void);
void gpio_led_red_on(void);
void gpio_led_red_off(void);

void gpio_led1_on(void);
void gpio_led1_off(void);
void gpio_led2_on(void);
void gpio_led2_off(void);
void gpio_led3_on(void);
void gpio_led3_off(void);
void gpio_led4_on(void);
void gpio_led4_off(void);
void gpio_led5_on(void);
void gpio_led5_off(void);

uint8_t gpio_jack();

void gpio_led_multi_on(void);
void gpio_led_multi_off(void);

void gpio_step(void);


#endif /* GPIO_H */
