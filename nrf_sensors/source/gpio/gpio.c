#include "gpio.h"
#include "nrf_gpio.h"
#include "board_pin.h"
#include "system/system_timer.h"
#include "radio/radio.h"
#include "battery/battery.h"
#include "state_machine.h"
#include "settings/settings.h"
#include "keyboard/keyboard.h"
#include "config.h"

#ifndef OTK_FIRM
    #error "OTK_FIRM is undefined!!!"
#endif

TIMER_TASK(tsk_radio_play);
__STATE(JACK_START);
__STATE(JACK_ON);
__STATE(JACK_READY);
__STATE(JACK_OFF);
__MACHINE(mch_jack, JACK_START);
TIMER_TASK(tsk_jack);
static uint32_t gpio_leds_tm = 15000;

void gpio_init(void)
{
    nrf_gpio_cfg_output(PIN_LED_GREEN);
    nrf_gpio_cfg_output(PIN_LED_RED);
    #ifdef PIN_LED_3
    nrf_gpio_cfg_output(PIN_LED_3);
    nrf_gpio_cfg_output(PIN_LED_4);
    nrf_gpio_cfg_output(PIN_LED_5);
    #endif
    #ifdef PIN_JACK
    nrf_gpio_cfg_input(PIN_JACK, NRF_GPIO_PIN_NOPULL);
    #endif

    #if (BOARD == 0)
    nrf_gpio_cfg_output(PIN_LED1);
    nrf_gpio_cfg_output(PIN_LED2);
    nrf_gpio_cfg_output(PIN_LED3);
    nrf_gpio_cfg_output(PIN_LED4);
    nrf_gpio_cfg_output(PIN_LED5);
    nrf_gpio_cfg_output(PIN_LED_MULTI);
    #endif
    
//    nrf_gpio_cfg_input(PIN_IRQ_CODEC, NRF_GPIO_PIN_PULLDOWN);
//    nrf_gpio_cfg_input(PIN_INT_ACCEL1, NRF_GPIO_PIN_PULLDOWN);
//    nrf_gpio_cfg_input(PIN_INT_ACCEL2, NRF_GPIO_PIN_PULLDOWN);

    gpio_led1_off();
    gpio_led2_off();
    gpio_led3_off();
    gpio_led4_off();
    gpio_led5_off();
    gpio_led_green_off();
    gpio_led_red_off();
    gpio_led_multi_off();
    
    TIMER_TASK_START(tsk_radio_play, 30000);
}

void gpio_uninit(void)
{
    nrf_gpio_cfg_input(PIN_LED_GREEN, NRF_GPIO_PIN_PULLDOWN);
    nrf_gpio_cfg_input(PIN_LED_RED, NRF_GPIO_PIN_PULLDOWN);
    #ifdef PIN_LED_3
    nrf_gpio_cfg_input(PIN_LED_3, NRF_GPIO_PIN_PULLDOWN);
    nrf_gpio_cfg_input(PIN_LED_4, NRF_GPIO_PIN_PULLDOWN);
    nrf_gpio_cfg_input(PIN_LED_5, NRF_GPIO_PIN_PULLDOWN);
    #endif
    #ifdef PIN_JACK
    nrf_gpio_cfg_input(PIN_JACK, NRF_GPIO_PIN_NOPULL);
    #endif
}

void gpio_leds_touch(uint32_t delta){
    gpio_leds_tm = system_timer_value + delta;
}

void gpio_led_green_on(void)
{
    nrf_gpio_pin_set(PIN_LED_GREEN);
}

void
gpio_led_green_off(void)
{
    nrf_gpio_pin_clear(PIN_LED_GREEN);
}

void
gpio_led_red_on(void)
{
    nrf_gpio_pin_set(PIN_LED_RED);
}

void
gpio_led_red_off(void)
{
    nrf_gpio_pin_clear(PIN_LED_RED);
}

void
gpio_led1_on(void)
{
    #if (BOARD == 0)
    nrf_gpio_pin_set(PIN_LED1);
    #endif
}

void
gpio_led1_off(void)
{
    #if (BOARD == 0)
    nrf_gpio_pin_clear(PIN_LED1);
    #endif
}

void
gpio_led2_on(void)
{
    #if (BOARD == 0)
    nrf_gpio_pin_set(PIN_LED2);
    #endif
}

void
gpio_led2_off(void)
{
    #if (BOARD == 0)
    nrf_gpio_pin_clear(PIN_LED2);
    #endif
}

void
gpio_led3_on(void)
{
    #if (BOARD == 0)
    nrf_gpio_pin_set(PIN_LED3);
    #endif
}

void
gpio_led3_off(void)
{
    #if (BOARD == 0)
    nrf_gpio_pin_clear(PIN_LED3);
    #endif
}

void
gpio_led4_on(void)
{
    #if (BOARD == 0)
    nrf_gpio_pin_set(PIN_LED4);
    #endif
}

void
gpio_led4_off(void)
{
    #if (BOARD == 0)
    nrf_gpio_pin_clear(PIN_LED4);
    #endif
}

void
gpio_led5_on(void)
{
    #if (BOARD == 0)
    nrf_gpio_pin_set(PIN_LED5);
    #endif
}

void
gpio_led5_off(void)
{
    #if (BOARD == 0)
    nrf_gpio_pin_clear(PIN_LED5);
    #endif
}

void
gpio_led_multi_on(void)
{
    #if (BOARD == 0)
    nrf_gpio_pin_set(PIN_LED_MULTI);
    #endif
}

void
gpio_led_multi_off(void)
{
    #if (BOARD == 0)
    nrf_gpio_pin_clear(PIN_LED_MULTI);
    #endif
}

uint8_t gpio_jack()
{
#ifdef PIN_JACK
    #ifdef PIN_JACK_POS
    return nrf_gpio_pin_read(PIN_JACK);
    #else
    return !nrf_gpio_pin_read(PIN_JACK);
    #endif
#else
    #ifdef JDET_BY_MAX
    if(max9867_is_init() && max9867_ctrl_is_on())
        return max9867_ctrl_get_jkmic();//
    else
        return true;
    #else
    return true;
    #endif
#endif
}

__STATE(JACK_START)
{
    if(gpio_jack())
        __SET_STATE(mch_jack, JACK_ON);
    else
        __SET_STATE(mch_jack, JACK_OFF);
}

__STATE(JACK_ON)
{
    if(gpio_jack())
        TIMER_TASK_START(tsk_jack, 300);
    
    if(TIMER_TASK_CHECK(tsk_jack))
        __SET_STATE(mch_jack, JACK_READY);
}

__STATE(JACK_READY)
{
    if(!gpio_jack())
        TIMER_TASK_START(tsk_jack, 300);
    
    if(TIMER_TASK_CHECK(tsk_jack))
    {
        __SET_STATE(mch_jack, JACK_ON);
    }    
    
    if(__TIME_IN(mch_jack) > 5000)
    {
        __SET_STATE(mch_jack, JACK_OFF);
    }
}

__STATE(JACK_OFF)
{
    if(!gpio_jack())
        TIMER_TASK_START(tsk_jack, 300);
    
    if(TIMER_TASK_CHECK(tsk_jack))
        __SET_STATE(mch_jack, JACK_ON);
}

static void set_batt_led(uint32_t pin, int lvl){
    if(lvl < 0)
        lvl = 0;
    if(lvl > 30)
        lvl = 30;
    
    if((system_timer_value % 30) > lvl)
        nrf_gpio_pin_clear(pin);
    else
        nrf_gpio_pin_set(pin);
}

void gpio_step(void)
{
    
    batt_charge_state_t ch_state = battery_charge_state();
    
    uint8_t led_state = 0;
    
    
    if(((ch_state == BAT_CHARGED && (system_timer_value % 1000)<=10)) || ch_state == BAT_CHARGING){//system_timer_value <= 2000
        led_state = 2;
    }
    
    
    switch(led_state){
        case 0:
            gpio_led_red_off();
            gpio_led_green_off();
            break;
        case 1:
            gpio_led_red_on();
            gpio_led_green_off();
            break;
        case 2:
            gpio_led_red_off();
            gpio_led_green_on();
            break;
    }
    
//    uint8_t lvl = battery_level();
//    set_batt_led(PIN_LED_GREEN, lvl-30);
    
    __PROCESS_MACHINE(mch_jack);
}
