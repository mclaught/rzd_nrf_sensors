#include "bsp.h"
#include "nrf52.h"
#include "nrf_clock.h"
#include "nrf_drv_clock.h"

#include "board_pin.h"
//#include "radio/radio.h"
#include "system/system_timer.h"
#include "power/power.h"
#include "wdt/wdt.h"
#include "radio/radio_rfx2411.h"
#include "radio/radio.h"
#include "radio_prefs.h"
#include "battery/battery.h"
#include "state_machine.h"
#include "firmware_info.h"
#include "settings/settings.h"
#include "keyboard/keyboard.h"
#include "gpio/gpio.h"
#include "state_machine.h"
#include "sensors/sensors.h"

bool setup_mode = false;
int __cnt = 0;

int main(void)
{
    
    ret_code_t err_code;

    SystemInit();  
	
    err_code = nrf_drv_clock_init();
    APP_ERROR_CHECK(err_code);
    
    firmware_init();
    
    oscillator_init();
    
    #ifdef REGULATOR
        #if (REGULATOR == 1)
        NRF_POWER->DCDCEN = 1;
        #endif
    #else
        #error "REGULATOR not defined"
    #endif
    
    system_timer_init();
    
    setts_init();
    
    keyboard_init();
//    battery_preinit();
//    battery_init();
    
    radio_init();
    
    gpio_init();
    gpio_led_red_on();
    

    NRF_POWER->RESETREAS = 0xF000F;//POWER_RESETREAS_SREQ_Msk | POWER_RESETREAS_DOG_Msk
    //NRF_POWER->GPREGRET = 0;
    
    /* Activate deep sleep mode */
    SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;
    
    while(1){
        double t = get_cpu_temp();
        uint8_t channel = 37;
        
        NRF_RNG->EVENTS_VALRDY = 0;
        NRF_RNG->TASKS_START = 1;
        while(NRF_RNG->EVENTS_VALRDY == 0){__NOP();}
        NRF_RNG->TASKS_STOP = 1;
        uint8_t pack_n = NRF_RNG->VALUE & 0xFF;
        
        radio_rfx2411_tx_low_idq();
        
        for(int i=0; i<settings.repeat; i++){//while(1)
            radio_beacon_send(settings.beacon_id, channel, t, pack_n);
            
            while(system_timer_value < 1000 && NRF_RADIO->EVENTS_DISABLED == 0);
            
            system_timer_wait(2);//settings.short_period
            
            channel++;
            if(channel > 39){
                channel = 37;
                __cnt++;
            }
        }
        
        radio_rfx2411_bypass();
        
        keyboard_step();
        
        if(setup_mode){
            gpio_led_green_on();
            gpio_led_red_off();
            
            radio_deinit(); 
            
            radio_rfx2411_rx_low_noise();
            system_timer_wait(500);
            radio_read_settings();
            while(setup_mode){
                gpio_led_green_on();
                keyboard_step();
            }
            radio_rfx2411_bypass();
            gpio_led_green_off();
        }

        if(settings.long_period){
            if(settings.power_flag)
                power_off();
            else
                system_off();
        }
        
        wdt_step();
    }
}

void HardFault_Handler(void){
    //NVIC_SystemReset();
    while(1);
}