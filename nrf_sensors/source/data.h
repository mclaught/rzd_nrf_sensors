#ifndef DATA_H
#define DATA_H

#include "opus_prefs.h"
#include <stdint.h>
#include "board_pin.h"
//#include "nrf_drv_config.h"
//#include "nrf_drv_saadc.h"

#define RX_I2S_BUF_CNT 6   //RX
#define TX_I2S_BUF_CNT 2  //TX

#if (RX_I2S_BUF_CNT > TX_I2S_BUF_CNT)
    #define MAX_BUF_CNT RX_I2S_BUF_CNT
#else    
    #define MAX_BUF_CNT TX_I2S_BUF_CNT
#endif    

extern volatile uint8_t test_rssi[2];
extern uint16_t i2s_buf[MAX_BUF_CNT][FRAME_SIZE_16_MAX];
extern uint8_t i2s_data[2];
extern int16_t batt_data[BATT_ADC_CH_CNT];

#endif
