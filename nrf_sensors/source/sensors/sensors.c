#include "sensors.h"
#include "nrf.h"
#include "nrf52.h"
#include "nrf52_bitfields.h"
#include "system/system_timer.h"

double get_cpu_temp(){
    NRF_TEMP->TASKS_START = 1;
    
    uint32_t tm_start = system_timer_value;
    while(!NRF_TEMP->EVENTS_DATARDY && system_timer_diff(tm_start)<1000){
        __NOP();
    }
    
    return (double)NRF_TEMP->TEMP/4;
}