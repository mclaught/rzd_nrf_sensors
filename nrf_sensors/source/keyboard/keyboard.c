#include "nrf_gpio.h"
#include "nrf52.h"
#include "board_pin.h"
#include "keyboard.h"
#include "system/system_timer.h"
#include "gpio/gpio.h"
#include "power/power.h"
#include "nordic_common.h"
#include "radio/radio.h"
#include "radio/radio_rfx2411.h"
#include "radio_prefs.h"
#include "state_machine.h"
#include "settings/settings.h"
#include "battery/battery.h"
#include "system/system_timer.h"
#include "config.h"

#include <string.h>

#ifndef OTK_FIRM
    #error "OTK_FIRM is undefined!!!"
#endif

#define MIN_VOL -31
#define MAX_VOL 39
#define VOL_STEP 2

extern bool setup_mode;

system_time_t tm_buttons_check = 0;
uint32_t btn_pin[BTN_CNT] = {
    PIN_BTN1,
    PIN_BTN2,
    #if (BTN_CNT==3)
    PIN_BTN3
    #endif
};
uint8_t last_btn_state = 0;
system_time_t btn_tm = 0;
uint8_t btn_state = 0;
oper_mode_t oper_mode = OM_RX;

__EXT_MACHINE(mch_main);
__STATE(MAIN_RX);
__STATE(MAIN_TX);


void keyboard_init(void)
{
    for(uint8_t i=0; i<BTN_CNT; i++)
    {
        nrf_gpio_cfg_input(btn_pin[i], NRF_GPIO_PIN_PULLUP);
    }
    //pnrf_gpio_cfg_input(PIN_JACK, NRF_GPIO_PIN_NOPULL);
    
//    #if (OTK_FIRM == 1 && BOARD == 1837)
//    if( ((settings_t*)0x7F000)->otk_flag == 0xFF || (((settings_t*)0x7F000)->otk_flag_ex & (1<<OTK_I2S_RX)) == 0) //
//        oper_mode = OM_TX;
//    #endif
    
    #if (BTN_CNT==3)
    if(button_pressed(2))
    {
        #if (OTK_FIRM != 1)
        last_btn_state = BTN3;
        #endif
    }
    #endif
        
    last_btn_state = 0;
    for(uint8_t i=0; i<BTN_CNT; i++)
    {
        if(button_pressed(i))
            last_btn_state |= (1<<i);
    }
}

void keyboard_uninit(void)
{
    for(uint8_t i=0; i<BTN_CNT; i++)
    {
        nrf_gpio_cfg_input(btn_pin[i], NRF_GPIO_PIN_PULLDOWN);
    }
}

//uint8_t btns;
//uint8_t lng;
//void keyboard_event(uint8_t buttons, uint8_t event)
//{
//    if(event == BTN_DOWN)
//    {
//        if(buttons == BTN1)
//        {
//            if(RADIO_PLAY)
//            {
//                if(volume < MAX_VOL)
//                    volume++;
//                set_volume(volume);
//            }
//            else
//            {
//                power_system_reset();
//            }
//            return;
//        }
//        if(buttons == BTN2)
//        {
//            if(volume > MIN_VOL)
//                volume--;
//            set_volume(volume);
//            return;
//        }
//    }
//    
//    #if (BTN_CNT==3)
//    if(buttons == BTN3)
//    {
//        if(event == BTN_DOWN)
//        {
//            power_max_shdn();
//            power_system_reset();
////            radio_deinit();
////            radio_init_tx(settings.channel_tx);
////            
////            opus_deinit_dec();
////            opus_init_enc();

////            max9867_setmode(MM_TX);
////            
////            __SET_STATE(mch_main, MAIN_TX);
//        }
//        
//        if(event == BTN_UP)
//        {
//            power_system_reset();
////            radio_deinit();
////            radio_init_rx(settings.channel_rx1);
////            
////            opus_deinit_enc();
////            opus_init_dec();
////            
////            max9867_setmode(MM_RX);
////            
////            __SET_STATE(mch_main, MAIN_RX);
//        }
//    }
//    #endif
//    
//    if(event == BTN_LONG && (buttons == (BTN1|BTN2)))
//    {
//        __SET_STATE(mch_pairing, PAIR_INIT);
//        __SET_STATE(mch_info, RINF_IDLE);
//    }
//}

uint8_t button_pressed(uint8_t btn)
{
    //TMP2
//    if(btn == 2)
//        return 1;
    return !nrf_gpio_pin_read(btn_pin[btn]);
}

static bool wait_pressed(int btn, int tm){
    uint32_t tm_start = system_timer_value;
    
    while(button_pressed(btn) && system_timer_diff(tm_start) < tm){
        __NOP();
    }
    
    return system_timer_diff(tm_start) >= tm;
}

static uint32_t btn3_tm = 0;
void keyboard_step(void)
{
    if(wait_pressed(0, 500)){
        settings.power_flag = (settings.power_flag+1)%2;
        write_settings();
        
        if(settings.power_flag){
            for(int i=0; i<3; i++){
                gpio_led_red_off();
                system_timer_wait(100);
                gpio_led_red_on();
                system_timer_wait(100);
            }
        }else{
            gpio_led_red_off();
        }
        while(button_pressed(0)){
            system_timer_wait(10);
        }
    }
    if(wait_pressed(1, 500)){
        setup_mode = !setup_mode;
    }
//    btn_state = 0;
//    for(uint8_t i=0; i<BTN_CNT; i++)
//    {
//        if(button_pressed(i))
//            btn_state |= (1<<i);
//    }
//    
//    if(btn_state){
//        gpio_leds_touch(5000);
//    }

//    if(btn_state == BTN1)
//    {
//        wait_pressed(0);
//        settings.power_flag = (settings.power_flag+1)%2;
//        write_settings();
//    }
//    else if(btn_state == BTN2)
//    {
//        wait_pressed(1);
//        setup_mode = !setup_mode;
//        
//    }
//    else if(btn_state == (BTN1|BTN2))
//    {
//    }

//    //однократное нажатие
//    uint8_t btn_changed = btn_state ^ last_btn_state;
//    if(btn_changed)
//    {
//    }
//    
//    if(btn_state == 0)
//        btn_tm = system_timer_value;
//    
//    last_btn_state = btn_state;
}
