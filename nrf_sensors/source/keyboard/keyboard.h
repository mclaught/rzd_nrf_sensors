#ifndef KEYBOARD_H
#define KEYBOARD_H

#include "stdint.h"
#include "board_pin.h"

#if (BOARD == 19001)
    #define BTN1 1
    #define BTN2 2
    #define BTN3    3
#else
    #define BTN1 1
    #define BTN2 2
#endif
#define BTN12 3
#if (BTN_CNT==3)
    #define BTN3 4
#endif

#define BTN_DOWN 0
#define BTN_PRESS 1
#define BTN_LONG 2
#define BTN_UP 3

#define BTN_TIMEOUT 1000

typedef void (*kb_event)(void);

typedef enum
{
    OM_RX,
    OM_TX
}oper_mode_t;

void keyboard_init(void);
void keyboard_uninit(void);
uint8_t button_pressed(uint8_t btn);
void keyboard_step(void);
void keyboard_event(uint8_t buttons, uint8_t long_press);

extern oper_mode_t oper_mode;
extern uint8_t btn_state;

#endif
