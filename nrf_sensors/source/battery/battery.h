#ifndef _BATTERY_H
#define _BATTERY_H

#include <stdbool.h>
#include <stdint.h>
#include "config.h"

typedef enum {
    BAT_EXTRA_LOW,
    BAT_LOW,
    BAT_NORMAL,
    BAT_CHARGING,
    BAT_CHARGED
} batt_charge_state_t;

void battery_preinit(void);
void battery_init(void);
void battery_deinit(void);
batt_charge_state_t battery_charge_state(void);
bool battery_vbat_isok(void);
void battery_step(void);
uint8_t battery_level();
uint8_t batt_control_is_ok(void);

#endif
