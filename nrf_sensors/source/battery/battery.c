#include "battery/battery.h"

#include "board_pin.h"
#include "nrf52.h"
#include "nrf_saadc.h"
#include "nrf_gpio.h"
#include "board_pin.h"
#include "data.h"
#include "gpio/gpio.h"
#include "system/system_timer.h"
#include "state_machine.h"
#include "power/power.h"
#include "config.h"

#ifndef OTK_FIRM
    #error "OTK_FIRM is undefined!!!"
#endif

#define SAMPLES_IN_BUFFER 1
#if (BATT_GAIN == SAADC_CH_CONFIG_GAIN_Gain1)
    #define GAIN 1.0
#elif (BATT_GAIN == SAADC_CH_CONFIG_GAIN_Gain1_2)
    #define GAIN 0.5
#elif (BATT_GAIN == SAADC_CH_CONFIG_GAIN_Gain1_6)
    #define GAIN 0.16667
#else
    #error "BATT_GAIN undefined"
#endif

#if (BATT_RES == SAADC_RESOLUTION_VAL_12bit)
    #define ADC_MAX 4095.0
#elif (BATT_RES == SAADC_RESOLUTION_VAL_10bit)
    #define ADC_MAX 1023.0
#elif (BATT_RES == SAADC_RESOLUTION_VAL_8bit)   
    #define ADC_MAX 255.0
#else
    #error "BATT_RES undefined"
#endif    

static uint32_t tm_adc = 0;
static uint8_t iter = 0;
static float vbatt = 0.0;
static float acharge = 0.0;
static float last_nocharge_vbatt = 0.0;

TIMER_TASK(tt_adc);
//TIMER_TASK(tt_batsign);

void battery_preinit(void)
{
    #ifdef PIN_CHARGE_STATE
        nrf_gpio_cfg_input(PIN_CHARGE_STATE, NRF_GPIO_PIN_NOPULL);
    #endif
    
#ifdef PIN_CH_DET
    #if (PIN_CH_DET_PULL==1)
        nrf_gpio_cfg_input(PIN_CH_DET, NRF_GPIO_PIN_PULLUP);
    #elif (PIN_CH_DET_PULL==2)
        nrf_gpio_cfg_input(PIN_CH_DET, NRF_GPIO_PIN_PULLDOWN);
    #else
        nrf_gpio_cfg_input(PIN_CH_DET, NRF_GPIO_PIN_NOPULL);
    #endif
#endif
}

void battery_init(void)
{
    #if (CH_STATE_TYPE == 18)
    
    NRF_SAADC->CH[0].PSELP =  (VBAT_INPUT                        << SAADC_CH_PSELP_PSELP_Pos);
    NRF_SAADC->CH[0].CONFIG = (SAADC_CH_CONFIG_RESP_Bypass       << SAADC_CH_CONFIG_RESP_Pos) | 
                              (BATT_GAIN                         << SAADC_CH_CONFIG_GAIN_Pos) |
                              (SAADC_CH_CONFIG_REFSEL_Internal   << SAADC_CH_CONFIG_REFSEL_Pos) |
                              (SAADC_CH_CONFIG_TACQ_20us         << SAADC_CH_CONFIG_TACQ_Pos) |
                              (SAADC_CH_CONFIG_MODE_SE           << SAADC_CH_CONFIG_MODE_Pos) |
                              (SAADC_CH_CONFIG_BURST_Disabled    << SAADC_CH_CONFIG_BURST_Pos);
    NRF_SAADC->RESOLUTION = (BATT_RES << SAADC_RESOLUTION_VAL_Pos);
    NRF_SAADC->INTENSET = (SAADC_INTENSET_DONE_Enabled << SAADC_INTENSET_DONE_Pos) |
                          (SAADC_INTENSET_STARTED_Enabled << SAADC_INTENSET_STARTED_Pos);
    
    NVIC_EnableIRQ(SAADC_IRQn);
    NVIC_SetPriority(SAADC_IRQn, 2);
    
    NRF_SAADC->ENABLE = (SAADC_ENABLE_ENABLE_Enabled << SAADC_ENABLE_ENABLE_Pos);
    
    TIMER_TASK_START(tt_adc, 0);
    
    #elif (CH_STATE_TYPE == 7)
    
    NRF_SAADC->CH[0].PSELP =  (SAADC_CH_PSELP_PSELP_VDD << SAADC_CH_PSELP_PSELP_Pos);
    NRF_SAADC->CH[0].CONFIG = (SAADC_CH_CONFIG_RESP_Bypass     << SAADC_CH_CONFIG_RESP_Pos) | 
                              (BATT_GAIN                       << SAADC_CH_CONFIG_GAIN_Pos) |
                              (SAADC_CH_CONFIG_REFSEL_Internal << SAADC_CH_CONFIG_REFSEL_Pos) |
                              (SAADC_CH_CONFIG_TACQ_20us       << SAADC_CH_CONFIG_TACQ_Pos) |
                              (SAADC_CH_CONFIG_MODE_SE         << SAADC_CH_CONFIG_MODE_Pos) |
                              (SAADC_CH_CONFIG_BURST_Disabled  << SAADC_CH_CONFIG_BURST_Pos);
    NRF_SAADC->RESOLUTION = (BATT_RES << SAADC_RESOLUTION_VAL_Pos);
    NRF_SAADC->INTENSET = (SAADC_INTENSET_DONE_Enabled << SAADC_INTENSET_DONE_Pos) |
                          (SAADC_INTENSET_STARTED_Enabled << SAADC_INTENSET_STARTED_Pos);
    
    NVIC_EnableIRQ(SAADC_IRQn);
    NVIC_SetPriority(SAADC_IRQn, 2);
    
    NRF_SAADC->ENABLE = (SAADC_ENABLE_ENABLE_Enabled << SAADC_ENABLE_ENABLE_Pos);
    
    TIMER_TASK_START(tt_adc, 0);
    
    #elif (CH_STATE_TYPE == 74)
    
    NRF_SAADC->CH[0].PSELP =  (SAADC_CH_PSELP_PSELP_VDD        << SAADC_CH_PSELP_PSELP_Pos);
    NRF_SAADC->CH[0].CONFIG = (SAADC_CH_CONFIG_RESP_Bypass     << SAADC_CH_CONFIG_RESP_Pos) | 
                              (BATT_GAIN                       << SAADC_CH_CONFIG_GAIN_Pos) |
                              (SAADC_CH_CONFIG_REFSEL_Internal << SAADC_CH_CONFIG_REFSEL_Pos) |
                              (SAADC_CH_CONFIG_TACQ_20us       << SAADC_CH_CONFIG_TACQ_Pos) |
                              (SAADC_CH_CONFIG_MODE_SE         << SAADC_CH_CONFIG_MODE_Pos) |
                              (SAADC_CH_CONFIG_BURST_Disabled  << SAADC_CH_CONFIG_BURST_Pos);
    
    NRF_SAADC->CH[1].PSELP =  (SAADC_CH_PSELP_PSELP_AnalogInput0  << SAADC_CH_PSELP_PSELP_Pos);
    NRF_SAADC->CH[1].CONFIG = (SAADC_CH_CONFIG_RESP_Bypass        << SAADC_CH_CONFIG_RESP_Pos) | 
                              (BATT_GAIN                          << SAADC_CH_CONFIG_GAIN_Pos) |
                              (SAADC_CH_CONFIG_REFSEL_Internal    << SAADC_CH_CONFIG_REFSEL_Pos) |
                              (SAADC_CH_CONFIG_TACQ_20us          << SAADC_CH_CONFIG_TACQ_Pos) |
                              (SAADC_CH_CONFIG_MODE_SE            << SAADC_CH_CONFIG_MODE_Pos) |
                              (SAADC_CH_CONFIG_BURST_Disabled     << SAADC_CH_CONFIG_BURST_Pos);
                              
    NRF_SAADC->RESOLUTION = (BATT_RES << SAADC_RESOLUTION_VAL_Pos);
    NRF_SAADC->INTENSET = (SAADC_INTENSET_DONE_Enabled << SAADC_INTENSET_DONE_Pos) |
                          (SAADC_INTENSET_STARTED_Enabled << SAADC_INTENSET_STARTED_Pos);
    
    NVIC_EnableIRQ(SAADC_IRQn);
    NVIC_SetPriority(SAADC_IRQn, 2);
    
    NRF_SAADC->ENABLE = (SAADC_ENABLE_ENABLE_Enabled << SAADC_ENABLE_ENABLE_Pos);
    
    TIMER_TASK_START(tt_adc, 0);
    
    #endif
    
    //TIMER_TASK_START(tt_batsign, 100);
}

void battery_deinit(void)
{
    #ifdef PIN_CH_DET
        #if(PIN_CH_DET_PULL==1)
        nrf_gpio_cfg_input(PIN_CH_DET, NRF_GPIO_PIN_PULLDOWN);
        #else
        nrf_gpio_cfg_input(PIN_CH_DET, NRF_GPIO_PIN_NOPULL);
        #endif
    #endif
    
    NRF_SAADC->TASKS_STOP = 1;
    NVIC_DisableIRQ(SAADC_IRQn);
    NRF_SAADC->ENABLE = 0;
}

//batt_charge_state_t last_charge_state = BAT_NORMAL;
static bool is_charge = false;   
static bool ch_det = false;
batt_charge_state_t battery_charge_state(void)
{
    #if (CH_STATE_TYPE == 18)
    
    ch_det = nrf_gpio_pin_read(PIN_CH_DET);
    
    if(!ch_det)
    {
        if(vbatt < VBAT_EXTRA_LOW)
            return BAT_EXTRA_LOW;
        if(vbatt < VBAT_LOW)
            return BAT_LOW;
        else
            return BAT_NORMAL;
    }
    if(!nrf_gpio_pin_read(PIN_CHARGE_STATE))
        return BAT_CHARGING;
    return BAT_CHARGED;
    
    #elif (CH_STATE_TYPE == 7)
        #ifdef PIN_CH_DET
            if(nrf_gpio_pin_read(PIN_CH_DET))// && (vbatt - last_nocharge_vbatt)>0.02
            {
                //if(vbatt < VBAT_MAX)
                    return BAT_CHARGING;
                //else
                    //return BAT_CHARGED;
            }
            else
            {
                if(vbatt >= VBAT_MAX)
                    return BAT_CHARGED;
                else if(vbatt < VBAT_EXTRA_LOW)
                    return BAT_EXTRA_LOW;
                else if(vbatt < VBAT_LOW)
                    return BAT_LOW;
                else
                    return BAT_NORMAL;
            }
        #else
//                if(vbatt >= VBAT_MAX)
//                    return BAT_CHARGED;
//                else 
                if(vbatt < VBAT_EXTRA_LOW)
                    return BAT_EXTRA_LOW;
                else if(vbatt < VBAT_LOW)
                    return BAT_LOW;
                else
                    return BAT_NORMAL;
        #endif
    
    #elif (CH_STATE_TYPE == 74)
        if(!is_charge && acharge > ACHARGE_TRESH_H)
        {
            is_charge = true;
            //last_charge_state = BAT_CHARGING;
        }
        else if(is_charge && acharge < ACHARGE_TRESH_L)
        {
            is_charge = false;
        }
        
        if(is_charge)
            return BAT_CHARGING;
        else if(vbatt >= VBAT_MAX)
            return BAT_CHARGED;
        else if(vbatt < VBAT_EXTRA_LOW)
            return BAT_EXTRA_LOW;
        else if(vbatt < VBAT_LOW)
            return BAT_LOW;
        else
            return BAT_NORMAL;
    #endif
}    

bool battery_vbat_isok(void)
{
    #if (CH_STATE_TYPE == 74)
    return vbatt > VBAT_LOW;
    #else
    return true;
    #endif
}

#define FLT_D   3.0
#define FLT_K   (1/FLT_D)
#define FLT_1_K (1 - FLT_K)
static float battery_filter(float v){
    if(vbatt == 0.0){
        vbatt = v;
    }else{
        vbatt = (vbatt * FLT_1_K) + (v * FLT_K);
    }
    return vbatt;
}

static float lvl;
uint8_t battery_level()
{
    lvl = (vbatt - VBAT_EXTRA_LOW)*100.0 / (VBAT_MAX - VBAT_EXTRA_LOW);
    if(lvl < 0)
        lvl = 0;
    if(lvl > 100)
        lvl = 100.0;
    return (uint8_t)(lvl + 0.5);
}

uint8_t batt_control_is_ok(void){
    return vbatt != 0.0;
}

uint8_t bat_state;
void battery_step(void)
{
    if(TIMER_TASK_CHECK(tt_adc))
    {
        NRF_SAADC->RESULT.PTR = (uint32_t)&batt_data;
        NRF_SAADC->RESULT.MAXCNT = BATT_ADC_CH_CNT;
        
        NRF_SAADC->EVENTS_DONE = 0;
        NRF_SAADC->TASKS_START = 1;
        
        TIMER_TASK_START(tt_adc,1000);
    }
    
}

void SAADC_IRQHandler(void)
{
    if(NRF_SAADC->EVENTS_STARTED)
    {
        NRF_SAADC->EVENTS_STARTED = 0;
        NRF_SAADC->TASKS_SAMPLE = 1;
    }
    
    if(NRF_SAADC->EVENTS_DONE)
    {
        NRF_SAADC->EVENTS_DONE = 0;
 
        #if (CH_STATE_TYPE == 18)
        //vbatt = (float)batt_data[0]/ADC_MAX/GAIN*0.6/160.0*980.0;
        battery_filter((float)batt_data[0]/ADC_MAX/GAIN*0.6/160.0*980.0);
        #elif (CH_STATE_TYPE == 7)
        vbatt = (float)batt_data[0]/ADC_MAX/GAIN*0.6;
        #elif (CH_STATE_TYPE == 74)
        vbatt = (float)batt_data[0]/ADC_MAX/GAIN*0.6;
        acharge = (float)batt_data[1]/ADC_MAX/GAIN*0.6*212.8;
        #endif
    }
}
