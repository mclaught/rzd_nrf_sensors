#ifndef _CONFIG_H
#define _CONFIG_H

// <<< Use Configuration Wizard in Context Menu >>>

// <h> Radio preferences
// <o> Radio mode <0=> 1 mbit/s <1=> 256 kbit/s
#define RADIO_MOD 0

// <o> Protocol type <0=> Simple packets <1=> BLE
// <i> Simple packets minimize transmission time and power consumption. BLE protocol makes compatibility with other devices, such as MC-15 or Android.
#define PROTO 0

// <o> Channel 1 <0-125>
#define RADIO_CHANNEL1  16

// <o> Channel 2 <0-125>
#define RADIO_CHANNEL2  16

// <o> Channel TX <0-125>
#define RADIO_CHANNEL_TX 22

// <o> Master ID <1-65535>
#define RADIO_MASTER_ID 52

#define AMPL_OFF 0
#define AMPL_CTRL 1
#define AMPL_ON 2
// <o> Amplifier power control mode <0=> Allways off <1=> Start-stop <2=> Allways on
#define AMPL_CONTROL 1

// <o> LNA off mode <0=> Bypass <1=> Shutdown
#define LNA_OFF_MODE 0

// <o> Amplifier mode <0=> Low noise <1=> Low current
#define AMPL_MODE 0

//<q> Radio module control <0=> Always on <1=> Power control
#define RADIO_CONTROL 1

//<q> RSSI statistic <0=> Off <1=> On
#define TEST_RSSI 0

// </h>

// <h> Codec prefs

//<q> Codec control <0=> Always on <1=> Power control
#define CODEC_CONTROL 0

// <o> Sound treshold <0-32767>
#define MAX_CTRL_TRESH 100

// <o> Codec off delay <0-10000>
#define MAX_CTRL_TIME 100

// </h>

// <h> OPUS prefs
// <o> Sample rate (Hz)
// <8000UL=> 8000
// <16000UL=> 16000
// <24000UL=> 24000
// <48000UL=> 48000
#define F_SAMPLE_HZ             24000UL

// <o> Audio frame duration (ms)
#define AUDIO_FRAME_SIZE_MS     20

// <o> Target bitrate MAX (Hz)
#define TARGET_BITRATE_BPS      32000

// </h>

// <h> POWER prefs

// <o> Power off mode <0=> Sleep mode <1=> Shutdown mode
#define USE_SHDN 0

// <q> LED power safe <0=> Normal mode <1=> Power safe mode
#define LED_POWER_SAFE 1
// </h>

// <h> Miscelaneous
// <o> Multi-master <0=> Single master <1=> Multiple masters
#define MULTIMASTER   0

// <o> Masters amount
#define MASTER_CNT  2

// <q> Nearfield pairing <0=> Off <1=> On
#define NEAR_PAIR 1

// <o> Default volume
#define VOLUME_DEF  0
// </h>

// <h> Firmware
// <q> Include bootloader <0=> No <1=> Include
#define WITH_BOOT  0

// <q> Force bootloader with other dev_id <0=> No <1=> Force
#define FORCE_BOOT  1

// <q> Testing firmware <0=> Test <1=> Work
#define OTK_FIRM    0
// </h>

// <<< end of configuration section >>>
 
#endif
