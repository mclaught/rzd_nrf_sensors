/**
@file    state_machine.h
@version V1.0
@author Sergey Taldykin
@date    07-Jul-2017
@brief   Конечный автомат

@addtogroup STATE_MACHINE Конечный автомат
@{
@brief Библиотека для быстрого и прозрачного создания конечных автоматов

*/

#ifndef __STATE_MACHINE_H
#define __STATE_MACHINE_H

#include <stdint.h>

/**
@brief Макрос определения состояния текущего счетчика времени.
@note Измените этот макрос в соответствии с тем, как в Вашем приложении реализован счетчик времени
*/
#define __CUR_TM  (system_timer_value)
/**
@brief Количество флагов автомата /32.
@note Количество групп флагов по 32 флага в группе.
*/
#define FLAGS_CNT 1

///@brief Функциональный тип обработчика состояний
typedef void (*StateHandler)(void *_mch);

///@brief Тип переменных конечного автомата
typedef struct{
    StateHandler state;
    StateHandler prev_state;
    uint32_t cycle;
    uint32_t tm;
    uint32_t flags[FLAGS_CNT];
    void* extra;
}StateMachine;

///@brief Тип переменных задачи таймера
typedef struct {
    uint32_t time;
    uint8_t active;
}timer_task_t;

/**
@brief Объявление автомата с инициализацией начальным состоянием
@param m Автомат
@param init_state Начальное состояние
*/
#define __MACHINE(m, init_state) volatile StateMachine m = {(StateHandler)init_state##_StateHandler, 0, 0, 0, {0}};
#define __EXT_MACHINE(m) extern volatile StateMachine m;

/** 
@brief Объявляет обработчик состояния
@param name Имя состояния

Объявляет функцию-обработчик состояния. Обработчик не может быть вложенным ни в какие другие блоки кода 
и должен применяться только в первом уровне вложенности C-файла.
*/
#define __STATE(name) void name##_StateHandler(StateMachine *_mch)

/**
@brief Получение текущего состояния
@param m Автомат
@return Дескриптор текущего состоняния
*/
#define __CUR_STATE(m)    m.state

/**
@brief Получение предыдущего состояния
@param m Автомат
@return Дескриптор предыдущего состоняния
*/
#define __PREV_STATE(m)    m.prev_state

/**
@brief Проверка текущего состояния
@param m    Автомат
@param name Имя состояния
@return true - если текущее состояние соответствует заданному
*/
#define __IN_STATE(m,name) (m.state == (StateHandler)name##_StateHandler)

/**
@brief Установка нового состояния
@param m    Автомат
@param name  Имя состояния
*/
#define __SET_STATE(m, name)     do{ (m).prev_state = (m).state; (m).state = (StateHandler)name##_StateHandler; (m).tm = __CUR_TM; (m).cycle = 0; }while(0)
    
/**
@brief Текущий автомат. Используется внутри обработчиков.
@return Значение типа StateMachine
*/
#define __CUR_MCH   (*(StateMachine*)_mch)    
    
/**
@brief Указатель на текущий автомат. Используется внутри обработчиков.
@return Значение типа StateMachine
*/
#define __CUR_MCH_PTR   ((StateMachine*)_mch)    

/**
@brief Время в текущем состоянии
@param m    Автомат
*/
#define __TIME_IN(m)    (__CUR_TM - (m).tm)
    
#define TIMER_TASK(name)    timer_task_t name = {.time = 0, .active=0}

#define TIMER_TASK_REF extern timer_task_t name
    
#define TIMER_TASK_START(nm, period)  do{ (nm).time = (__CUR_TM + period); (nm).active = 1; }while(0)
    
#define TIMER_TASK_STOP(name)  do{ (name).active = 0; }while(0)    
    
#define TIMER_TASK_ACTIVE(name) (name).active
    
#define TIMER_TASK_CHECK(name)  ( (name).active && __CUR_TM>=(name).time)

/**
@brief № цикла после смены состояния
@param m    Автомат
@return № цикла. 0 - тот же цикл, в котором произошла смена состояния; 1 - первый цикл после смены состояния.


Может использоваться для создания кода, выполняемого при смене состояний.

Примеры:
@code
MCH_DECL_STATE(A)   //обработка состояния A
{
    if( MCH_CYCLE(mch) == 1 )
    {
        //код, выполняемый при переходе в состояние A
    }
    //.................
}

MCH_DECL_STATE(B)   //обработка состояния B
{
    if( MCH_CYCLE(mch) == 1 && MCH_PREV_STATE(mch) == STATE_HNDL(A) )
    {
        //код, выполняемый при переходе из состояние A в состояние B
    }
    //.................
}
@endcode
*/
#define __CYCLE(m)    (m.cycle)

/**
@brief Установка флага. 
@param m Автомат
@param f Порядковый номер флага
*/
#define __SET_FLAG(m, f) do{m.flags[f>>5] |= (1 << (f&0x1F));}while(0)
/**
@brief Сброс флага. 
@param m Автомат
@param f Порядковый номер флага
*/
#define __RESET_FLAG(m, f) do{m.flags[f>>5] &= ~(1 << (f&0x1F));}while(0)
/**
@brief Проверка состояния флага. 
@param m Автомат
@param f Порядковый номер флага
@return Состояние флага (bool).
*/
#define __IS_FLAG(m, f) (m.flags[f>>5] & (1 << (f&0x1F)))

/**
@brief Цикл обработки конечного автомата. 
@param m Автомат
Необходимо использовать этот макрос в главном цикле программы.
Для каждого автомата - свой вызов
*/
#define __PROCESS_MACHINE(m)  do{ m.cycle++; if(m.state) m.state((void*)&m); }while(0)


#define __HANDLER(name) uint32_t name##_last_time = 0; void name##_HANDLER_F(void)

#define __PROCESS_HANDLER(name, period) if((__CUR_TM - name##_last_time) >= period){ name##_HANDLER_F(); name##_last_time = __CUR_TM;}

#endif
/**
@}
*/
