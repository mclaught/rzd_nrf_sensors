#ifndef OPUS_PREFS_H
#define OPUS_PREFS_H

#include "config.h"

/* ***** OPUS related defines ***** */

#define MONO                    1
#define STEREO                  2

#define FRAME_SIZE_16_MAX       ((F_SAMPLE_HZ * AUDIO_FRAME_SIZE_MS) / 1000)
#define FRAME_SIZE_16_VAR       ((F_SAMPLE_HZ * settings.frame_size_ms) / 1000)
#define FRAME_SIZE_16_MIN         ((F_SAMPLE_HZ * 5) / 1000)
//#define I2S_FRAME_SIZE_16       (2 * FRAME_SIZE_16_PLAY)
#define PCM_OFFSET              FRAME_SIZE_16_PLAY
#define PACKET_SIZE_MAX         ((TARGET_BITRATE_BPS * AUDIO_FRAME_SIZE_MS) / 8000)
#define PACKET_SIZE_VAR         ((settings.bitrate * settings.frame_size_ms) / 8)
#define PACKET_SIZE_MIN         ((TARGET_BITRATE_BPS * 5) / 8000)
#define MAX_PACKET_SIZE         (PACKET_SIZE * 2)
#define OPUS_GAIN               0

/* Configures decoder gain adjustment.

Scales the decoded output by a factor specified in Q8 dB units. This has a
maximum range of -32768 to 32767 inclusive, and returns OPUS_BAD_ARG otherwise.
The default is zero indicating no adjustment. This setting survives decoder
reset.

gain = pow(10, x/(20.0*256))

Parameters
 [in]	x	opus_int32: Amount to scale PCM signal by in Q8 dB units.
*/

#endif /* OPUS_PREFS_H */
