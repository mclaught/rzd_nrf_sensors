#ifndef BOARD_PIN_H
#define BOARD_PIN_H

#if (BOARD == 7)                  //RG-07

    #define PIN_RFX2411_TXEN    21
    #define PIN_RFX2411_RXEN    20
    //#define PIN_RFX2411_PDET    28  //-
    #define PIN_RFX2411_MODE    19
    #define PIN_RFX2411_SWANT   18

    #define PIN_LED_RED         5
    #define PIN_LED_GREEN       6

    //#define PIN_MAX9867_DVDD    11  //?

    #define BTN_CNT 2
    #define PIN_BTN1            3
    #define PIN_BTN2            4

    #define PIN_MAX9867_SDOUT   28
    #define PIN_MAX9867_SDIN    29
    #define PIN_MAX9867_LRCLK   27
    #define PIN_MAX9867_BCLK    26
    #define PIN_MAX9867_MCLK    25

    #define PIN_MAX9867_SCL     31
    #define PIN_MAX9867_SDA     30

    //#define PIN_INT_ACCEL2      0
    //#define PIN_INT_ACCEL1      1 
    #define PIN_I2C_SCL         31
    #define PIN_I2C_SDA         30 

    #define PIN_BATT_CHARGE     16
    //#define PIN_CH_DET          7 //на RG-07 PIN_CH_DET отключен в следствие его тотальной глючности
    #define PIN_CH_DET_PULL     1

    #define PIN_EN_PA_PWR       8

    //#define PIN_IRQ_CODEC       2

    //Batt config
    #define BATT_GAIN SAADC_CH_CONFIG_GAIN_Gain1_6
    #define BATT_RES SAADC_RESOLUTION_VAL_8bit
    #define VBAT_EXTRA_LOW    2.3
    #define VBAT_LOW        2.5
    #define VBAT_MAX        2.9
    #define CH_STATE_TYPE   7
    #define BATT_ADC_CH_CNT 1

#elif (BOARD == 18)       // ========================== RG-18 =================================

    #define PIN_RFX2411_TXEN    21
    #define PIN_RFX2411_RXEN    20
    //#define PIN_RFX2411_PDET    28  //-
    #define PIN_RFX2411_MODE    19
    //#define PIN_RFX2411_SWANT   18

    #define PIN_LED_RED         15
    #define PIN_LED_GREEN       14

    //#define PIN_MAX9867_DVDD    11  //?

    #define BTN_CNT 3
    #define PIN_BTN1            26
    #define PIN_BTN2            25
    #define PIN_BTN3            18

    #define PIN_MAX9867_SDOUT   10
    #define PIN_MAX9867_SDIN    9
    #define PIN_MAX9867_LRCLK   8
    #define PIN_MAX9867_BCLK    7
    #define PIN_MAX9867_MCLK    6

    #define PIN_MAX9867_SCL     5
    #define PIN_MAX9867_SDA     4

    //#define PIN_INT_ACCEL2      12
    //#define PIN_INT_ACCEL1      13 
    #define PIN_I2C_SCL         5
    #define PIN_I2C_SDA         4 

    #define PIN_IRQ_CODEC       3
    #define PIN_JACK            27

    #define PIN_VBAT_CHECK      2
    #define PIN_CHARGE_STATE     16
    #define PIN_CH_DET          17
    #define PIN_CH_DET_PULL     0

    //Batt config
    #define BATT_GAIN SAADC_CH_CONFIG_GAIN_Gain1
    #define BATT_RES SAADC_RESOLUTION_VAL_8bit
    #define VBAT_LOW    3.60
    #define VBAT_EXTRA_LOW    3.5
    #define VBAT_MAX        3.7
    #define CH_STATE_TYPE   18
    #define VBAT_INPUT  SAADC_CH_PSELP_PSELP_AnalogInput0
    #define BATT_ADC_CH_CNT 1
    
#elif (BOARD == 1837)       // ========================== RG-18 v.3.7 =================================

    #define PIN_RFX2411_TXEN    21
    #define PIN_RFX2411_RXEN    20
    //#define PIN_RFX2411_PDET    28  //-
    #define PIN_RFX2411_MODE    19
    //#define PIN_RFX2411_SWANT   18

    #define PIN_LED_RED         15
    #define PIN_LED_GREEN       14

    //#define PIN_MAX9867_DVDD    11  //?

    #define BTN_CNT 3
    #define PIN_BTN1            26
    #define PIN_BTN2            25
    #define PIN_BTN3            18

    #define PIN_MAX9867_SDOUT   10
    #define PIN_MAX9867_SDIN    9
    #define PIN_MAX9867_LRCLK   8
    #define PIN_MAX9867_BCLK    7
    #define PIN_MAX9867_MCLK    6

    #define PIN_MAX9867_SCL     5
    #define PIN_MAX9867_SDA     4

    #define PIN_INT_ACCEL2      12
    #define PIN_INT_ACCEL1      13 
    #define PIN_I2C_SCL         5
    #define PIN_I2C_SDA         4 

    #define PIN_IRQ_CODEC       3
    //#define PIN_JACK            0
    //#define JDET_BY_MAX

    #define PIN_VBAT_CHECK      2
    #define PIN_CHARGE_STATE     16
    #define PIN_CH_DET          17
    #define PIN_CH_DET_PULL     0

    //Batt config
    #define BATT_GAIN SAADC_CH_CONFIG_GAIN_Gain1
    #define BATT_RES SAADC_RESOLUTION_VAL_8bit
    #define VBAT_LOW    3.65
    #define VBAT_EXTRA_LOW    3.5
    #define VBAT_MAX        4.1
    #define CH_STATE_TYPE   18
    #define VBAT_INPUT  SAADC_CH_PSELP_PSELP_AnalogInput0
    #define BATT_ADC_CH_CNT 1
    
#elif (BOARD == 18374)       // ========================== RG-18 v.3.74 =================================

    #define PIN_RFX2411_TXEN    21
    #define PIN_RFX2411_RXEN    20
    //#define PIN_RFX2411_PDET    28  //-
    #define PIN_RFX2411_MODE    19
    //#define PIN_RFX2411_SWANT   18

    #define PIN_LED_RED         15
    #define PIN_LED_GREEN       14
    //#define PIN_LED_3           30
    //#define PIN_LED_4           29
    //#define PIN_LED_5           28

    //#define PIN_MAX9867_DVDD    11  //?

    #define BTN_CNT 3
    #define PIN_BTN1            26
    #define PIN_BTN2            25
    #define PIN_BTN3            18

    #define PIN_MAX9867_SDOUT   10
    #define PIN_MAX9867_SDIN    9
    #define PIN_MAX9867_LRCLK   8
    #define PIN_MAX9867_BCLK    7
    #define PIN_MAX9867_MCLK    6

    #define PIN_MAX9867_SCL     5
    #define PIN_MAX9867_SDA     4

    //#define PIN_INT_ACCEL2      12
    //#define PIN_INT_ACCEL1      13 
    #define PIN_I2C_SCL         5
    #define PIN_I2C_SDA         4 

    #define PIN_IRQ_CODEC       3
    //#define PIN_JACK            0
    //#define JDET_BY_MAX

    #define PIN_VBAT_CHECK      2
    #define PIN_CHARGE_STATE     16
    #define PIN_CH_DET          17
    #define PIN_CH_DET_PULL     0

    //Batt config
    #define BATT_GAIN SAADC_CH_CONFIG_GAIN_Gain1_2
    #define BATT_RES SAADC_RESOLUTION_VAL_12bit
    #define VBAT_LOW    3.60
    #define VBAT_EXTRA_LOW    3.5
    #define VBAT_MAX        4.1
    #define CH_STATE_TYPE   18
    #define VBAT_INPUT  SAADC_CH_PSELP_PSELP_AnalogInput0
    #define BATT_ADC_CH_CNT 1
    
    //Regulator 0=LDO 1=DC-DC
    #define REGULATOR   0
    
#elif (BOARD == 18375)       // ========================== RG-18 v.3.75 =================================

    #define PIN_RFX2411_TXEN    21
    #define PIN_RFX2411_RXEN    20
    //#define PIN_RFX2411_PDET    28  //-
    #define PIN_RFX2411_MODE    19
    //#define PIN_RFX2411_SWANT   18

    #define PIN_LED_RED         15
    #define PIN_LED_GREEN       14
    #define PIN_LED_3           30
    #define PIN_LED_4           29
    #define PIN_LED_5           28

    //#define PIN_MAX9867_DVDD    11  //?

    #define BTN_CNT 3
    #define PIN_BTN1            26
    #define PIN_BTN2            25
    #define PIN_BTN3            18

    #define PIN_MAX9867_SDOUT   10
    #define PIN_MAX9867_SDIN    9
    #define PIN_MAX9867_LRCLK   8
    #define PIN_MAX9867_BCLK    7
    #define PIN_MAX9867_MCLK    6

    #define PIN_MAX9867_SCL     5
    #define PIN_MAX9867_SDA     4

    //#define PIN_INT_ACCEL2      12
    //#define PIN_INT_ACCEL1      13 
    #define PIN_I2C_SCL         5
    #define PIN_I2C_SDA         4 

    #define PIN_IRQ_CODEC       3
    #define PIN_JACK            27
    //#define JDET_BY_MAX

    #define PIN_VBAT_CHECK      2
    #define PIN_CHARGE_STATE     16
    #define PIN_CH_DET          17
    #define PIN_CH_DET_PULL     0

    //Batt config
    #define BATT_GAIN SAADC_CH_CONFIG_GAIN_Gain1_2
    #define BATT_RES SAADC_RESOLUTION_VAL_12bit
    #define VBAT_LOW    3.60
    #define VBAT_EXTRA_LOW    3.5
    #define VBAT_MAX        4.1
    #define CH_STATE_TYPE   18
    #define VBAT_INPUT  SAADC_CH_PSELP_PSELP_AnalogInput0
    #define BATT_ADC_CH_CNT 1
    
    //Regulator 0=LDO 1=DC-DC
    #define REGULATOR   1
    
#elif (BOARD == 74)                  //RG-07 v.4

    #define PIN_RFX2411_TXEN    21
    #define PIN_RFX2411_RXEN    20
    //#define PIN_RFX2411_PDET    28  //-
    #define PIN_RFX2411_MODE    19
    #define PIN_RFX2411_SWANT   18

    #define PIN_LED_RED         6
    #define PIN_LED_GREEN       7

    //#define PIN_MAX9867_DVDD    11  //?

    #define BTN_CNT 2
    #define PIN_BTN1            4
    #define PIN_BTN2            5

    #define PIN_MAX9867_SDOUT   28
    #define PIN_MAX9867_SDIN    29
    #define PIN_MAX9867_LRCLK   27
    #define PIN_MAX9867_BCLK    26
    #define PIN_MAX9867_MCLK    25

    #define PIN_MAX9867_SCL     31
    #define PIN_MAX9867_SDA     30

    //#define PIN_INT_ACCEL2      0
    //#define PIN_INT_ACCEL1      1 
    #define PIN_I2C_SCL         31
    #define PIN_I2C_SDA         30 

    #define PIN_BATT_CHARGE     16
    //#define PIN_CH_DET          2 //на RG-07 PIN_CH_DET отключен в следствие его тотальной глючности
    //#define PIN_AN_RF_POUT      3
    
    #define PIN_EN_PA_PWR       8

//    #define PIN_IRQ_CODEC       11
    
    //#define PIN_JACK            10
    
    //Batt config
    #define BATT_GAIN SAADC_CH_CONFIG_GAIN_Gain1_6
    #define BATT_RES SAADC_RESOLUTION_VAL_8bit
    #define VBAT_EXTRA_LOW    2.3
    #define VBAT_LOW        2.5
    #define VBAT_MAX        2.9
    #define CH_STATE_TYPE   7
    #define BATT_ADC_CH_CNT 1
    
#elif (BOARD == 742)                  //RG-07 v.4.2

    #define PIN_RFX2411_TXEN    21
    #define PIN_RFX2411_RXEN    20
    //#define PIN_RFX2411_PDET    28  //-
    #define PIN_RFX2411_MODE    19
    #define PIN_RFX2411_SWANT   18

    #define PIN_LED_RED         14
    #define PIN_LED_GREEN       15

    //#define PIN_MAX9867_DVDD    11  //?

    #define BTN_CNT 2
    #define PIN_BTN1            13
    #define PIN_BTN2            16

    #define PIN_MAX9867_SDOUT   28
    #define PIN_MAX9867_SDIN    29
    #define PIN_MAX9867_LRCLK   27
    #define PIN_MAX9867_BCLK    26
    #define PIN_MAX9867_MCLK    25

    #define PIN_MAX9867_SCL     31
    #define PIN_MAX9867_SDA     30

    //#define PIN_INT_ACCEL2      0
    //#define PIN_INT_ACCEL1      1 
    #define PIN_I2C_SCL         31
    #define PIN_I2C_SDA         30 

    //#define PIN_BATT_CHARGE     16
    #define PIN_CH_DET          2
    //#define PIN_AN_RF_POUT      3
    
    #define PIN_EN_PA_PWR       8

//    #define PIN_IRQ_CODEC       11
    
    #define PIN_JACK            12
    #define PIN_JACK_POS
    
    //Batt config
    #define BATT_GAIN SAADC_CH_CONFIG_GAIN_Gain1_6
    #define BATT_RES SAADC_RESOLUTION_VAL_8bit
    #define VBAT_EXTRA_LOW    2.3
    #define VBAT_LOW        2.5
    #define VBAT_MAX        2.9
    #define CH_STATE_TYPE   74
    #define BATT_ADC_CH_CNT 2
    #define ACHARGE_TRESH_L   38
    #define ACHARGE_TRESH_H   40
    
#elif (BOARD == 19001)       // Наушник MAX9867

    #define PIN_LED_RED         2
    #define PIN_LED_GREEN       1

    #define BTN_CNT             2
    #define PIN_BTN1            21
    #define PIN_BTN2            6

    #define PIN_MAX9867_SDIN             30
    #define PIN_MAX9867_SDOUT             31
    #define PIN_MAX9867_BCLK             28
    #define PIN_MAX9867_LRCLK             29
    #define PIN_MAX9867_MCLK            27
    #define PIN_MAX9867_SCL           26
    #define PIN_MAX9867_SDA           25 
    #define PIN_I2C_SCL         26
    #define PIN_I2C_SDA         25 

    #define PIN_INT_ACCEL2      3
//    #define PIN_INT_ACCEL1      4 

    //#define PIN_IRQ_CODEC       3
    //#define PIN_JACK            0
    #define PIN_CODEC_PDN         0

    #define PIN_VBAT_CHECK       3
    #define PIN_CHARGE_STATE     7
    #define PIN_CH_DET           8
    #define PIN_CH_DET_PULL      2

    //Batt config
    #define BATT_GAIN SAADC_CH_CONFIG_GAIN_Gain1_2
    #define BATT_RES SAADC_RESOLUTION_VAL_12bit
    #define VBAT_LOW    3.60
    #define VBAT_EXTRA_LOW    3.5
    #define VBAT_MAX        4.1
    #define CH_STATE_TYPE   18
    #define VBAT_INPUT  SAADC_CH_PSELP_PSELP_AnalogInput1
    #define BATT_ADC_CH_CNT 1
    
    //Regulator 0=LDO 1=DC-DC
    #define REGULATOR   1
    
#else
    #error "BOARD undefined"
#endif

#endif /* BOARD_PIN_H */
