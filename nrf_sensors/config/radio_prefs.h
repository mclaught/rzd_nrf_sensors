#ifndef RADIO_PREFS_H
#define RADIO_PREFS_H

#include "config.h"

#define RADIO_ADDR1     0x8e89bed6
#define RADIO_ADDR_BEACON    0x54d8b421
#define RADIO_ADDR_PR   0x1234
#define RADIO_ADDR_FEEDBACK   0x2345
#define RADIO_CHANNEL_PAIRING  80
#define RADIO_CHANNEL_SILENT  0
#define RADIO_CHANNEL_INFO  80
#define RADIO_CHANNEL_FEEDBACK  50
#define RADIO_CHANNELS_COUNT    100

#define RADIO_CHANNEL_MIN 1
#define RADIO_CHANNEL_MAX 125

#define RADIO_SERVICE_CNANNEL   100

//#define RADIO_MODE Nrf_1Mbit
//#define RADIO_MODE Nrf_2Mbit
//#define RADIO_MODE Nrf_250Kbit /* Deprecated */
#define RADIO_MODE Ble_1Mbit
//#define RADIO_MODE Ble_2Mbit

//#define RADIO_TXPOWER 0dBm
//#define RADIO_TXPOWER Pos3dBm
#define RADIO_TXPOWER Pos4dBm
//#define RADIO_TXPOWER Neg30dBm
//#define RADIO_TXPOWER Neg40dBm
//#define RADIO_TXPOWER Neg20dBm
//#define RADIO_TXPOWER Neg16dBm
//#define RADIO_TXPOWER Neg12dBm
//#define RADIO_TXPOWER Neg8dBm
//#define RADIO_TXPOWER Neg4dBm

#define RADIO_RFX2411_ANT radio_rfx2411_anta
//#define RADIO_RFX2411_ANT radio_rfx2411_antb

//#define RADIO_RFX2411_RX_MODE radio_rfx2411_bypass
#define RADIO_RFX2411_RX_MODE radio_rfx2411_rx_low_noise
//#define RADIO_RFX2411_RX_MODE radio_rfx2411_rx_low_current

#define RADIO_PROBABILITY 95 /* 95% успешных пакетов */

#endif /* RADIO_PREFS_H */
