#include "gpio.h"
#include "nrf_gpio.h"
#include "nrf_gpiote.h"
#include "board_pin.h"
#include "system/system_timer.h"
#include "uart/uart.h"
#include "radio/radio.h"

uint32_t tm_recv = 0;

void gpioInit(void)
{
    nrf_gpio_cfg_output(PIN_LED_GREEN);
    nrf_gpio_cfg_output(PIN_LED_RED);
}

void gpio_led_green_on(void)
{
    nrf_gpio_pin_set(PIN_LED_GREEN);
}

void gpio_led_green_off(void)
{
    nrf_gpio_pin_clear(PIN_LED_GREEN);
}

void
gpio_led_red_on(void)
{
    nrf_gpio_pin_set(PIN_LED_RED);
}

void
gpio_led_red_off(void)
{
    nrf_gpio_pin_clear(PIN_LED_RED);
}

void gpioStep(void)
{
    uint8_t led_state = 0;

    if(system_timer_diff(tm_recv) < 50)
        led_state |= 1;
    
    if(getset_fl)
        led_state |= 2;
    else if((system_timer_value%1000) < 500)
        led_state |= 2;
    
    switch(led_state){
        case 0:
            gpio_led_red_off();
            gpio_led_green_off();
            break;
        case 1:
            gpio_led_red_on();
            gpio_led_green_off();
            break;
        case 2:
            gpio_led_red_off();
            gpio_led_green_on();
            break;
        case 3:
            gpio_led_red_on();
            gpio_led_green_on();
            break;
    }
}
