#include "settings.h"
#include "nrf.h"
#include "uart/uart.h"

uint32_t   pg_size;
uint32_t   pg_num;
settings_t settings;

void settingsInit(void){
    pg_size = NRF_FICR->CODEPAGESIZE;
    pg_num = NRF_FICR->CODESIZE - 1;
    settings = *(settings_t*)(pg_size * pg_num);
    
//    uint8_t devid[8];
//    ((uint32_t*)devid)[0] = NRF_FICR->DEVICEID[0];
//    ((uint32_t*)devid)[1] = NRF_FICR->DEVICEID[1];
//    settings.master_id = 0;
//    for(int i=0; i<4; i++){
//        settings.master_id ^= ((uint16_t*)devid)[i];
//    }
    if(settings.master_id == 0xFFFF || settings.group>MAX_DATA){
        settings.master_id = 111;
        settings.group = 4;
    }
}

static void flash_page_erase(uint32_t * page_address)
{
    // Turn on flash erase enable and wait until the NVMC is ready:
    NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Een << NVMC_CONFIG_WEN_Pos);

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }

    // Erase page:
    NRF_NVMC->ERASEPAGE = (uint32_t)page_address;

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }

    // Turn off flash erase enable and wait until the NVMC is ready:
    NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Ren << NVMC_CONFIG_WEN_Pos);

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }
}

static void flash_word_write(uint32_t * address, uint32_t value)
{
    // Turn on flash write enable and wait until the NVMC is ready:
    NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Wen << NVMC_CONFIG_WEN_Pos);

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }

    *address = value;

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }

    // Turn off flash write enable and wait until the NVMC is ready:
    NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Ren << NVMC_CONFIG_WEN_Pos);

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }
}

uint32_t* ptr_tmp;
uint32_t* ptr_setts;
int32_t cnt;

void write_settings(void)
{
    flash_page_erase((uint32_t*)(pg_size * pg_num));
    
    ptr_tmp = (uint32_t*)&settings;
    ptr_setts = (uint32_t*)(pg_size * pg_num);
    cnt = sizeof(settings_t);
    while(cnt > 0)
    {
        flash_word_write(ptr_setts, *ptr_tmp);
        ptr_tmp++;
        ptr_setts++;
        cnt -= 4;
    }
}
