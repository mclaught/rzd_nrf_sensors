#ifndef GPIO_H
#define GPIO_H

#include <stdbool.h>
#include <stdint.h>

void gpio_led_green_on(void);
void gpio_led_green_off(void);
void gpioInit(void);
void gpioStep(void);

extern uint32_t tm_recv;

#endif /* GPIO_H */
