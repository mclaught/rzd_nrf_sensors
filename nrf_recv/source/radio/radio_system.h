#ifndef RADIO_SYSTEM_H
#define RADIO_SYSTEM_H

#include <stdint.h>
#include <stdbool.h>

#define RADIO_ADDR              0x8e89bed6

typedef enum {
    RADIO_OPER_NONE,
    RADIO_OPER_RX,
    RADIO_OPER_TX
}radio_oper_t;

typedef void (*radio_rx_callback_t)(void* packet, uint8_t rssi);
typedef bool (*radio_validate_callback_t)(void* packet);//
typedef void (*radio_tx_callback_t)(void);
typedef void (*radio_address_callback_t)(void);
typedef void (*radio_end_callback_t)(void);

void radio_configure(uint32_t addr, uint8_t channel, uint8_t len, uint32_t power, bool continuous);
void radio_set_rx_callback(radio_rx_callback_t r_callback, radio_validate_callback_t v_callback);
void radio_set_tx_callback(radio_tx_callback_t t_callback);
void radio_configure_tx(uint32_t addr, uint8_t channel, uint8_t len, uint32_t power, radio_tx_callback_t t_callback, bool countinuos);
void radio_set_address_callback(radio_address_callback_t cb);
void radio_set_end_callback(radio_end_callback_t cb);
void radio_set_address(uint32_t addr);
void radio_select_address(uint8_t addr_n);
void radio_disable(void);
void radio_read_packet(void);
void radio_send_packet(void* packet);
uint32_t radio_inactive_time(void);

#endif
