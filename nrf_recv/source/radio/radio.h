#ifndef RADIO_H
#define RADIO_H

#include <stdint.h>
#include <stdbool.h>
#include "config.h"

typedef __packed struct {
    uint8_t header; /* 0x42 */
    uint8_t length;
    uint8_t mac[6];
} radio_ble_header_t; //size=8

typedef __packed struct {
    uint8_t  length;
    uint8_t  flags_ad_type;
    uint8_t  name[5];
} bluetooth_ad_name; //size=7

typedef __packed struct {
    uint8_t  length;
    uint8_t  flags_ad_type;
    uint16_t ident;
    uint16_t tag;
    
    uint16_t beacon_id;
    int16_t  t1;    
    uint8_t  pack_n;
} bluetooth_ad_sensors_t;

typedef __packed struct {
    radio_ble_header_t            ble_header;
    bluetooth_ad_sensors_t   beacon;
} radio_sensors_t;

typedef __packed struct {
    uint8_t  length;
    uint8_t  flags_ad_type;
    uint16_t ident;
    uint16_t tag;
//    uint8_t data[16];
    
    uint8_t excursion;
    uint8_t rssi;
    uint8_t repeat;
    uint8_t channel;
    
    uint16_t short_period;
    uint16_t long_period;
    uint8_t  power_level;
    uint8_t reserved2;
    
    uint32_t reserved3;
    uint16_t reserved4;
    
    uint16_t majorId;
    uint16_t minorId;
    uint16_t ex;
} bluetooth_ad_setts_t;

typedef __packed struct {
    radio_ble_header_t     ble_header;
    bluetooth_ad_setts_t   beacon;
} radio_settings_t;

//typedef __packed struct {
//    uint8_t  length;
//    uint8_t  flags_ad_type;
//    uint16_t ident;
//    uint32_t packet_id;
//    uint8_t cnt;
//    dev_id_t data[MAX_SEND_IDS];
//} bluetooth_ad_count_data_t;

//typedef __packed struct{
//    radio_ble_header_t header;
//    bluetooth_ad_count_data_t data;
//} bluetooth_ad_counter_t;

//typedef __packed struct {
//    uint8_t  length;
//    uint8_t  flags_ad_type;
//    uint16_t ident;
//    uint32_t packet_id;
//} bluetooth_ad_count_answer_t;

//typedef __packed struct{
//    radio_ble_header_t header;
//    bluetooth_ad_count_answer_t data;
//} bluetooth_ad_cnt_answer_t;

void RADIO_Init(void);
void RADIO_Step(void);

extern bool getset_fl;

#endif
