#include "radio.h"
#include "radio_system.h"
#include "radio_rfx2411.h"
#include "nrf.h"
#include "nrf52.h"
#include "nrf52_bitfields.h"
#include "config.h"
#include "uart/uart.h"
#include "fifo/fifo.h"
#include "system/timer_us.h"
#include "system/system_timer.h"
#include "settings.h"
#include "uart/uart.h"
#include <string.h>

bool getset_fl = false;

static void address_callback(void){
}

static void radio_tx_callback(void){
}

static void end_callback(void){
}

static bool validate_callback(void* packet){
    radio_sensors_t* pack = (radio_sensors_t*)packet;
    
    if(pack->ble_header.header != 0x42)
        return false;
            
    if(pack->beacon.tag != 0xDACE)
        return false;
    
    if(pack->beacon.ident != (settings.master_id | 0x8000))
        return false;
    
    return true;
}

static void rx_callback(void* packet, uint8_t rssi){
    radio_sensors_t* pack = (radio_sensors_t*)packet;
    UARTAddData(DATA_T, pack->beacon.beacon_id, pack->beacon.t1);
    UARTAddData(DATA_RSSI, pack->beacon.beacon_id, rssi);
}

void RADIO_Init(void){
    radio_rfx2411_init();
    radio_rfx2411_rx_low_noise();
    radio_set_rx_callback(rx_callback, validate_callback);
    radio_configure(RADIO_ADDR, 39, sizeof(radio_sensors_t), RADIO_TXPOWER_TXPOWER_Pos4dBm, false);
    radio_read_packet();
}

void RADIO_Step(void){
}

void RADIO_SendSetts(){
    
}