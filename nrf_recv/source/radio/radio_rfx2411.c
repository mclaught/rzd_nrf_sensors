#include "radio_rfx2411.h"

#include "nrf_gpio.h"

#include "board_pin.h"

void
radio_rfx2411_init(void)
{
    #ifdef PIN_RFX2411_MODE
    nrf_gpio_cfg_output(PIN_RFX2411_MODE);
    nrf_gpio_cfg_output(PIN_RFX2411_RXEN);
    nrf_gpio_cfg_output(PIN_RFX2411_TXEN);
    #endif
    #ifdef PIN_RFX2411_SWANT
    nrf_gpio_cfg_output(PIN_RFX2411_SWANT);
    #endif
    
    #ifdef PIN_EN_PA_PWR
    nrf_gpio_cfg_output(PIN_EN_PA_PWR);
    nrf_gpio_pin_set(PIN_EN_PA_PWR);
    #endif
    
    #ifdef PIN_PA_EN
    nrf_gpio_cfg_output(PIN_PA_EN);
    nrf_gpio_pin_set(PIN_PA_EN);
    #endif

    radio_rfx2411_anta();
    radio_rfx2411_shutdown();
}

void
radio_rfx2411_deinit(void)
{
    #ifdef PIN_RFX2411_MODE
    nrf_gpio_cfg_input(PIN_RFX2411_MODE, NRF_GPIO_PIN_PULLDOWN);
    nrf_gpio_cfg_input(PIN_RFX2411_RXEN, NRF_GPIO_PIN_PULLDOWN);
    nrf_gpio_cfg_input(PIN_RFX2411_TXEN, NRF_GPIO_PIN_PULLDOWN);
    #endif
    #ifdef PIN_RFX2411_SWANT
    nrf_gpio_cfg_input(PIN_RFX2411_SWANT, NRF_GPIO_PIN_PULLDOWN);
    #endif
    
    #ifdef PIN_EN_PA_PWR
    nrf_gpio_pin_clear(PIN_EN_PA_PWR);
    nrf_gpio_cfg_input(PIN_EN_PA_PWR, NRF_GPIO_PIN_PULLDOWN);
    #endif
}

void radio_rfx2411_shutdown(void)
{
    #ifdef PIN_RFX2411_MODE
    nrf_gpio_pin_clear(PIN_RFX2411_TXEN);
    nrf_gpio_pin_clear(PIN_RFX2411_RXEN);
    nrf_gpio_pin_clear(PIN_RFX2411_MODE);
    #endif
}

void
radio_rfx2411_bypass(void)
{
    #ifdef PIN_RFX2411_MODE
    nrf_gpio_pin_clear(PIN_RFX2411_TXEN);
    nrf_gpio_pin_clear(PIN_RFX2411_RXEN);
    nrf_gpio_pin_set(PIN_RFX2411_MODE);
    #endif
}

void
radio_rfx2411_tx_low_idq(void)
{
    #ifdef PIN_RFX2411_TXEN
    nrf_gpio_pin_set(PIN_RFX2411_TXEN);
    nrf_gpio_pin_clear(PIN_RFX2411_MODE);
    #endif
}

void
radio_rfx2411_tx_high_idq(void)
{
    #ifdef PIN_RFX2411_TXEN
    nrf_gpio_pin_set(PIN_RFX2411_TXEN);
    nrf_gpio_pin_set(PIN_RFX2411_MODE);
    #endif
}

void
radio_rfx2411_rx_low_noise(void)
{
    #ifdef PIN_RFX2411_TXEN
    nrf_gpio_pin_clear(PIN_RFX2411_TXEN);
    nrf_gpio_pin_set(PIN_RFX2411_RXEN);
    nrf_gpio_pin_clear(PIN_RFX2411_MODE);
    #endif
}

void
radio_rfx2411_rx_low_current(void)
{
    #ifdef PIN_RFX2411_TXEN
    nrf_gpio_pin_clear(PIN_RFX2411_TXEN);
    nrf_gpio_pin_set(PIN_RFX2411_RXEN);
    nrf_gpio_pin_set(PIN_RFX2411_MODE);
    #endif
}

void
radio_rfx2411_anta(void)
{
    #ifdef PIN_RFX2411_SWANT
    nrf_gpio_pin_set(PIN_RFX2411_SWANT);
    #endif
}

void
radio_rfx2411_antb(void)
{
    #ifdef PIN_RFX2411_SWANT
    nrf_gpio_pin_clear(PIN_RFX2411_SWANT);
    #endif
}
