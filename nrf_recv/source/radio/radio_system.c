#include "radio_system.h"

#include "nrf.h"
#include "nrf52.h"
#include "nrf52_bitfields.h"
#include "system/system_timer.h"

#define RADIO_MAX_PACKET_SIZE   100
//#define RADIO_BUF_PACKET_CNT    4

static radio_oper_t radio_oper;
static radio_rx_callback_t rx_callback = 0;
static radio_validate_callback_t vd_callback = 0;
static radio_tx_callback_t tx_callback = 0;
static radio_address_callback_t address_callback = 0;
static radio_end_callback_t end_callback = 0;
static uint8_t radio_buffer[RADIO_MAX_PACKET_SIZE];
//static uint8_t* radio_buf_in = radio_buffer;
//static uint8_t* radio_buf_out = radio_buffer;
//static uint8_t* radio_buf_end = radio_buffer+RADIO_MAX_PACKET_SIZE*RADIO_BUF_PACKET_CNT;
static uint32_t tm_packet_recv = 0;

void radio_configure(uint32_t addr, uint8_t channel, uint8_t len, uint32_t power, bool continuous)
{
    NVIC_DisableIRQ(RADIO_IRQn);

    NRF_RADIO->TXPOWER = power << RADIO_TXPOWER_TXPOWER_Pos;

    switch(channel){
        case 37:
            NRF_RADIO->FREQUENCY = 2;
            break;
        case 38:
            NRF_RADIO->FREQUENCY = 26;
            break;
        default:
            NRF_RADIO->FREQUENCY = (channel << 1) + 2;
    }
    NRF_RADIO->DATAWHITEIV = channel | 0x40;
    NRF_RADIO->MODE = RADIO_MODE_MODE_Ble_1Mbit;
    
    // Radio address config
    NRF_RADIO->PREFIX0 = addr >> 24;
    NRF_RADIO->PREFIX1 = 0x0UL;
    NRF_RADIO->BASE0   = addr << 8;
    NRF_RADIO->BASE1   = 0x0UL;

    NRF_RADIO->TXADDRESS    = 0x00UL;
    NRF_RADIO->RXADDRESSES  = 0x01UL;

    // Packet configuration
    NRF_RADIO->PCNF0 =
            0UL << RADIO_PCNF0_S1LEN_Pos |
            1UL << RADIO_PCNF0_S0LEN_Pos |
            8UL << RADIO_PCNF0_LFLEN_Pos;


    // Packet configuration
    NRF_RADIO->PCNF1 =
            (RADIO_PCNF1_WHITEEN_Enabled << RADIO_PCNF1_WHITEEN_Pos) | //Enabled
            (RADIO_PCNF1_ENDIAN_Little << RADIO_PCNF1_ENDIAN_Pos) |
            (3UL << RADIO_PCNF1_BALEN_Pos) |
            (0 << RADIO_PCNF1_STATLEN_Pos) |
            (len << RADIO_PCNF1_MAXLEN_Pos);

    // CRC Config

    // Configure CRC calculation
    NRF_RADIO->CRCCNF =
            (RADIO_CRCCNF_SKIPADDR_Skip << RADIO_CRCCNF_SKIPADDR_Pos) |
            (RADIO_CRCCNF_LEN_Three << RADIO_CRCCNF_LEN_Pos);//

    if(continuous){
        NRF_RADIO->SHORTS =
                (RADIO_SHORTS_READY_START_Enabled << RADIO_SHORTS_READY_START_Pos) |
                (RADIO_SHORTS_END_START_Enabled << RADIO_SHORTS_END_START_Pos) |
                (RADIO_SHORTS_END_DISABLE_Disabled << RADIO_SHORTS_END_DISABLE_Pos) |
                (RADIO_SHORTS_DISABLED_RXEN_Disabled << RADIO_SHORTS_DISABLED_RXEN_Pos) |
                (RADIO_SHORTS_DISABLED_TXEN_Disabled << RADIO_SHORTS_DISABLED_TXEN_Pos) |
                (RADIO_SHORTS_ADDRESS_RSSISTART_Disabled << RADIO_SHORTS_ADDRESS_RSSISTART_Pos) |
                (RADIO_SHORTS_DISABLED_RSSISTOP_Disabled << RADIO_SHORTS_ADDRESS_RSSISTART_Pos);
    }else{
        NRF_RADIO->SHORTS =
                (RADIO_SHORTS_READY_START_Enabled << RADIO_SHORTS_READY_START_Pos) |
                (RADIO_SHORTS_END_START_Disabled << RADIO_SHORTS_END_START_Pos) |
                (RADIO_SHORTS_END_DISABLE_Enabled << RADIO_SHORTS_END_DISABLE_Pos) |
                (RADIO_SHORTS_DISABLED_RXEN_Disabled << RADIO_SHORTS_DISABLED_RXEN_Pos) |
                (RADIO_SHORTS_DISABLED_TXEN_Disabled << RADIO_SHORTS_DISABLED_TXEN_Pos) |
                (RADIO_SHORTS_ADDRESS_RSSISTART_Enabled << RADIO_SHORTS_ADDRESS_RSSISTART_Pos) |
                (RADIO_SHORTS_DISABLED_RSSISTOP_Enabled << RADIO_SHORTS_ADDRESS_RSSISTART_Pos);
    }

    NRF_RADIO->CRCINIT = 0x00555555UL;
    NRF_RADIO->CRCPOLY = 0x0000065B;
    

    NRF_RADIO->INTENSET =
            (RADIO_INTENSET_DISABLED_Enabled << RADIO_INTENSET_DISABLED_Pos) |
            (RADIO_INTENSET_ADDRESS_Enabled << RADIO_INTENSET_ADDRESS_Pos) |
            (RADIO_INTENSET_END_Enabled << RADIO_INTENSET_END_Pos);

    NVIC_SetPriority(RADIO_IRQn, 1);
    NVIC_ClearPendingIRQ(RADIO_IRQn);
    NVIC_EnableIRQ(RADIO_IRQn);
    
}

void radio_set_rx_callback(radio_rx_callback_t r_callback, radio_validate_callback_t v_callback){
    rx_callback = r_callback;
    vd_callback = v_callback;
}

void radio_set_tx_callback(radio_tx_callback_t t_callback){
    tx_callback = t_callback;
}

void radio_set_address(uint32_t addr){
    addr |= (RADIO_ADDR & 0xFFFF0000);
    
    NRF_RADIO->PREFIX0 = addr >> 24;
    NRF_RADIO->PREFIX1 = 0x0UL;
    NRF_RADIO->BASE0   = addr << 8;
    NRF_RADIO->BASE1   = 0x0UL;
}

void radio_select_address(uint8_t addr_n){
    NRF_RADIO->TXADDRESS = addr_n;
}

void radio_set_address_callback(radio_address_callback_t cb){
    address_callback = cb;
}

void radio_set_end_callback(radio_end_callback_t cb){
    end_callback = cb;
}

void radio_disable(void){
    radio_oper = RADIO_OPER_NONE;
    
    NVIC_DisableIRQ(RADIO_IRQn);
    
    if(NRF_RADIO->STATE != (RADIO_STATE_STATE_Disabled << RADIO_STATE_STATE_Pos)){
        NRF_RADIO->TASKS_DISABLE = 1;
        while(NRF_RADIO->EVENTS_DISABLED == 0);
    }
    NRF_RADIO->EVENTS_DISABLED = 0;
    
}

uint32_t radio_inactive_time(void){
    return system_timer_diff(tm_packet_recv);
}

void radio_read_packet(void){
    radio_oper = RADIO_OPER_RX;
    
    NRF_RADIO->PACKETPTR = (uint32_t)radio_buffer;
    NRF_RADIO->TASKS_RXEN = 1;
}

void radio_send_packet(void* packet){
    //radio_disable();
    
    radio_oper = RADIO_OPER_TX;
    
    NRF_RADIO->PACKETPTR = (uint32_t)packet;
    NRF_RADIO->TASKS_TXEN = 1;
}

void RADIO_IRQHandler(void){
    if(NRF_RADIO->EVENTS_DISABLED)
    {
        NRF_RADIO->EVENTS_DISABLED = 0;
        
        switch(radio_oper){
            case RADIO_OPER_NONE:
                break;
            case RADIO_OPER_RX:
                if(rx_callback){
                    if(NRF_RADIO->CRCSTATUS == (RADIO_CRCSTATUS_CRCSTATUS_CRCOk << RADIO_CRCSTATUS_CRCSTATUS_Pos)){
                        bool valid = vd_callback ? vd_callback(radio_buffer) : true;
                        if(valid){
                            if(rx_callback)
                                rx_callback(radio_buffer, NRF_RADIO->RSSISAMPLE);
                            
                            tm_packet_recv = system_timer_value;//get_sys_time();
                        }
                    }
                }
                break;
            case RADIO_OPER_TX:
                if(tx_callback){
                    tx_callback();
                }
                break;
        }
        
        if(radio_oper == RADIO_OPER_RX){
            radio_read_packet();
        }
    }
    
    if(NRF_RADIO->EVENTS_ADDRESS){
        NRF_RADIO->EVENTS_ADDRESS = 0;
        
        if(address_callback)
            address_callback();
    }
    
    if(NRF_RADIO->EVENTS_END){
        NRF_RADIO->EVENTS_END = 0;
        
        if(end_callback)
            end_callback();
    }
}
