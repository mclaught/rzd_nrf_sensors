#include "fifo.h"

fifo_t fifo[MAX_CH_CNT];

void fifo_init(void){
    for(int i=0; i<MAX_CH_CNT; i++){
        fifo[i].in = fifo[i].out = 0;
    }
}

int fifo_cnt(int n){
    if(fifo[n].in >= fifo[n].out)
        return fifo[n].in - fifo[n].out;
    else
        return fifo[n].in + FIFO_SIZE - fifo[n].out;
}

int fifo_free(int n){
    return FIFO_SIZE - fifo_cnt(n);
}

uart_packet_t* fifo_in_ptr(int n){
    return &fifo[n].fifo[fifo[n].in];
}

void fifo_in_next(int n){
    fifo[n].in++;
    if(fifo[n].in >= FIFO_SIZE)
        fifo[n].in = 0;
}

uart_packet_t* fifo_out_ptr(int n){
    return &fifo[n].fifo[fifo[n].out];
}

void fifo_out_next(int n){
    fifo[n].out++;
    if(fifo[n].out >= FIFO_SIZE)
        fifo[n].out = 0;
}
