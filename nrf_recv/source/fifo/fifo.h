#ifndef FIFO_H
#define FIFO_H

#include "config.h"
#include "uart/uart.h"

#define FIFO_SIZE   100

typedef struct{ 
    uart_packet_t fifo[FIFO_SIZE];
    int in;
    int out;
}fifo_t;

void fifo_init(void);
int fifo_cnt(int n);
int fifo_free(int n);
uart_packet_t* fifo_in_ptr(int n);
void fifo_in_next(int n);
uart_packet_t* fifo_out_ptr(int n);
void fifo_out_next(int n);

#endif
