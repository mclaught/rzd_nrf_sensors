#include "timer_us.h"

#include "nrf.h"
#include "nrf52.h"

void timer_us_start(void){
    NRF_TIMER0->TASKS_STOP = 1;

    // Create an Event-Task shortcut to clear Timer 1 on COMPARE[0] event.
    NRF_TIMER0->SHORTS     = 0;//(TIMER_SHORTS_COMPARE0_STOP_Enabled << TIMER_SHORTS_COMPARE0_STOP_Pos);
    NRF_TIMER0->MODE       = TIMER_MODE_MODE_Timer;
    NRF_TIMER0->BITMODE    = (TIMER_BITMODE_BITMODE_32Bit << TIMER_BITMODE_BITMODE_Pos);
    NRF_TIMER0->PRESCALER  = 4;  // 1us resolution
    NRF_TIMER0->TASKS_START = 1;
}

uint32_t timer_us_get(void){
    NRF_TIMER0->TASKS_CAPTURE[0] = 1;
    return NRF_TIMER0->CC[0]; 
}

void timer_us_stop(void){
    NRF_TIMER0->TASKS_STOP = 1;
}
