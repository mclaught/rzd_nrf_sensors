#include "system_timer.h"

#include "nrf52.h"
#include "nrf52_bitfields.h"
#include "nrf_drv_clock.h"
#include "nrf_rtc.h"

#define LFCLK_FREQUENCY           (32768UL)
#define RTC_FREQUENCY             (1000UL)
#define COMPARE_COUNTERTIME       (5)

#define COUNTER_PRESCALER         ((LFCLK_FREQUENCY/RTC_FREQUENCY) - 1)

volatile system_time_t system_timer_value = 0;

void
RTC1_IRQHandler(void)
{
    NRF_RTC1->EVENTS_TICK = 0;
    if (NRF_RTC1->EVENTS_COMPARE[0] != 0)
    {
        NRF_RTC1->EVENTS_COMPARE[0] = 0;
        NRF_RTC1->TASKS_CLEAR = 1;
    }
    system_timer_value++;
}

system_time_t
system_timer_diff(system_time_t old_time)
{
    return system_timer_value - old_time;
}

void system_timer_init(void)
{
    /* Request LF clock */
    nrf_drv_clock_lfclk_request(NULL);

    NRF_RTC1->TASKS_STOP = 1;
    NRF_RTC1->TASKS_CLEAR = 1;

    NRF_RTC1->PRESCALER = COUNTER_PRESCALER;
    NRF_RTC1->CC[0] = COMPARE_COUNTERTIME * RTC_FREQUENCY;

    // Enable TICK event and TICK interrupt:
    NRF_RTC1->EVTENSET = RTC_EVTENSET_TICK_Msk;
    NRF_RTC1->INTENSET = RTC_INTENSET_TICK_Msk;

    // Enable COMPARE0 event and COMPARE0 interrupt:
    NRF_RTC1->EVTENCLR = RTC_EVTENCLR_COMPARE0_Msk;
    NRF_RTC1->INTENCLR = RTC_INTENCLR_COMPARE0_Msk;

    NVIC_SetPriority(RTC1_IRQn, 1);
    NVIC_EnableIRQ(RTC1_IRQn);
    NRF_RTC1->TASKS_START = 1;
}

void
system_timer_deinit(void)
{
    NRF_RTC1->TASKS_STOP = 1;
    NVIC_DisableIRQ(RTC1_IRQn);
    NRF_RTC1->EVTENCLR = RTC_EVTENSET_TICK_Msk;
    NRF_RTC1->INTENCLR = RTC_INTENSET_TICK_Msk;
    nrf_drv_clock_lfclk_release();
}

void
system_timer_init_poweroff(void)
{
    NRF_RTC1->TASKS_STOP = 1;
    NRF_RTC1->TASKS_CLEAR = 1;

    NRF_RTC1->PRESCALER = COUNTER_PRESCALER;
    NRF_RTC1->CC[0] = COMPARE_COUNTERTIME * RTC_FREQUENCY;

    // Enable TICK event and TICK interrupt:
    NRF_RTC1->EVTENCLR = RTC_EVTENCLR_TICK_Msk;
    NRF_RTC1->INTENCLR = RTC_INTENCLR_TICK_Msk;

    // Enable COMPARE0 event and COMPARE0 interrupt:
    NRF_RTC1->EVTENSET = RTC_EVTENSET_COMPARE0_Msk;
    NRF_RTC1->INTENSET = RTC_INTENSET_COMPARE0_Msk;

    NVIC_SetPriority(RTC1_IRQn, 1);
    NVIC_EnableIRQ(RTC1_IRQn);
    NRF_RTC1->TASKS_START = 1;
}

void
system_timer_wait(system_time_t time)
{
    static system_time_t start;

    start = system_timer_value;
    while (system_timer_value - start < time)
        __WFE();
}

void
system_delay_ms(uint16_t ms)
{
    while (ms--)
    {
        volatile uint32_t a;
        for (a = 0; a < 7143; a++)
            (void)a;
    }
}
