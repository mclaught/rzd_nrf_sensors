#ifndef TIMER_US_H
#define TIMER_US_H

#include <stdint.h>

void timer_us_start(void);
uint32_t timer_us_get(void);
void timer_us_stop(void);

#endif
