#ifndef UART_SYSTEM_H
#define UART_SYSTEM_H

#include <stdint.h>
#include <stdbool.h>

#define UART NRF_UARTE0

typedef void (*uart_tx_callback_t)(void);

void uart_init(void);
uint32_t uart_available(void);
uint32_t uart_get_bytes(uint8_t* buf, int sz);
void uart_send(uint8_t* buf, int sz);
bool uart_isbusy(void);

extern uart_tx_callback_t uart_tx_callback;

#endif
