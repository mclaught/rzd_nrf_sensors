#include "uart.h"

#include "uart_system.h"
#include "fifo/fifo.h"
#include "radio/radio.h"
#include "settings.h"
#include "system/system_timer.h"
#include "gpio.h"
#include "board_pin.h"
#include "nrf_gpio.h"
#include <string.h>

uart_packet_t uart_packet = {
    .header = {
        .sign = 0xBBAA,
        .len = 0
    },
};
int packs_cnt = 0;
char uart_command[100];
int uart_command_n = 0;

uint8_t crc(uint8_t* data, int len){
    uint8_t _crc = 0;
    for(int i=0; i<len; i++)
        _crc ^= data[i];
    return _crc;
}

uint16_t crc16(uint8_t *pcBlock, uint16_t len)
{
    uint16_t crc = 0xFFFF;
    uint8_t i;

    while (len--)
    {
        crc ^= *pcBlock++ << 8;

        for (i = 0; i < 8; i++)
            crc = crc & 0x8000 ? (crc << 1) ^ 0x1021 : crc << 1;
    }
    return crc;
}

static bool parseParam(char* cmd, int *value){
    *value = 0;
    if(strstr(uart_command, cmd)){
        for(int i=strlen(cmd); (uart_command[i]!='\r' && uart_command[i]!='\n'); i++){
            char c = uart_command[i];
            *value *= 10;
            *value += (c - '0');
        }
        
        write_settings();
        
        gpio_led_green_on();
        system_timer_wait(1000);
        gpio_led_green_off();
        
        return true;
    }
    return false;
}

void processCmd(void){
    int value = 0;
    bool ok = false;
    if(parseParam("MASTER_ID=", &value)){
        settings.master_id = value;
        ok = true;
    }
    if(parseParam("GROUP=", &value)){
        settings.group = value<MAX_DATA ? value : MAX_DATA;
        ok = true;
    }
    
    if(ok){
        write_settings();
    }
}

static void uart_tx(){
//    for(int i=0; i<10; i++)__NOP();
    nrf_gpio_pin_clear(PIN_RS485_RW);
}

void UARTInit(void){
    nrf_gpio_cfg_output(PIN_RS485_RW);
    nrf_gpio_pin_clear(PIN_RS485_RW);
    uart_tx_callback = uart_tx;
    uart_init();
}

void UARTStep(void){
    char c;
    if(uart_available() >= 1){
        int n = uart_get_bytes((uint8_t*)&c, 1);
        if(uart_command_n < 100){
            uart_command[uart_command_n++] = c;
            if(c == '\n'){
                processCmd();
                uart_command_n = 0;
            }
        }
    }
}

void UARTAddData(uint8_t type, uint8_t code, uint32_t value){
    if(packs_cnt >= settings.group){
        packs_cnt = 0;
        uart_packet.header.len = 0;
    }
    
    uart_packet.data[packs_cnt].type = type;
    uart_packet.data[packs_cnt].code = code;
    uart_packet.data[packs_cnt].value = value;
    
    packs_cnt++;
    
    uart_packet.header.len += sizeof(uart_data_t);

    if(packs_cnt == settings.group){
        uint8_t* p = (uint8_t*)&uart_packet;
        p += sizeof(uart_header_t);
        p += uart_packet.header.len;
        *(uint16_t*)p = crc16((uint8_t*)uart_packet.data, uart_packet.header.len);
        nrf_gpio_pin_set(PIN_RS485_RW);
        for(int i=0; i<5; i++)__NOP();
        uart_send((uint8_t*)&uart_packet, UART_PACK_SZ(packs_cnt));
    }
    
    tm_recv = system_timer_value;
}
