#ifndef UART_H
#define UART_H

#include <stdint.h>
#include <stdbool.h>

#include "config.h"

#define MAX_DATA    200

enum uart_data_type_t {DATA_T=1, DATA_RSSI};

typedef __packed struct {
    uint16_t sign;              //сигнатура пакета = 0xAABB
    uint16_t  len;
}uart_header_t;

typedef __packed struct {
    uint8_t type;
    uint8_t code;
    uint32_t value;
}uart_data_t;

typedef __packed struct {
    uart_header_t header;
    uart_data_t data[MAX_DATA];
    uint16_t crc;
}uart_packet_t;

#define UART_PACK_SZ(N) (sizeof(uart_header_t) + N*sizeof(uart_data_t) + sizeof(uint16_t))
#define CRC_PTR(pack) (uint16_t*)((uint8_t*)pack + sizeof(uart_header_t) + pack->header.len)
    
void UARTInit(void);
void UARTStep(void);
bool UARTActive(void);
void UARTAddData(uint8_t type, uint8_t code, uint32_t value);

#endif
