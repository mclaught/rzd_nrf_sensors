#include "uart_system.h"

#include "nrf52.h"
#include "nrf_uarte.h"
#include "nrf_uart.h"
#include "nrf_gpio.h"
#include "board_pin.h"
#include "system/system_timer.h"
#include <string.h>

#define UART_BUFFER_MIN 4
#define UART_BUFFER_SIZE 100

static uint8_t buffer[UART_BUFFER_SIZE];
static uint8_t* buf_recv = buffer + UART_BUFFER_MIN;
static uint8_t* buf_in = buffer;
static uint8_t* buf_out = buffer;
static uint8_t* buf_end = buffer + UART_BUFFER_SIZE;
static uint8_t uart_answer[100];
extern volatile uint32_t uart_test_cnt;

uart_tx_callback_t uart_tx_callback = NULL;

void uart_init(void){
    nrf_gpio_cfg_output(PIN_UART_TX);
    nrf_gpio_cfg_input(PIN_UART_RX, NRF_GPIO_PIN_NOPULL);
    
//    nrf_uarte_baudrate_set(UART, NRF_UARTE_BAUDRATE_115200);// 
//    nrf_uarte_configure(UART, NRF_UARTE_PARITY_EXCLUDED, NRF_UARTE_HWFC_DISABLED);
//    nrf_uarte_txrx_pins_set(UART, PIN_UART_TX, NRF_UARTE_PSEL_DISCONNECTED);
//    nrf_uarte_hwfc_pins_disconnect(UART);
//    nrf_uarte_int_enable(UART, NRF_UARTE_INT_ENDTX_MASK);//NRF_UARTE_INT_RXSTARTED_MASK | NRF_UARTE_INT_ENDRX_MASK | NRF_UARTE_INT_RXTO_MASK | 
//    
//    nrf_uarte_shorts_enable(UART, NRF_UARTE_SHORT_ENDRX_STARTRX);
//                             
//    nrf_uarte_event_clear(UART, NRF_UARTE_EVENT_CTS);
//    nrf_uarte_event_clear(UART, NRF_UARTE_EVENT_ERROR);
//    nrf_uarte_event_clear(UART, NRF_UARTE_EVENT_NCTS);
//    nrf_uarte_event_clear(UART, NRF_UARTE_EVENT_ENDRX);
//    nrf_uarte_event_clear(UART, NRF_UARTE_EVENT_RXTO);
//    nrf_uarte_event_clear(UART, NRF_UARTE_EVENT_RXSTARTED);
//    
//    nrf_uarte_rx_buffer_set(UART, buffer, UART_BUFFER_MIN);
//    
//    nrf_uarte_enable(UART);
    
    nrf_uart_baudrate_set(NRF_UART0, NRF_UART_BAUDRATE_115200);
    nrf_uart_configure(NRF_UART0, NRF_UART_PARITY_EXCLUDED, NRF_UART_HWFC_DISABLED);
    nrf_uart_txrx_pins_set(NRF_UART0, PIN_UART_TX, PIN_UART_RX);
    nrf_uart_hwfc_pins_disconnect(NRF_UART0);
    nrf_uart_int_enable(NRF_UART0, NRF_UART_INT_MASK_RXDRDY | NRF_UART_INT_MASK_TXDRDY);
    
    nrf_uart_enable(NRF_UART0);
    
    NVIC_SetPriority(UARTE0_UART0_IRQn, 2);
    NVIC_ClearPendingIRQ(UARTE0_UART0_IRQn);
    NVIC_EnableIRQ(UARTE0_UART0_IRQn);
    
    nrf_uart_task_trigger(NRF_UART0, NRF_UART_TASK_STARTRX);
}

uint32_t uart_available(void){
    if(buf_in >= buf_out)
        return buf_in - buf_out;
    else
        return buf_in + UART_BUFFER_SIZE - buf_out;
}

uint32_t uart_get_bytes(uint8_t* buf, int sz){
    uint8_t* p = buf;
    int rd_cnt = 0;
    for(int i=0; i<sz; i++){
        *p = *buf_out;
        p++;
        buf_out++;
        rd_cnt++;
        if(buf_out >= buf_end)
            buf_out = buffer;
        if(buf_out == buf_in)
            break;
    }
    return rd_cnt;
}

static uint8_t* send_buf;
static int send_cnt = 0;
static int send_n = 0;
static bool busy = false;
void uart_send(uint8_t* buf, int sz){
//    if(buf < (uint8_t*)0x20000000 || buf >= (uint8_t*)0x20010000 ){
//        tmp_buf = buf;
//        memcpy(uart_answer, buf, sz);
//        buf = uart_answer;
//    }
//    
//    busy = true;
//    nrf_uarte_tx_buffer_set(UART, buf, sz);
//    nrf_uarte_task_trigger(UART, NRF_UARTE_TASK_STARTTX);
    
    send_buf = buf;
    send_cnt = sz;
    send_n = 0;
    
    nrf_uart_task_trigger(NRF_UART0, NRF_UART_TASK_STARTTX);
    nrf_uart_txd_set(NRF_UART0, send_buf[send_n]);
}

bool uart_isbusy(void){
    return busy;
}

void UARTE0_UART0_IRQHandler(void){
//    if(UART->EVENTS_RXSTARTED){
//        UART->EVENTS_RXSTARTED = 0;
//        
//        UART->RXD.PTR = (uint32_t)buf_recv;
//        buf_recv += UART_BUFFER_MIN;
//        if(buf_recv >= buf_end)
//            buf_recv = buffer;
//    }

//    if(UART->EVENTS_ENDRX){
//        UART->EVENTS_ENDRX = 0;
//        
//        buf_in += UART_BUFFER_MIN;
//        if(buf_in >= buf_end)
//            buf_in = buffer;
//        //uart_test_cnt += UART_BUFFER_MIN;
//    }
//    
//    if(UART->EVENTS_RXTO){
//        UART->EVENTS_RXTO = 0;
//        
//        nrf_uarte_task_trigger(UART, NRF_UARTE_TASK_FLUSHRX);
//    }
//    
//    if(UART->EVENTS_ENDTX){
//        UART->EVENTS_ENDTX = 0;
//        busy = false;
//        if(uart_tx_callback)
//            uart_tx_callback();
//    }

    if(NRF_UART0->EVENTS_RXDRDY){
        NRF_UART0->EVENTS_RXDRDY = 0;
        
        *buf_in = NRF_UART0->RXD;
        buf_in++;
        if(buf_in >= buf_end)
            buf_in = buffer;
    }
    
    if(NRF_UART0->EVENTS_TXDRDY){
        NRF_UART0->EVENTS_TXDRDY = 0;
        
        send_n++;
        
        if(send_n < send_cnt){
            nrf_uart_txd_set(NRF_UART0, send_buf[send_n]);
        }else{
            nrf_uart_task_trigger(NRF_UART0, NRF_UART_TASK_STOPTX);
            if(uart_tx_callback)
                uart_tx_callback();
        }
    }
}
