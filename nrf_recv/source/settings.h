#ifndef SETTINGS_H
#define SETTINGS_H

#include <stdint.h>

typedef __packed struct{
    uint16_t master_id;
    uint8_t ch1;
    uint8_t ch2;
    uint8_t group;
}settings_t;

void settingsInit(void);
void write_settings(void);

extern settings_t settings;

#endif
