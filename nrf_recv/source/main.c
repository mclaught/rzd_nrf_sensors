#include "bsp.h"
#include "nrf52.h"
#include "nrf_clock.h"
#include "nrf_drv_clock.h"
#include "system/system_timer.h"
#include "uart/uart_system.h"
#include "uart/uart.h"
#include "fifo/fifo.h"
#include "radio/radio.h"
#include "settings.h"
#include "gpio.h"
#include "board_pin.h"

void oscillator_init(void)
{
    /* Start 16 MHz crystal oscillator */
    NRF_CLOCK->EVENTS_HFCLKSTARTED = 0;
    NRF_CLOCK->TASKS_HFCLKSTART = 1;

    /* Wait for the external oscillator to start up */
    while (NRF_CLOCK->EVENTS_HFCLKSTARTED == 0)
    {
    }
}

int main(void)
{
    ret_code_t err_code;
    err_code = nrf_drv_clock_init();
    
    APP_ERROR_CHECK(err_code);
    
    oscillator_init();
    
    #ifdef REGULATOR
        #if (REGULATOR == 1)
        NRF_POWER->DCDCEN = 1;
        #endif
    #else
        #error "REGULATOR not defined"
    #endif
    
    system_timer_init();
    
    settingsInit();
    UARTInit();
    RADIO_Init();
    gpioInit();
    
    while(1)
    {
        UARTStep();
        RADIO_Step();
        gpioStep();
    }
}
