#ifndef RADIO_PREFS_H
#define RADIO_PREFS_H

#include "nrf52_bitfields.h"

#define RADIO_POWER     RADIO_TXPOWER_TXPOWER_Pos4dBm
#define RADIO_MODE      RADIO_MODE_MODE_Ble_1Mbit

//---------------- BLE -----------------
#define BLE_ADV_ADDR    0x8e89bed6
#define BLE_ADV_CHANNEL 39
#define BLE_PAIRING_CHANNEL 39
//--------------------------------------

#define RADIO_ADDR    0x052d755a
#define RADIO_CHANNEL 7

//--------------250----------------------
#define NRF_PAIRING_ADDR         0x00A5A500UL
#define NRF_PAIRING_CHANNEL      0

#define NRF_ADDR        0x00973501UL
#define NRF_CHANNEL     0x01 
//---------------------------------------
#endif /* RADIO_PREFS_H */
