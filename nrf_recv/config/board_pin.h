#ifndef BOARD_PIN_H
#define BOARD_PIN_H

#if (BOARD==7)

    #define PIN_RFX2411_TXEN    21
    #define PIN_RFX2411_RXEN    20
    //#define PIN_RFX2411_PDET    28  //-
    #define PIN_RFX2411_MODE    19
    #define PIN_RFX2411_SWANT   18

    #define PIN_LED_RED         5
    #define PIN_LED_GREEN       6

    #define PIN_UART_RX         3
    #define PIN_UART_TX         4
    
#elif (BOARD == 18)
    #define PIN_RFX2411_TXEN    21
    #define PIN_RFX2411_RXEN    20
    #define PIN_RFX2411_MODE    19
    
    #define PIN_LED_RED         15
    #define PIN_LED_GREEN       14

    #define PIN_UART_RX         26
    #define PIN_UART_TX         25
    
    #define PIN_RS485_RW        18
    
    #define REGULATOR           1
    
#elif (BOARD == 19)
    #define PIN_PA_EN           22

    #define PIN_LED_RED         27
    #define PIN_LED_GREEN       28

    #define PIN_UART_RX         5
    #define PIN_UART_TX         4
    
    #define PIN_RS485_RW        6
    
    #define REGULATOR           0
#endif
    
#endif /* BOARD_PIN_H */
