package com.mclaught.rzd_ppru_updater;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;

import java.lang.reflect.Method;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;

public class MainActivity extends FlutterActivity {
    private static final String CHANNEL = "com.mclaught.ppru_updater/main";
    private MethodChannel channel;
    private MethodChannel.Result pendingResult;

    @SuppressLint("MissingPermission")
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == 1){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                startActivity(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE));
                pendingResult.success(true);
            }else{
                pendingResult.success(false);
            }
        }else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        super.configureFlutterEngine(flutterEngine);

        channel = new MethodChannel(flutterEngine.getDartExecutor(), CHANNEL);

        channel.setMethodCallHandler(new MethodChannel.MethodCallHandler() {
            @Override
            public void onMethodCall(@NonNull MethodCall call, @NonNull MethodChannel.Result result) {
                if(call.method.equals("RequestBluetoothPermissions")){
                    RequestBluetoothPermissions(call, result);
                }else if(call.method.equals("AndroidSDK")){
                    AndroidSDK(call, result);
                }else if(call.method.equals("startLocationIntent")){
                    startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
                    result.success(true);
                }
            }
        });
    }

    private void AndroidSDK(MethodCall call, MethodChannel.Result result) {
        result.success(Build.VERSION.SDK_INT);
    }

    private void RequestBluetoothPermissions(MethodCall call, MethodChannel.Result result) {
        pendingResult = result;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            requestPermissions(new String[]{
                    "android.permission.BLUETOOTH_CONNECT",
                    "android.permission.BLUETOOTH_SCAN",
                    "android.permission.BLUETOOTH_ADMIN"
            }, 1);
        }else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            requestPermissions(new String[]{
                    "android.permission.ACCESS_COARSE_LOCATION",
                    "android.permission.ACCESS_FINE_LOCATION"
            }, 1);
//            startActivity(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE));
//            pendingResult.success(true);
        }
    }
}
