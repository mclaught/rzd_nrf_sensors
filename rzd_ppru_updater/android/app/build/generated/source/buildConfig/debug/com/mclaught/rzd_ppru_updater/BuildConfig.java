/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.mclaught.rzd_ppru_updater;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.mclaught.rzd_ppru_updater";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0.0";
}
