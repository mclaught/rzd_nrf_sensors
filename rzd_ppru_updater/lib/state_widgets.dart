import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:rzd_ppru_updater/ble_uart.dart';
import 'package:rzd_ppru_updater/dev_setts.dart';

class StateWidget extends StatelessWidget{
  bool horz=false;
  double imgSz=300;

  StateWidget({Key? key}) : super(key: key);

  void initSize(BuildContext context, double padV, double padH, {double maxSz=300}){
    Size sz = MediaQuery.of(context).size;
    horz = sz.width > sz.height;
    imgSz = (sz.width > sz.height ? sz.height - padV : sz.width-padH);
    if(imgSz > maxSz) imgSz = maxSz;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    throw UnimplementedError();
  }

}

class StateStart extends StateWidget{
  final Function() onStart;
  final bool devFound;

  StateStart({Key? key, required this.onStart, required this.devFound}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    initSize(context, 200, 60);

    return !horz ? Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: _items(context, imgSz, devFound),
    ) : Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: _items(context, imgSz, devFound),
    );
  }

  _items(BuildContext context, double imgSz, devFound) {
    String text = devFound ? "Чтобы продолжить работу с найденным устройством "
        "нажмите кнопку запуска, затем переведите "
        "устройство в сервисный режим в течение 5 сек.\r\n"
        "Чтобы начать начать работу с новым устройством, нажмите кнопку сброса" :
    "Нажмите кнопку чтобы начать работу, затем переведите устройство в сервисный режим";

    return [
      IconButton(
        onPressed: onStart,
        iconSize: imgSz,
        icon: Image.asset("icon/start2.png", width: imgSz, height: imgSz, fit: BoxFit.fill,),
      ),
      Container(
        child: Text(text, style: const TextStyle(fontSize: 16), textAlign: TextAlign.center,),
        alignment: Alignment.center,
        margin: const EdgeInsets.all(32),
      )
    ];
  }
}

class StateScan extends StateWidget{
  StateScan({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    initSize(context, 210, 60);

    return Container(
      margin: const EdgeInsets.all(16),
      child: !horz ? Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: _items(context, imgSz),
      ) : Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: _items(context, imgSz),
      ),
    );
  }

  _items(BuildContext context, double imgSz) => [
    Image(image: const AssetImage("icon/radar4.png"), width: imgSz, height: imgSz, fit: BoxFit.fill, ),
    Container(
      child: const Text("Ожидание устройства...\r\nПереведите его в сервисный режим", style: TextStyle(fontSize: 16), textAlign: TextAlign.center),
      margin: const EdgeInsets.all(32),
    ),
  ];
}

class StateConnected extends StateWidget{
  final Function() onFlash;
  final Function() onSetts;
  final BleUart bleUart;
  final FWInfo fwInfo;
  final DevSetts devSetts;

  StateConnected({Key? key, required this.onFlash, required this.onSetts, required this.bleUart, required this.fwInfo, required this.devSetts}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    initSize(context, 250, 250);

    return !horz ? Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: _items(context, imgSz),
    ) : Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: _items(context, imgSz),
    );
  }

  _items(context, imgSz) {
    return [
      Container(
        margin: const EdgeInsets.only(bottom: 32, left: 32, right: 32),
        child: Card(
          elevation: 10,
          shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10))),
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              children: [
                Container(
                  child: Image.asset("icon/info2.png", width: 48, height: 48,),
                  margin: const EdgeInsets.only(right: 16),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("№ поезда: ${devSetts.train}-${devSetts.subTrain}"),
                    Text("№ системы: ${devSetts.system}"),
                    Text("Канал: ${devSetts.chan}"),
                    Text("Мощность: ${devSetts.tx_pwr < 128 ? devSetts.tx_pwr : devSetts.tx_pwr-256}dBm"),
                    Text("Индекс: ${devSetts.indx}"),
                    Text("Bootloader(BLE): v.${fwInfo.bootVerHex()}"),
                    Text("Bootloader: v.${fwInfo.boot2VerHex()}"),
                    Text("Основное ПО: v.${fwInfo.fwVerHex()}"),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
      Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          IconButton(
            onPressed: onFlash,
            iconSize: imgSz,
            icon: Image.asset("icon/flash2.png", width: imgSz, height: imgSz, fit: BoxFit.fill,),
            tooltip: "Обновление ПО",
            highlightColor: Theme.of(context).colorScheme.secondary,
            splashColor: Theme.of(context).colorScheme.secondary,
          ),
          const Text("Обновление ПО")
        ],
      ),
      Container(
        margin: const EdgeInsets.only(top: 32),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            IconButton(
              onPressed: onSetts,
              iconSize: imgSz,
              icon: Image.asset("icon/setts4.png", width: imgSz, height: imgSz, fit: BoxFit.fill,),
              tooltip: "Настройка",
              highlightColor: Theme.of(context).colorScheme.secondary,
              splashColor: Theme.of(context).colorScheme.secondary,
            ),
            const Text("Настройка")
          ],
        ),
      ),
    ];
  }
}

class StateFileInfo extends StateWidget{
  String _infoTxt = "";
  late final bool valid;
  final Function() onStart;
  final Function() onCancel;

  StateFileInfo({required List<String> info, required bool valid, required this.onStart, required this.onCancel}){
    this.valid = valid;
    for(int i=0; i<info.length; i++){
      _infoTxt += info[i]+"\r\n";
    }
  }

  @override
  Widget build(BuildContext context) {
    Size sz = MediaQuery.of(context).size;
    bool horz = sz.width > sz.height;
    double imgSz = (sz.width > sz.height ? sz.height - 250 : sz.width-160);
    if(imgSz > 200) imgSz = 200;
    initSize(context, 250, 160, maxSz: 200);

    var buttons = [
      ElevatedButton(
        child: Text("Отмена"),
        style: ButtonStyle(
            backgroundColor: MaterialStateProperty.resolveWith<Color?>((states)=> Theme.of(context).colorScheme.primary),
            elevation: MaterialStateProperty.resolveWith<double?>((states) => 10)
        ),
        onPressed: onCancel,
      ),
      valid ? ElevatedButton(
        child: Text("Загрузить"),
        style: ButtonStyle(
            backgroundColor: MaterialStateProperty.resolveWith<Color?>((states)=> Theme.of(context).colorScheme.secondary),
            elevation: MaterialStateProperty.resolveWith<double?>((states) => 10)
        ),
        onPressed: onStart,
      ) : Offstage()
    ];

    var items = [
      Image.asset("icon/flash2.png", width: imgSz, height: imgSz, fit: BoxFit.fill,),
      Container(
        margin: EdgeInsets.all(horz ? 32 : 32),
        child: Card(
          elevation: 10,
          shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10))),
          child: Container(
            padding: EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Информация о прошивке:", style: TextStyle(fontWeight: FontWeight.bold),),
                Text(_infoTxt),
              ],
            ),
          ),
        ),
      ),
      horz ? Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: buttons,
      ) : Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: buttons,
      )
    ];

    return !horz ? Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: items,
    ) : Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: items,
    );
  }

}

class StateFlash extends StateWidget{
  final double progress;
  final String stateText;

  StateFlash({Key? key, this.progress=0, this.stateText="Обновление ПО"}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    initSize(context, 250, 60);

    return Column(
      children: [
        Image(image: AssetImage("icon/flash2.png"), width: imgSz, height: imgSz, fit: BoxFit.fill,),
        Padding(
          padding: const EdgeInsets.all(32.0),
          child: LinearProgressIndicator(
            value: progress,
            minHeight: 10,
            color: Theme.of(context).colorScheme.secondary,
          ),
        )
      ],
    );
  }
}

class StateFlashCircular extends StateFlash{
  StateFlashCircular({Key? key, double progress=0, String stateText="Обновление ПО"}) : super(key: key, progress: progress, stateText: stateText);

  @override
  Widget build(BuildContext context){
    initSize(context, 270, 60);

    return CircularPercentIndicator(
      radius: imgSz,
      lineWidth: 10,
      percent: progress,
      center: Image(image: const AssetImage("icon/flash2.png"), width: imgSz-50, height: imgSz-50, fit: BoxFit.fill,),
      footer: Text("${(progress*100).round()}%", style: const TextStyle(fontSize: 30),),
      progressColor: Theme.of(context).colorScheme.secondary,
      arcBackgroundColor: Theme.of(context).primaryColor,
      arcType: ArcType.FULL,
      animation: false,
      header: Padding(
        padding: EdgeInsets.all(8.0),
        child: Text(stateText, style: TextStyle(fontSize: 16),),
      ),
    );
  }
}

class StateReset extends StateWidget {
  @override
  Widget build(BuildContext context) {
    initSize(context, 200, 60);

    return !horz ? Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: _items(context, imgSz),
    ) : Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: _items(context, imgSz),
    );
  }

  _items(BuildContext context, double imgSz) => [
    Image(image: const AssetImage("icon/reset1.png"), width: imgSz, height: imgSz, fit: BoxFit.fill,),
    Container(
      child: const Text("Перезагрузка устройства", style: TextStyle(fontSize: 16), textAlign: TextAlign.center),
      margin: const EdgeInsets.only(top: 32),
    ),
  ];
}

class StateWait extends StateWidget {
  @override
  Widget build(BuildContext context) {
    initSize(context, 200, 60);

    return !horz ? Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: _items(context, imgSz),
    ) : Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: _items(context, imgSz),
    );
  }

  _items(BuildContext context, double imgSz) => [
    Image(image: const AssetImage("icon/wait.png"), width: imgSz, height: imgSz, fit: BoxFit.fill,),
    Container(
      child: const Text("Опрос устройства...", style: TextStyle(fontSize: 16), textAlign: TextAlign.center),
      margin: const EdgeInsets.only(top: 32),
    ),
  ];
}


