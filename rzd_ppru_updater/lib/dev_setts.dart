import 'dart:typed_data';
import 'package:rzd_ppru_updater/bin_utils.dart';
import 'package:rzd_ppru_updater/crc16.dart';

class DevSetts {
  int rfu = 0;  //         8;//не исрользуется (0)
  int train = 0; //       14;//номер электрички (рейса)
  int subTrain = 0; //    2;//для разделения электричек одного маршрута
  int system = 0; //      8;//номер системы (0x24)
  int tx_period_us = 300;//пауза в микросекундах до следующего ретрансмита
  int retries_cnt = 2;//кол-во ретрансмитов
  int max_dev_indx = 11;//максимальный индекс устройства (Кол-во устройств - 1)
  int radio_timeslot_ms = 5;//величина таймслота ретранслятора
  int mode = 0x03;//0x03 не настраивается пользователем
  int chan = 0;//номер канала
  int indx = 0;//индекс
  int tx_pwr = 4;//мощность (записывается прямо в регистр, поэтому нужно преобразование, напр. -20дБм->0xEC, и т.д.)
  int last_ppru_delay_ms = 5;

  bool valid = true;

  Crc16 crc16 = Crc16();

  DevSetts({
      this.rfu = 0,
      this.train = 0,
      this.subTrain = 0,
      this.system = 0,
      this.tx_period_us = 0,
      this.retries_cnt = 0,
      this.max_dev_indx = 0,
      this.radio_timeslot_ms = 0,
      this.mode = 3,
      this.chan = 0,
      this.indx = 0,
      this.tx_pwr = 0,
      this.last_ppru_delay_ms = 0}); //время на ответ для последнего устройства

  DevSetts.fromBin(List<int> bin, int index){
    int crc = BinUtils.intFromBinLE(bin, index+16, 2);
    int crc_calc = crc16.calculate(Uint8List.fromList(bin.sublist(index, index+16)));
    if(crc != crc_calc) {
      valid = false;
      print("Not valid");
      return;
    }

    rfu = BinUtils.intFromBinLE(bin, index+0, 1);
    int trn = BinUtils.intFromBinLE(bin, index+1, 2);
    subTrain = (trn & 0xC000) >> 14;
    train = trn & ~(0xC000);
    system = BinUtils.intFromBinLE(bin, index+3, 1);

    tx_period_us = BinUtils.intFromBinLE(bin, index+4, 4);
    retries_cnt = BinUtils.intFromBinLE(bin, index+8, 1);
    max_dev_indx = BinUtils.intFromBinLE(bin, index+9, 1);
    radio_timeslot_ms = BinUtils.intFromBinLE(bin, index+10, 1);
    mode = BinUtils.intFromBinLE(bin, index+11, 1);
    chan = BinUtils.intFromBinLE(bin, index+12, 1);
    indx = BinUtils.intFromBinLE(bin, index+13, 1);
    tx_pwr = BinUtils.intFromBinLE(bin, index+14, 1);
    last_ppru_delay_ms = BinUtils.intFromBinLE(bin, index+15, 1);
  }

  DevSetts.copy(DevSetts other){
    valid = other.valid;

    rfu = other.rfu;
    subTrain = other.subTrain;
    train = other.train;
    system = other.system;

    tx_period_us = other.tx_period_us;
    retries_cnt = other.retries_cnt;
    max_dev_indx = other.max_dev_indx;
    radio_timeslot_ms = other.radio_timeslot_ms;
    mode = other.mode;
    chan = other.chan;
    indx = other.indx;
    tx_pwr = other.tx_pwr;
    last_ppru_delay_ms = other.last_ppru_delay_ms;
  }

  Uint8List toBin(){
    Uint8List bin = Uint8List(32);

    BinUtils.intToBinLE(bin, 0, 1, rfu);//
    int trn = train | (subTrain << 14);
    BinUtils.intToBinLE(bin, 1, 2, trn);
    BinUtils.intToBinLE(bin, 3, 1, system);

    BinUtils.intToBinLE(bin, 4, 4, tx_period_us);
    BinUtils.intToBinLE(bin, 8, 1, retries_cnt);
    BinUtils.intToBinLE(bin, 9, 1, max_dev_indx);
    BinUtils.intToBinLE(bin, 10, 1, radio_timeslot_ms);
    BinUtils.intToBinLE(bin, 11, 1, 3);//mode
    BinUtils.intToBinLE(bin, 12, 1, chan);
    BinUtils.intToBinLE(bin, 13, 1, indx);
    BinUtils.intToBinLE(bin, 14, 1, tx_pwr);
    BinUtils.intToBinLE(bin, 15, 1, last_ppru_delay_ms);

    Uint8List forCRC = bin.sublist(0, 16);
    int crc = crc16.calculate(forCRC);
    print("forCRC = $forCRC");
    print("CRC16 = $crc");
    BinUtils.intToBinLE(bin, 16, 2, crc);

    return bin;
  }

  String toString(){
    return "rfu: $rfu\r\n"
        "train: $train\r\n"
        "subTrain: $subTrain\r\n"
        "system: $system\r\n"
        "tx_period_us: $tx_period_us\r\n"
        "retries_cnt: $retries_cnt\r\n"
        "max_dev_indx: $max_dev_indx\r\n"
        "mode: $mode\r\n"
        "chan: $chan\r\n"
        "indx: $indx\r\n"
        "tx_pwr: $tx_pwr\r\n"
        "last_ppru_delay_ms: $last_ppru_delay_ms";
  }

  bool equal(DevSetts other){
    if(rfu != other.rfu)
      return false;
    if(subTrain != other.subTrain)
      return false;
    if(train != other.train)
      return false;
    if(system != other.system)
      return false;

    if(tx_period_us != other.tx_period_us)
      return false;
    if(retries_cnt != other.retries_cnt)
      return false;
    if(max_dev_indx != other.max_dev_indx)
      return false;
    if(radio_timeslot_ms != other.radio_timeslot_ms)
      return false;
    if(mode != other.mode)
      return false;
    if(chan != other.chan)
      return false;
    if(indx != other.indx)
      return false;
    if(tx_pwr != other.tx_pwr)
      return false;
    if(last_ppru_delay_ms != other.last_ppru_delay_ms)
      return false;

    return true;
  }

  String toCsv() {
    return "$train,$subTrain,$system,$chan,$tx_pwr,$max_dev_indx,$indx,$radio_timeslot_ms,$tx_period_us,$retries_cnt,$last_ppru_delay_ms";
  }
}
