import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/services.dart';
import 'package:flutter_reactive_ble/flutter_reactive_ble.dart';
import 'package:permission_handler/permission_handler.dart';

// import 'package:flutter_ble/characteristic.dart';
// import 'package:flutter_ble/conn_state_update.dart';
// import 'package:flutter_ble/flutter_ble.dart';
// import 'package:flutter_ble/uuid.dart';

import 'package:rzd_ppru_updater/bin_utils.dart';
import 'package:rzd_ppru_updater/dev_setts.dart';
import 'package:rzd_ppru_updater/hex_converter.dart';

class BleUart {
  static const ANS_OK = 0;
  static const ANS_READDATA = 1;
  static const ERR_CRC = 2;
  static const ERR_BADREQ = 3;
  static const ERR_MEMCRC = 4;
  static const ANS_INFO = 5;
  static const ANS_BKT_PROGRESS = 6;

  static const platform = MethodChannel('com.mclaught.ppru_updater/main');
  
  FlutterReactiveBle flutterReactiveBle = FlutterReactiveBle();
  // final FlutterBle flutterReactiveBle = FlutterBle();
  final bool _clear_on_stop = false;
  final serviceId = Uuid.parse("6E400001-B5A3-F393-E0A9-E50E24DCCA9E");
  final charRxId = Uuid.parse("6E400002-B5A3-F393-E0A9-E50E24DCCA9E");
  final charTxId = Uuid.parse("6E400003-B5A3-F393-E0A9-E50E24DCCA9E");
  DiscoveredDevice? _dev;
  late QualifiedCharacteristic charRx;
  late QualifiedCharacteristic charTx;
  StreamController<BleDevState> _onConnect = StreamController();
  StreamController<Progress> _onProgress = StreamController();
  StreamController<DevSetts> _onDevSetts = StreamController();
  StreamController<FWInfo> _onFWInfo = StreamController();
  bool _connected = false;
  bool _started = false;
  int _mtu = 247;
  List<int> _sendBuf = [];
  int qIndex = 0;
  List<Uint8List> queue = [];
  int err_cnt = 0;
  Timer? timeout;
  late StreamSubscription<ConnectionStateUpdate> _connection;

  Stream<BleDevState> get onConnect => _onConnect.stream;

  Future<int> androidSDK() async {
    if(Platform.isAndroid) {
      int res = await platform.invokeMethod("AndroidSDK");
      // print("SDK = ${Platform.operatingSystemVersion} ($res)");
      return res;
    }else{
      return 31;
    }
  }

  Future<bool> requestBluetoothPermissions() async {
    bool res = true;

    if(Platform.isAndroid) {

      if(await androidSDK() >= 31) {
        if (!await Permission.bluetoothConnect.request().isGranted) {
          res = false;
        }
        if (!await Permission.bluetoothScan.request().isGranted) {
          res = false;
        }
      }else{
        if(!await Permission.location.request().isGranted){
          res = false;
        }
      }
    }else if(Platform.isIOS){
      //TODO: Добавить проверки для iOs
    }
    // bool res = await platform.invokeMethod("RequestBluetoothPermissions");
    return res;
  }

  Future<void> startLocationIntent()async{
    if(Platform.isAndroid) {
      await platform.invokeMethod("startLocationIntent");
    }
  }

  Future<void> start() async {
    _started = true;

    // _scanForDevs();
    
    if(_dev == null) {
      _scanForDevs();
    }else{
      _onConnect.add(BleDevState(BleDevStates.scaning, _dev));
      _connect(withscan: false);
    }
  }

  Future<void> stop() async {
    print("STOP");

    if(_connection != null){
      _connection.cancel();
    }

    if(_clear_on_stop) {
      clear();
    }
    _started = false;

    _onConnect.add(BleDevState(BleDevStates.disconnected, _dev));
  }

  DiscoveredDevice? get dev => _dev;

  bool get started => _started;

  bool get connected => _connected;

  Future<void> _scanForDevs()async{
    print("SCAN");

    _dev = null;

    _onConnect.add(BleDevState(BleDevStates.scaning, _dev));

    flutterReactiveBle.scanForDevices(withServices: [serviceId], scanMode: ScanMode.lowLatency, requireLocationServicesEnabled: true).listen((device) {
      if(_dev == null) {
        _dev = device;
        print("DEV: " + device.name);

        if(_started) {
          _connect();
        }
      }
    }, onError: (error) {
      print("SCAN ERROR: "+error.toString());
      _onConnect.add(BleDevState(BleDevStates.disconnected, _dev));
    });

  }

  void _connect({bool withscan = false})async{
    print("CONNECT");

    if(_dev == null)
      return;

    Stream<ConnectionStateUpdate> stream;

    if(withscan) {
      stream = flutterReactiveBle.connectToAdvertisingDevice(
        id: _dev?.id ?? "",
        withServices: [serviceId],
        prescanDuration: const Duration(seconds: 5),
        servicesWithCharacteristicsToDiscover: {
          serviceId: [charRxId, charTxId]
        },
        connectionTimeout: const Duration(seconds: 5),
      );
    }else{
      stream = flutterReactiveBle.connectToDevice(
        id: _dev?.id ?? "",
        servicesWithCharacteristicsToDiscover: {serviceId: [charRxId, charTxId]},
        connectionTimeout: const Duration(seconds: 5),
      );
    }
    if(stream != null) {
      _connection = stream.listen((connectionState) {
        print("CONN STATE: " + connectionState.connectionState.toString());

        switch (connectionState.connectionState) {
          case DeviceConnectionState.connecting:
            _onConnect.add(BleDevState(BleDevStates.connecting, _dev));
            break;
          case DeviceConnectionState.disconnecting:
            _onConnect.add(BleDevState(BleDevStates.disconnecting, _dev));
            break;
          // case DeviceConnectionState.connected:
          //   bleState = BleDevStates.connected;
          //   _onConnect.add(BleDevState(bleState, _dev));
          //   break;
          case DeviceConnectionState.disconnected:
            _connected = true;
            var last_started = _started;
            stop();
            Future.delayed(Duration(milliseconds: 300)).then((value) => start());
            break;
        }

        if (connectionState.connectionState == DeviceConnectionState.connected) {
          charRx = QualifiedCharacteristic(serviceId: serviceId,
              characteristicId: charRxId,
              deviceId: _dev?.id ?? "");
          charTx = QualifiedCharacteristic(serviceId: serviceId,
              characteristicId: charTxId,
              deviceId: _dev?.id ?? "");
          flutterReactiveBle.requestMtu(deviceId: _dev?.id ?? "", mtu: 256).then((value){
            _mtu = value;
            print("MTU: ${_mtu}");
            _onConnect.add(BleDevState(BleDevStates.connected, _dev));
          });
          _connected = true;

          flutterReactiveBle.subscribeToCharacteristic(charTx).listen((data) {
            // String str = String.fromCharCodes(data);
            print("READ: " + data.toString());
            _onRead(data);
          },
              onError: (error) {
                print("CHAR ERROR: " + error.toString());
                //_onConnect.add(BleDevState(BleDevStates.disconnected, _dev));
              }
          );


        }

        // if (connectionState.connectionState == DeviceConnectionState.disconnected) {
        //   _connected = false;
        //   stop();
        // }
      },
      onError: (error, stack) {
        print("CONNECTION ERROR: " + error.toString());
        stop();
        // _onConnect.add(BleDevState(BleDevStates.disconnected, _dev));
      });
    }
  }

  Future<void> _write(Uint8List data) async {
    if(_connected && charRx != null) {
      print("WRITE: ${data}");
      await flutterReactiveBle.writeCharacteristicWithResponse(charRx, value: data);
    }
  }

  Future<void> _writeStr(String str)async{
    var data = Uint8List.fromList(str.codeUnits);
    await _write(data);
  }

  void _addToQueue(Uint8List data){
    queue.add(data);
  }

  void _onRead(List<int> data) {
    if(data.length >= 4) {
      int code = data[3] >> 4;

      switch(code){
        case ANS_OK:
          err_cnt = 0;
          qIndex++;
          _onProgress.add(Progress(qIndex, queue.length, complete: qIndex == queue.length));
          if (qIndex < queue.length) {
            _write(queue[qIndex]);
          }
          break;

        case ERR_CRC:
          err_cnt++;
          if(err_cnt < 10) {
            print("ERROR ${data[0]}");
            _write(queue[qIndex]);
          }else{
            // _onProgress.add(Progress(qIndex, queue.length, error: true, errorStr: "Ошибка контрольной суммы"));
            _onProgress.addError("Ошибка контрольной суммы");
          }
          break;

        case ANS_READDATA:
          DevSetts devSetts = DevSetts.fromBin(data, 4);
          _onDevSetts.add(devSetts);
          break;

        case ERR_MEMCRC:
          // _onProgress.add(Progress(qIndex, queue.length, error: true, errorStr: "Ошибка контрольной суммы"));
          _onProgress.addError("Ошибка контрольной суммы");
          break;

        case ANS_INFO:
          FWInfo info = FWInfo.fromBin(Uint8List.fromList(data));
          _onFWInfo.add(info);
          if(timeout!=null){
            timeout?.cancel();
          }
          break;

        case ANS_BKT_PROGRESS:
          if(data.length >= 12) {
            int progress = BinUtils.intFromBinLE(data, 4, 4);
            int result = BinUtils.intFromBinLE(data, 8, 4);
            if (result == 0) {
              _onProgress.add(Progress(progress, 100,
                  complete: false, device: 1));
            }else if(result == 1){
              _onProgress.add(Progress(100, 100,
                  complete: true, device: 1));
            } else {
              _onProgress.addError("RS485 ${_RS485Error(result)}");
              print("Ошибка БКТЭ ${result}");
            }
          }
          break;
      }
    }
  }

  void _startQueue(){
    if(!queue.isEmpty){
      qIndex = 0;
      _write(queue[0]);
    }else{
      print("QUEUE IS EMPTY");
    }
  }

  Future<List<String>> loadFile(String? path) async {
    File file = File(path ?? "");
    if(!file.existsSync()) {
      throw Exception("Файл не найден");
    }

    var lines = await file.readAsLines();
    print("READ FILE: "+lines.length.toString());

    return lines;
  }

  Future<dynamic> assetsList()async{
    dynamic assets = jsonDecode(await rootBundle.loadString("assets/firmwares.json"));
    return assets;
  }

  Future<List<String>> loadFileFromAssets(String? name) async {
    if(name==null)
      return [];

    String str = await rootBundle.loadString(name);

    var lines = str.split("\r\n");
    print("READ FILE: "+lines.length.toString());

    return lines;
  }

  Future<Stream<Progress>?> sendFile(List<String> lines)async {
    _onProgress = StreamController();

    if(lines.isNotEmpty) {
      try {
        HexConverter converter = HexConverter(_mtu);
        queue = await converter.convertLines(lines);
        if (queue.isNotEmpty) {
          _startQueue();
        }
      } catch (e) {
        _onProgress.addError(e.toString());
      }
    }else{
      _onProgress.addError("Нет данных для отправки");
    }

    return _onProgress.stream;
  }

  Stream<DevSetts> readSettings(int addr){
    _onDevSetts = StreamController();

    HexConverter converter = HexConverter(_mtu);
    queue = converter.makeReadCmd(addr, 18);
    _startQueue();

    return _onDevSetts.stream;
  }

  Stream<Progress> writeSettings(DevSetts devSetts) {
    _onProgress = StreamController();

    HexConverter converter = HexConverter(_mtu);
    queue = converter.makeWriteSettsCmd(0x7F000, Uint8List.fromList(devSetts.toBin()));
    _startQueue();

    return _onProgress.stream;
  }

  Stream<Progress> reboot() {
    _onProgress = StreamController();

    // _onConnect.add(BleDevState(BleDevStates.reset, _dev));

    HexConverter converter = HexConverter(_mtu);
    queue = converter.makeResetCmd();
    _startQueue();

    return _onProgress.stream;
  }

  void clear(){
    print("CLEAR");
    _dev = null;
    stop();
    // _onConnect.add(BleDevState(BleDevStates.disconnected, _dev));
  }

  Stream<FWInfo> getFWInfo(){
    _onFWInfo = StreamController();

    print("GET INFO");
    HexConverter converter = HexConverter(_mtu);
    queue = converter.makeInfoCmd();
    _startQueue();

    // timeout = Timer(Duration(milliseconds: 300), () => stop());

    return _onFWInfo.stream;
  }

  void destroy() {
    // flutterReactiveBle.deinitialize();
  }
}

String _RS485Error(int result) {
  switch(result){
    case 0xFFFFFFFF:
      return "нет ответа от устройства";
    case 0xFFFFFFFE:
      return "ошибка перехода в сервисный режим";
    case 0xFFFFFFFD:
      return "файл не принят устройством";
    case 0xFFFFFFFC:
      return "ошибка записи блока";
    case 0xFFFFFFFB:
      return "ошибка обновления";
  }

  return "ошибка";
}

class FWInfo {
  late int boot_ver;
  late int boot2_ver;
  late int fw_ver;

  FWInfo({this.boot_ver=0, this.boot2_ver=0, this.fw_ver=0});

  FWInfo.fromBin(Uint8List data){
    if(data.length >= 16) {
      boot_ver = BinUtils.intFromBinLE(data, 4, 4);
      boot2_ver = BinUtils.intFromBinLE(data, 8, 4);
      fw_ver = BinUtils.intFromBinLE(data, 12, 4);
    }else{
      boot_ver = BinUtils.intFromBinLE(data, 4, 4);
      boot2_ver = BinUtils.intFromBinLE(data, 8, 4);
      fw_ver = 0;
    }
  }

  String bootVerHex() {
    return boot_ver != 0 && boot_ver != 0xFFFFFFFF ? boot_ver.toRadixString(16) : "...";
  }

  String fwVerHex() {
    return fw_ver!=0 && fw_ver != 0xFFFFFFFF ? fw_ver.toRadixString(16) : "...";
  }

  String boot2VerHex() {
    return boot2_ver != 0 && boot2_ver != 0xFFFFFFFF ? boot2_ver.toRadixString(16) : "...";
  }
}

class Progress {
  final int progress;
  final int total;
  // final bool error;
  // final String errorStr;
  final bool complete;
  int device;

  Progress(this.progress, this.total, {this.complete = false, this.device=0});
}

enum BleDevStates{
  scaning,
  connecting,
  connected,
  disconnecting,
  disconnected,
  reset
}

class BleDevState {
  final BleDevStates _state;
  String _text = "";
  final DiscoveredDevice? _device;

  BleDevState(this._state, this._device, {String text = ""}){
    if(text == "") {
      switch (_state) {
        case BleDevStates.scaning:
          _text = "Ожидание";
          break;
        case BleDevStates.connected:
          _text = "Подключено";
          break;
        case BleDevStates.disconnected:
          _text = "Отключено";
          break;
        case BleDevStates.connecting:
          _text = "Подключение";
          break;
        case BleDevStates.disconnecting:
          _text = "Отключение";
          break;
        // case BleDevStates.reset:
        //   _text = "Перезагрузка";
        //   break;
      }
    }else{
      _text = text;
    }
  }

  DiscoveredDevice? get device => _device;

  String get text => _text;

  BleDevStates get state => _state;
}