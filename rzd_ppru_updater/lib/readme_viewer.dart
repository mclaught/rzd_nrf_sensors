import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

class ReadmeViewer extends StatefulWidget{
  final String text;

  const ReadmeViewer({Key? key, required this.text}) : super(key: key);

  @override
  State<ReadmeViewer> createState() => _ReadmeViewerState();

}

class _ReadmeViewerState extends State<ReadmeViewer> {
  String html = "";

  @override
  void initState() {
    setState((){
      print(widget.text);
      List<String> strs = widget.text.split("\n");
      for(String str in strs){
        str = str.replaceAll("\r", "");
        html += "<p>$str</p>";
      }
      print(html);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Readme"),
      ),
      body: SingleChildScrollView(
        child: Container(
          // child: Text(widget.text),
          child: Html(data: html,),
          margin: EdgeInsets.all(5),
        ),
      ),
    );
  }
}