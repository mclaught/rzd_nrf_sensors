import 'dart:typed_data';

class BinUtils{
  static int intFromBinLE(List<int> bin, int index, int size){
    if((index+size) > bin.length)
      return 0;

    int x = 0;
    for(int i=0; i<size; i++){
      x |= (bin[i+index] << (i*8));
    }
    return x;
  }

  static void intToBinLE(List<int> bin, int index, int size, int val){
    for(int i=index; i<(index+size); i++){
      bin[i] = val & 0xFF;
      val >>= 8;
    }
  }
}