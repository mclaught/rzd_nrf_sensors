import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:package_info/package_info.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:rzd_ppru_updater/ble_uart.dart';
import 'package:rzd_ppru_updater/crc16.dart';
import 'package:rzd_ppru_updater/dev_setts.dart';
import 'package:rzd_ppru_updater/hex_converter.dart';
import 'package:rzd_ppru_updater/log.dart';
import 'package:rzd_ppru_updater/readme_viewer.dart';
import 'package:rzd_ppru_updater/setts_page.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:rzd_ppru_updater/state_widgets.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Настройка устройства',
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale('en', ''),
        Locale('ru', ''),
      ],
      theme: ThemeData(
        // primarySwatch: Colors.blue,
        colorScheme: ColorScheme.fromSwatch(
          primarySwatch: Colors.blue,
        ).copyWith(
          secondary: Colors.orange,
          background: Colors.blue.shade50
        ),
      ),
      home: const MyHomePage(title: 'Настройка 127'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  BleDevStates _bleState = BleDevStates.disconnected;
  String _bleDevice = "";
  BleUart bleUart = BleUart();
  double _progress = 0;
  Widget _stateWidget = StateStart(onStart: (){}, devFound: false,);
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  FWInfo fwInfo = FWInfo(boot_ver: 0, fw_ver: 0);
  bool _reconnect = false;
  DevSetts _devSetts = DevSetts();
  CsvLog log = CsvLog();
  String _readmeText = "";

  @override
  void initState() {
    super.initState();
    // _timer = Timer.periodic(Duration(milliseconds: 1000), _onTimer);

    // Crc16 crc16 = Crc16();
    // Uint8List testCrc = Uint8List.fromList([0xFF,0x02,0x40,0x01,0x64,0x00,0x00,0x00,0x0A,0x0A,0x05,0x03,0x01,0x01,0x04,0x0A]);//Uint8List.fromList("123456789".codeUnits);
    // print("Test CRC-16: ${crc16.calculate(testCrc).toRadixString(16).toUpperCase()}");

    _stateWidget = StateStart(onStart: _start, devFound: bleUart.dev!=null,);

    bleUart.onConnect.listen((state) {
      print("STATE: ${state.text}");

      _bleState = state.state;
      _bleDevice = state.device?.name ?? "";

      switch(state.state){
        case BleDevStates.scaning:
          setState(() => _stateWidget = StateScan());
          break;
        case BleDevStates.connecting:
          setState(() => _stateWidget = StateScan());
          break;
        case BleDevStates.connected:
          Future.delayed(Duration(milliseconds: 1000)).then((value) {
            bleUart.getFWInfo().listen((info){
              fwInfo = info;
              _read_setts();
              // _showConnectedState();
            });
          });
          // _showConnectedState(progress: true);
          setState(() {
            _stateWidget = StateWait();
          });
          break;
        case BleDevStates.disconnecting:
          setState(() => _stateWidget = StateStart(onStart: _start, devFound: bleUart.dev!=null,));
          break;
        case BleDevStates.disconnected:
          setState(() => _stateWidget = StateStart(onStart: _start, devFound: bleUart.dev!=null,));
          // if(_reconnect){
          //   Timer(Duration(milliseconds: 100), () => _start());
          // }
          // bleUart.start();
          // setState(() => _stateWidget = StateScan());
          //Timer(Duration(milliseconds: 1000), () => bleUart.start());
          // ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Устройство отключено")));
          break;
      // case BleDevStates.reset:
      //   break;
        case BleDevStates.reset:
          setState(() => _stateWidget = StateReset());
          break;
      }
    });

    rootBundle.loadString("assets/Readme.txt")
      .then((value) => setState(() => _readmeText = value))
      .catchError((error) => setState(() => _readmeText = "Информация отсутствует"));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: ()async {
        bool? res = await showDialog<bool>(context: context, builder: (dialogContext) => AlertDialog(
          title: const Text("Завершить работу с приложением?"),
          content: const Text("Текущее соединение будет разорвано, а устройство перезагружено в рабочий режим."),
          actions: [
            TextButton(onPressed: (){
              Navigator.of(dialogContext).pop(false);
            }, child: Text("Отмена")),
            TextButton(onPressed: (){
              Navigator.of(dialogContext).pop(true);
            }, child: Text("Завершить")),
          ],
        ));
        if(res ?? false){
          exit(0);
        }
        return false;
      },
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text(widget.title),
          actions: [
            _readmeText.isNotEmpty ? IconButton(icon: Icon(Icons.info_outline), onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => ReadmeViewer(text: _readmeText)
                )
              );
            },) : Offstage(),
            IconButton(onPressed: (){
              showDialog(context: context, builder: (dlgCtx) => AlertDialog(
                title: Text("Отчет"),
                content: Text("Отправить отчет"),
                actions: [
                  TextButton(onPressed: (){
                    Navigator.of(dlgCtx).pop();
                    log.sendLog("${_devSetts.train}-${_devSetts.subTrain}", _bleDevice);
                  }, child: Text("Для устройства"),),
                  TextButton(onPressed: (){
                    Navigator.of(dlgCtx).pop();
                    log.sendTrainLog("${_devSetts.train}-${_devSetts.subTrain}");
                  }, child: Text("Для состава"),),
                  TextButton(onPressed: (){
                    Navigator.of(dlgCtx).pop();
                  }, child: Text("Отмена"),),
                ],
              ));
            }, icon: const Icon(Icons.share),),
            IconButton(onPressed: () {
              _about();
            }, icon: const Icon(Icons.help_outline))
          ],
        ),
        backgroundColor: Theme.of(context).colorScheme.background,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(),
              _stateWidget,
              const Text("")
              // Card(
              //   child: Padding(
              //     padding: const EdgeInsets.all(16.0),
              //     child: ,
              //   ),
              //   margin: EdgeInsets.all(8),
              //   elevation: 10,
              // ),

            ],
          ),
        ),
        bottomNavigationBar: BottomAppBar(
          elevation: 25,
          shape: _resetIsVisible() ? const CircularNotchedRectangle() : null,
          color: Theme.of(context).primaryColor,
          clipBehavior: Clip.antiAlias,
          child: Container(
            padding: const EdgeInsets.only(top: 12, bottom: 12, left: 24, right: 24),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                // _bleDevice.isNotEmpty ? const Text("Устройство:", style: TextStyle(fontSize: 18, color: Colors.white),) : Offstage(),
                _bleDevice.isNotEmpty ? Image.asset("icon/device.png", height: 48, width: 48,) : Offstage(),
                Container(
                  margin: const EdgeInsets.only(left: 20),
                  child: Text(
                    _bleDevice,
                    style: const TextStyle(fontSize: 16, color: Colors.white),
                  ),
                ),
                const Text("")
              ],
            ),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
        floatingActionButton: _resetIsVisible() ? FloatingActionButton(
          onPressed: () => _cancel(),
          child: const Icon(Icons.refresh, size: 40,),
          elevation: 10,
          tooltip: "Перезагрузка",
        ) : const Offstage(),
      ),
    );
  }

  _showConnectedState(){
    setState(() {
      _stateWidget = StateConnected(onFlash: _flash, onSetts: _read_setts_for_edit, bleUart: bleUart, fwInfo: fwInfo, devSetts: _devSetts,);
    });
  }

  Future<void> _start() async {
    if(Platform.isAndroid && await bleUart.androidSDK()<31 && !await Geolocator.isLocationServiceEnabled()){
      showDialog(context: context, builder: (dlgContext) => AlertDialog(
        title: Row(
          children: [
            Image.asset("icon/location.png", width: 64, height: 64,),
            Expanded(child: Text("Геолокация")),
          ],
        ),
        content: Text("Для работы с BLE-устройствами Android требует включить геолокацию в настройках телефона.\r\n\r\n"
            "Включите геолокацию и повторите попытку."),
        actions: [
          TextButton(onPressed: (){
            bleUart.startLocationIntent();
            Navigator.of(dlgContext).pop();
          }, child: Text("Перейти к настройкам"))
        ],
      ));
      return;
    }

    // if(!await Permission.location.status.isGranted){
    //   bool? res = await showDialog<bool>(context: context, builder: (BuildContext context) => AlertDialog(
    //     title: const Text("Внимание!"),
    //     content: const Text("Для доступа к управлению bluetooth-устройствами Android требует разрешения на доступ к точному местоположению."),
    //     actions: [
    //       TextButton(child: const Text("Продолжить"), onPressed: () {
    //         Navigator.of(context).pop(true);
    //       },)
    //     ],
    //   ));
    //   if(!(res ?? false)){
    //     return;
    //   }
    //   if(!await Permission.location.request().isGranted) {
    //     return;
    //   }
    // }

    if(!await bleUart.requestBluetoothPermissions()){
      return;
    }

    bleUart.start();
    _reconnect = true;
  }

  _flash()async{
    showDialog(context: context, builder: (dlgCtx) => AlertDialog(
      title: Text("Прошивки"),
      content: Text("Выбрать прошивку:"),
      actions: [
        TextButton(onPressed: (){
          Navigator.of(dlgCtx).pop();
          _flashFromFile();
        }, child: Text("Из внешнего файла")),
        TextButton(onPressed: (){
          Navigator.of(dlgCtx).pop();
          _flashFromAssets();
        }, child: Text("Встроенная")),
        TextButton(onPressed: (){
          Navigator.of(dlgCtx).pop();
        }, child: Text("Отмена")),
      ],
    ));
  }

  _flashFromFile() async {
    var perm1 = await Permission.storage.request();
    var perm2 = await Permission.manageExternalStorage.status;
    if(await bleUart.androidSDK() < 30){
      perm2 = PermissionStatus.granted;
    }

    if(perm2 != PermissionStatus.granted && Platform.isAndroid){
      bool? res = await showDialog(builder: (context) => AlertDialog(
        title: const Text("Разрешения"),
        content: const Text("Предоставьте приложению разрешение на доступ ко всем файлам"),
        actions: [
          TextButton(onPressed: () {
            Navigator.of(context).pop(true);
            // const AndroidIntent(
            //   action: "android.settings.MANAGE_ALL_FILES_ACCESS_PERMISSION",
            // ).launch();
          }, child: const Text("Продолжить"))
        ],
      ), context: context,);
      if(res ?? false){
        perm2 = await Permission.manageExternalStorage.request();
      }
    }
    if(perm1!=PermissionStatus.granted || perm2!=PermissionStatus.granted){
      print("STORAGE not granted: $perm1 $perm2");
      return;
    }

    FilePickerResult? result = await FilePicker.platform.pickFiles(
      dialogTitle: "Выбор файла прошивки",
    );
    print("RESULT: $result");
    if(result != null){
      try {
        var lines = await bleUart.loadFile(result.files.single.path);
        _flashLines(lines);
      }catch(e){
        setState(() {
          _stateWidget = StateFileInfo(
              info: [e.toString()],
              valid: false,
              onStart: () {},
              onCancel: _cancelSendLines
          );
        });
      }
    }else{

      _showConnectedState();

    }
  }

  _flashFromAssets()async{
    List<dynamic> assets = await bleUart.assetsList();
    if(assets.isEmpty)
      return;

    showDialog(context: context, builder: (dlgCtx) =>AlertDialog(
      title: Text("Выбор файла"),
      content: Container(
        width: MediaQuery.of(context).size.width-50,
        height: 300,
        child: ListView(
          children: assets.map<ListTile>((e) => ListTile(
            title: Text(e['name']),
            subtitle: Text(e['version']),
            onTap: () async {
              Navigator.of(dlgCtx).pop();
              var lines = await bleUart.loadFileFromAssets(e['file']);
              _flashLines(lines);
            },
          )).toList(),
        ),
      ),
      actions: [
        TextButton(child: Text('Отмена'), onPressed: (){
          Navigator.of(dlgCtx).pop();
        },)
      ],
    ));
  }

  _flashLines(lines){
    var info = HexConverter(247).getFileInfo(lines);
    setState(() {
      _stateWidget = StateFileInfo(
          info: info,
          valid: true,
          onStart: () => _startSendLines(lines),
          onCancel: _cancelSendLines
      );
    });
  }

  _startSendLines(List<String> lines)async {
    try {
      (await bleUart.sendFile(lines))?.listen((event) {

        _progress = event.total != 0 ? event.progress / event.total : 0;

        if(event.complete) {
          bleUart.getFWInfo().listen((info){
            fwInfo = info;
            _showConnectedState();
          });
          _showConnectedState();
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text("Загрузка успешно завершена"),
          ));
        }else {
          setState(() => _stateWidget = StateFlashCircular(progress: _progress, stateText: event.device==0 ? "Загрузка ПО" : "Загрузка через RS485",));
        }

      }, onError: (error){
        _showConnectedState();

        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(error.toString()),
        ));
      });
    }catch(e){
      _showConnectedState();
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(e.toString())));
    }
  }

  _cancelSendLines(){
    _showConnectedState();
  }

  _read_setts(){
    bleUart.readSettings(0x7F000).listen((devSetts) async {
      print("SETTINGS: $devSetts");
      _devSetts = devSetts;
      log.addToLog(_bleDevice, _devSetts);//_bleDevice+","+_devSetts.toCsv()
      _showConnectedState();
    });
  }

  _read_setts_for_edit() async {
    DevSetts result = await Navigator.push(context, MaterialPageRoute(builder: (context) => SettsPage(DevSetts.copy(_devSetts)),));
    print("NEW SETTINGS: $result");

    if(!result.equal(_devSetts)) {
      _devSetts = result;
      _showConnectedState();
      _saveSetts(result);
    }

    // bleUart.readSettings(0x7F000).listen((devSetts) async {
    //   print("SETTINGS: $devSetts");
    //
    //   DevSetts result = await Navigator.push(context, MaterialPageRoute(builder: (context) => SettsPage(DevSetts.copy(devSetts)),));
    //   print("NEW SETTINGS: $result");
    //
    //   if(!result.equal(devSetts)) {
    //     _saveSetts(result);
    //   }
    // });
  }

  void _saveSetts(DevSetts devSetts) {
    showDialog(
        context: context,
        builder: (dialogContext) => AlertDialog(
          title: const Text("Сохранение настроек"),
          content: const Text("Сохранить настройки в устройстве?"),
          actions: [
            TextButton(
                onPressed: () {
                  Navigator.of(dialogContext).pop();
                },
                child: const Text("Отмена")),
            TextButton(
                onPressed: () {
                  Navigator.of(dialogContext).pop();

                  bleUart.writeSettings(devSetts).listen((progress) {
                    if (progress.complete) {
                      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                        content: Text("Параметры сохранены"),
                        duration: Duration(seconds: 3),
                      ));
                    }
                  }, onError: (error) {
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                      content: Text("Ошибка при сохранении"),
                      duration: Duration(seconds: 3),
                    ));
                  });
                },
                child: const Text("Сохранить"))
          ],
        )
    );
  }

  void _cancel()async {
    print("CANCEL: $_bleState");
    if (_bleState == BleDevStates.connected) {
      bool? res = await showDialog<bool>(context: context, builder: (context) => AlertDialog(
        title: const Text("Завершение работы"),
        content: const Text("Работа с устройством будет завершена.\r\nУстройство будет перезагружено в рабочий режим."),
        actions: [
          TextButton(onPressed: () {
            Navigator.of(context).pop(true);
          }, child: const Text("Завершить")),
          TextButton(onPressed: () {
            Navigator.of(context).pop(false);
          }, child: const Text("Отмена"),)
        ],
      ),);
      if(res ?? false) {
        _reboot();
      }
    } else if(_bleState != BleDevStates.disconnected){
      setState(() {
        _bleState = BleDevStates.disconnected;
        _stateWidget = StateStart(onStart: _start, devFound: bleUart.dev!=null,);
        bleUart.stop();
      });
    }else{
      bleUart.clear();
    }
  }

  Future<void> _reboot() async {
    _reconnect = false;

    setState(() {
      _bleState = BleDevStates.reset;
      _stateWidget = StateReset();
    });

    Timer timeout = Timer(Duration(milliseconds: 3000), () => bleUart.stop());

    bleUart.reboot().listen((event) async {
      if(event.complete){
        timeout.cancel();
        await bleUart.stop();
      }
    }, onError: (error){
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("Ошибка при перезагрузке устройства")));
    });

    // print("START");
    // Timer(Duration(milliseconds: 500), () {
    //   bleUart.clear();
    //   bleUart.start();
    // });
  }

  bool _resetIsVisible() {
    return bleUart.dev!=null || _bleState == BleDevStates.scaning;
  }

  Future<void> _about() async {
    PackageInfo info = await PackageInfo.fromPlatform();

    showDialog(context: context, builder: (BuildContext context) => AboutDialog(
      applicationIcon: Image.asset("icon/launcher_icon.png", width: 64, height: 64,),
      applicationName: info.appName,
      applicationLegalese: "ООО Элком, 2021",
      applicationVersion: "${info.version} (${info.buildNumber})",
    ), );
  }

}
