import 'package:flutter/material.dart';
import 'package:rzd_ppru_updater/dev_setts.dart';

class SettsPage extends StatefulWidget{
  final DevSetts setts;

  SettsPage(this.setts);

  @override
  State<SettsPage> createState() => _SettsPageState();
}

class _SettsPageState extends State<SettsPage>{

  @override
  void initState() {
    Future.delayed(Duration(milliseconds: 100)).then((value){
      if(!widget.setts.valid) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content:
              Text("Текущие настройки не действительны."),
        ));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: ()async {
        Navigator.of(context).pop(widget.setts);
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text("Настройки устройства"),
        ),
        body: mounted ? Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListView(
            children: [
              // !(widget.setts.valid) ?
              //   Text("Сохраненные настройки не достоверны.\r\nПредставлены настройки по умолчанию.") :
              //   Offstage(),

              PreferenceGroup(
                title: "Адрес устройства"
              ),

              IntPreference(
                title: "№ поезда",
                value: widget.setts.train,
                onNewValue: (newValue) {
                  setState(() {
                    widget.setts.train = newValue;
                  });
                },
              ),

              IntPreference(
                title: "Доп. № поезда",
                value: widget.setts.subTrain,
                onNewValue: (newValue) {
                  setState(() {
                    widget.setts.subTrain = newValue;
                  });
                },
              ),

              IntPreference(
                title: "№ системы",
                value: widget.setts.system,
                onNewValue: (newValue) {
                  setState(() {
                    widget.setts.system = newValue;
                  });
                },
              ),

              PreferenceGroup(title: "Радио"),

              IntPreference(
                title: "Канал",
                value: widget.setts.chan,
                onNewValue: (newValue) {
                  setState(() {
                    widget.setts.chan = newValue;
                  });
                },
              ),

              IntListPreference(
                value: widget.setts.tx_pwr,
                title: "Мощность передатчика",
                entries: const [
                  "+4dBm",
                  "+3dBm",
                  "0dBm",
                  "-4dBm",
                  "-8dBm",
                  "-12dBm",
                  "-16dBm",
                  "-20dBm",
                  "-40dBm"
                ],
                values: const [4, 3, 0, 0xFC, 0xF8, 0xF4, 0xF0, 0xEC, 0xD8],
                onNewValue: (newValue){
                  setState(() {
                    widget.setts.tx_pwr = newValue;
                  });
                },
              ),

              PreferenceGroup(title: "Устройство"),

              IntPreference(
                title: "Макс. индекс устройства",
                value: widget.setts.max_dev_indx,
                onNewValue: (newValue) {
                  setState(() {
                    widget.setts.max_dev_indx = newValue;
                  });
                },
              ),

              IntPreference(
                title: "Индекс",
                value: widget.setts.indx,
                onNewValue: (newValue) {
                  setState(() {
                    widget.setts.indx = newValue;
                  });
                },
              ),

              PreferenceGroup(title: "Параметры передачи"),

              IntPreference(
                title: "Длительность таймслота (мс)",
                value: widget.setts.radio_timeslot_ms,
                onNewValue: (newValue) {
                  setState(() {
                    widget.setts.radio_timeslot_ms = newValue;
                  });
                },
              ),

              IntPreference(
                title: "Период передачи (мкс)",
                value: widget.setts.tx_period_us,
                onNewValue: (newValue) {
                  setState(() {
                    widget.setts.tx_period_us = newValue;
                  });
                },
              ),

              IntPreference(
                title: "Количество повторов",
                value: widget.setts.retries_cnt,
                onNewValue: (newValue) {
                  setState(() {
                    widget.setts.retries_cnt = newValue;
                  });
                },
              ),

              IntPreference(
                title: "Задержка устройства (мс)",
                value: widget.setts.last_ppru_delay_ms,
                onNewValue: (newValue) {
                  setState(() {
                    widget.setts.last_ppru_delay_ms = newValue;
                  });
                },
              ),
            ],
          ),
        ) : Center(child: Text("...")),
      ),
    );
  }
}

class PreferenceGroup extends StatelessWidget {
  final String title;

  const PreferenceGroup({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 32, right: 32, top: 16, bottom: 8),
      child: Text(
        title,
        style: TextStyle(
          fontSize: 12,
          color: Theme.of(context).colorScheme.secondary
        ),
      ),
    );
  }
}

class IntPreference extends StatelessWidget {
  final String title;
  int value;
  final Function(int newValue) onNewValue;
  TextEditingController _valueController = TextEditingController();

  IntPreference({this.title="", this.value=0, required this.onNewValue});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(title),
      subtitle: Text(value.toString()),
      onTap: () {
        _valueController = TextEditingController(text: value.toString());
        showDialog(context: context, builder: (context) => AlertDialog(
          title: Text(title),
          content: TextField(
            controller: _valueController,
            keyboardType: TextInputType.number,
            onChanged: (value) => this.value = int.parse(value),
          ),
          actions: [
            TextButton(onPressed: () {
              onNewValue(value);
              Navigator.of(context).pop();
            }, child: Text("OK")),
            TextButton(onPressed: (){
              Navigator.of(context).pop();
            }, child: Text("Отмена"))
          ],
        ));
      },
    );
  }
}

class IntListPreference extends StatelessWidget {
  final String title;
  final List<int> values;
  final List<String> entries;
  int value;
  final Function(int newValue) onNewValue;
  TextEditingController _valueController = TextEditingController();

  IntListPreference({this.title="", required this.values, required this.entries, this.value=0, required this.onNewValue});

  @override
  Widget build(BuildContext context) {
    String subTitle = "";
    for(int i=0; i<values.length; i++){
      if(values[i] == value){
        subTitle = entries[i];
        break;
      }
    }

    return ListTile(
      title: Text(title),
      subtitle: Text(subTitle),
      onTap: () {
        _valueController = TextEditingController(text: value.toString());
        showDialog(context: context, builder: (context) => AlertDialog(
          title: Text(title),
          content: Container(
            width: 300,
            child: ListView.builder(
              itemCount: entries.length,
              itemBuilder: (context, index) => ListTile(
                title: Text(entries[index]),
                onTap: () {
                  onNewValue(values[index]);
                  Navigator.of(context).pop();
                },
              ),

            ),
          ),
          actions: [
            TextButton(onPressed: (){
              Navigator.of(context).pop();
            }, child: Text("Отмена"))
          ],
        ));
      },
    );
  }
}