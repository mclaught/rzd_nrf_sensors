import 'dart:async';
import 'dart:typed_data';
import 'package:archive/archive_io.dart';
import 'package:rzd_ppru_updater/ble_uart.dart';

class HexConverter {
  final int _hdr_sz = 4;
  late int _mtu;
  int _ext_addr = 0;
  List<int> _buffer = [];
  List<Uint8List> _result = [];
  int _expected_addr = 0;
  int _mem_addr = 0;
  int _mem_len = 0;
  Crc32 _crc32 = Crc32();
  int _crcXor = 0;

  static const CMD_CLEAR = 1;
  static const CMD_READ = 2;
  static const CMD_FLASHTYPE = 3;
  static const CMD_RESET = 4;
  static const CMD_CRC = 5;
  static const CMD_INFO = 6;

  static const TYPE_FIRMWARE   = 0;
  static const TYPE_BOOT       = 1;
  static const TYPE_SD         = 2;
  static const TYPE_RS485      = 3;
  static const TYPE_SETTS      = 4;

  List<Uint8List> get result => _result;

  HexConverter(int mtu){
    int m = 256;
    _mtu = m + _hdr_sz;
    while(_mtu > mtu){
      m >>= 1;
      _mtu = m + _hdr_sz;
    }
  }

  int _byteFrom(String str, int index){
    if((index*2+2) >= str.length){
      throw Exception("Index out of bounds");
    }
    String bStr = str.substring(1 + index*2, 3 + index*2);
    return int.parse("0x"+bStr);
  }
  
  int _shortFrom(str, int index){
    return (_byteFrom(str, index) << 8) + _byteFrom(str, index+1);
  }

  int _longLEFrom(str, index){
    return (_byteFrom(str, index+3) << 24) + (_byteFrom(str, index+2) << 16) + (_byteFrom(str, index+1) << 8) + (_byteFrom(str, index));
  }

  void _add32(int x){
    for(int i=0; i<4; i++){
      _buffer.add(x & 0xFF);
      x >>= 8;
    }
  }

  int _crc(List<int> data){
    int c = 0;
    for(int i=0; i<data.length; i++){
      c = (c ^ data[i]) & 0xFF;
    }
    return c;
  }

  void _flush(){
    _buffer.add(_crc(_buffer));
    _result.add(Uint8List.fromList(_buffer));
    _buffer.clear();
  }

  bool _convertHeader(String str){
    if(str[0] == '#'){
      if(str.startsWith("#TYPE")){
        _mem_len = 0;
        _mem_addr = -1;
        _crc32 = Crc32();
        _crcXor = 0;
        _add32(CMD_CLEAR << 28);
        _flush();

        _add32(CMD_FLASHTYPE << 28);

        if(str.contains(" BOOT")){
          _buffer.add(TYPE_BOOT);
        }else if(str.contains(" SOFTDEVICE")){
          _buffer.add(TYPE_SD);
        }else if(str.contains(" BKT")){
          _buffer.add(TYPE_RS485);
        }else{
          _buffer.add(TYPE_FIRMWARE);
        }

        _flush();
      }
      return true;
    }else{
      return false;
    }
  }

  FWInfo findVersions(List<String> lines){
    FWInfo info = FWInfo();

    for(int i=0; i<lines.length; i++){
      String str = lines[i];
      if(str.isEmpty)
        continue;
      if(str[0] != ':')
        continue;

      int len = _byteFrom(str, 0);
      int addr = _shortFrom(str, 1);
      int type = _byteFrom(str, 3);

      switch(type){
        case 0:
          addr += _ext_addr;
          if(0x19024 >= addr && 0x19024 < (addr+len)){
            info.boot2_ver = _longLEFrom(str, 0x19024-addr+4);
          }
          if(0x1D024 >= addr && 0x1D024 < (addr+len)){
            info.fw_ver = _longLEFrom(str, 0x1D024-addr+4);
          }
          if(0x74024 >= addr && 0x74024 < (addr+len)){
            info.boot_ver = _longLEFrom(str, 0x74024-addr+4);
          }
          break;

        case 2:
          _ext_addr = _shortFrom(str, 4) << 4;
          break;

        case 4:
          _ext_addr = _shortFrom(str, 4) << 16;
          break;
      }

      if(info.fw_ver != 0)
        break;
    }

    return info;
  }

  void _convert(String str){
    print(str);
    if(str.isEmpty){
      return;
    }
    if(_convertHeader(str)) {
      return;
    }
    if(str[0] != ':') {
      throw Exception("Ошибка в файле");
    }

    int bytes = (str.length-1)~/2;
    int crc = 0;
    for(int i=0; i<bytes; i++){
      crc = (crc + _byteFrom(str, i)) & 0xFF;
    }
    if(crc != 0){
      throw Exception("Файл поврежден");
    }

    int len = _byteFrom(str, 0);
    int addr = _shortFrom(str, 1);
    int type = _byteFrom(str, 3);

    switch(type){
      case 0:
        addr += _ext_addr;

        if(addr < 0x80000) {
          if(_mem_addr == -1){
            _mem_addr = addr;
          }

          if((_mtu - _buffer.length) < len || addr != _expected_addr){
            if(_buffer.isNotEmpty) {
              _flush();
            }
          }
          _expected_addr = addr+len;

          if (_buffer.isEmpty) {
            _add32(addr);
          }
          for (int i = 0; i < len; i++) {
            int b = _byteFrom(str, 4 + i);
            _mem_len++;
            _crc32.add([b]);
            _crcXor ^= b;
            _crcXor &= 0xFF;
            _buffer.add(b);
          }
        }else{
          print("UICR ignored");
        }
        break;

      case 1:
        if(_buffer.isNotEmpty) {
          _flush();
        }

        var crc32val = _crc32.hash;
        print("CRC32: $crc32val");

        _add32((CMD_CRC << 28) | _mem_addr);
        _add32(_mem_len);
        // _buffer.add(_mem_crc);
        _add32(crc32val);
        _flush();

        break;

      case 2:
        _ext_addr = _shortFrom(str, 4) << 4;
        break;

      case 4:
        _ext_addr = _shortFrom(str, 4) << 16;
        break;
        
    }

    return;
  }

  void _reset(){
    _result = [];
    _buffer = [];
  }

  Future<List<Uint8List>> convertLines(List<String> lines)async {
    if(lines.isEmpty) {
      throw Exception("Файл пуст");
    }
    if(!lines[0].startsWith("#EXTHEX")) {
      throw Exception("Не поддерживаемый формат");
    }

    _reset();

    for(int i=0; i<lines.length; i++){
      _convert(lines[i]);
    }

    return _result;
  }

  List<Uint8List> makeReadCmd(int addr, int len){
    _reset();
    addr |= (CMD_READ << 28);
    _add32(addr);
    _add32(len);
    _flush();
    return _result;
  }

  List<Uint8List> makeWriteSettsCmd(int addr, List<int> data){
    _reset();
    _add32(CMD_CLEAR << 28);
    _flush();

    _add32(CMD_FLASHTYPE << 28);
    _buffer.add(TYPE_SETTS);
    _flush();

    _add32(addr);
    _buffer.addAll(data);
    _flush();
    return _result;
  }

  List<Uint8List> makeResetCmd() {
    _reset();
    _add32(CMD_RESET << 28);
    _flush();
    return _result;
  }

  List<Uint8List> makeInfoCmd() {
    _reset();
    _add32(CMD_INFO << 28);
    _flush();
    return _result;
  }

  List<String> getFileInfo(List<String> lines){
    List<String> info = [];

    if(lines.isEmpty) {
      throw Exception("Файл пуст");
    }
    if(!lines[0].startsWith("#EXTHEX")) {
      throw Exception("Не поддерживаемый формат");
    }

    for(int i=0; i<lines.length; i++){
      String line = lines[i];

      if(line.startsWith("#TYPE")){
        info.add("");
        if(line.contains(" BOOT")){
          info.add("BLE-Bootloader");
        }else if(line.contains(" SOFTDEVICE")){
          info.add("NRF SoftDevice");
        }else if(line.contains(" BKT")){
          info.add("Прошивка через RS485");
        }else if(line.contains(" FIRMWARE")){
          info.add("Основное ПО");
        }else{
          throw Exception("Неизвестный тип прошивки");
        }
      }

      if(line.startsWith("#VERSION")){
        info.add("Версия файла: ${line.substring(9)}");
      }
    }

    FWInfo ver = findVersions(lines);
    if(ver.bootVerHex() != "...") {
      info.add("Версия (BLE-boot): v.${ver.bootVerHex()}");
    }
    if(ver.boot2VerHex() != "...") {
      info.add("Версия (boot): v.${ver.boot2VerHex()}");
    }
    if(ver.fwVerHex() != "...") {
      info.add("Версия (ПО): v.${ver.fwVerHex()}");
    }

    return info;
  }
}
