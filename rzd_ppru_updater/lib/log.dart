import 'dart:io';
import 'package:intl/intl.dart';
import 'package:rzd_ppru_updater/dev_setts.dart';
import 'package:share_plus/share_plus.dart';

class CsvLog{

  CsvLog(){
  }

  addToLog(String dev, DevSetts devSetts){
    var time = DateTime.now();
    String str = DateFormat("d-M-yyyy H:m:s").format(time)+","+dev+","+devSetts.toCsv();

    String dir = "/sdcard/PPRU_updater/${devSetts.train}-${devSetts.subTrain}";
    String name = "$dir/$dev.csv";
    if(!Directory(dir).existsSync()) {
      Directory(dir).create(recursive: true);
    }
    print(str);

    File file = File(name);
    if(!file.existsSync()){
      file.create();
    }
    file.writeAsString(str+"\r\n", mode: FileMode.append);
  }

  List<String> trainList(){
    Directory dir = Directory("/sdcard/PPRU_updater");
    List<String> list = [];
    for(var e in dir.listSync()){
      list.add(e.path.split("/").last);
    }
    return list;
  }

  List<String> devList(String train){
    Directory dir = Directory("/sdcard/PPRU_updater/$train");
    List<String> list = [];
    for(var e in dir.listSync()){
      var name = e.path.split("/").last;
      if(!name.contains("Состав №"))
        list.add(name);
    }
    return list;
  }

  sendLog(String train, String dev){
    String path = "/sdcard/PPRU_updater/$train/$dev.csv";
    Share.shareFiles([path]);
  }

  sendTrainLog(String train) async {
    var list = devList(train);
    String content = "";
    for(String dev in list){
      File fDev = File("/sdcard/PPRU_updater/$train/$dev");
      content += fDev.readAsStringSync();
    }

    String trainFName = "/sdcard/PPRU_updater/$train/Состав №$train.csv";
    File file = File(trainFName);
    file.writeAsStringSync(content);

    Share.shareFiles([trainFName]);
  }
}