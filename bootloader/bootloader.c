#include "bootloader.h"
#include <string.h>
#include "nrf.h"
#include "nordic_common.h"
#include "app_error.h"
#include "nrf_mbr.h"
#include "nrf_sdm.h"
#include "nrf_fstorage.h"
#include "nrf_fstorage_sd.h"
#include "nrf_fstorage_nvmc.h"
#include "ble_nus.h"
#include "fwupdater.h"
#include "nrf_gpio.h"
#include "app_uart.h"
#include "app_timer.h"
#include "nrf_drv_clock.h"

#define MAX_ADDR    0x7FFFF
#define PG_REC_CNT  ((MAX_ADDR >> 15) + 1)

#define UPDC32(octet, crc) (crc_32_tab[((crc) ^ (octet)) & 0xff] ^ ((crc) >> 8))

uint32_t flash_size = 0;
uint32_t flashed_words = 0;
//int32_t addr_shift = 0;
static uint32_t fw_start_addr = 0x7FFFFFFF;
uint32_t tmp_start_addr = 0;

static uint8_t cleared_pg[PG_REC_CNT];
static uint8_t flash_type = 0;
static firmware_entry_t fw_entry;

static void fstorage_evt_handler(nrf_fstorage_evt_t * p_evt);
NRF_FSTORAGE_DEF(nrf_fstorage_t fstorage) =
{
    /* Set a handler for fstorage events. */
    .evt_handler = fstorage_evt_handler,

    /* These below are the boundaries of the flash space assigned to this instance of fstorage.
     * You must set these manually, even at runtime, before nrf_fstorage_init() is called.
     * The function nrf5_flash_end_addr_get() can be used to retrieve the last address on the
     * last page of flash available to write data. */
    .start_addr = 0x19000,
    .end_addr   = 0x7FFFF,
};

static uint32_t crc_32_tab[] = { /* CRC polynomial 0xedb88320 */
0x00000000, 0x77073096, 0xee0e612c, 0x990951ba, 0x076dc419, 0x706af48f,
0xe963a535, 0x9e6495a3, 0x0edb8832, 0x79dcb8a4, 0xe0d5e91e, 0x97d2d988,
0x09b64c2b, 0x7eb17cbd, 0xe7b82d07, 0x90bf1d91, 0x1db71064, 0x6ab020f2,
0xf3b97148, 0x84be41de, 0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7,
0x136c9856, 0x646ba8c0, 0xfd62f97a, 0x8a65c9ec, 0x14015c4f, 0x63066cd9,
0xfa0f3d63, 0x8d080df5, 0x3b6e20c8, 0x4c69105e, 0xd56041e4, 0xa2677172,
0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b, 0x35b5a8fa, 0x42b2986c,
0xdbbbc9d6, 0xacbcf940, 0x32d86ce3, 0x45df5c75, 0xdcd60dcf, 0xabd13d59,
0x26d930ac, 0x51de003a, 0xc8d75180, 0xbfd06116, 0x21b4f4b5, 0x56b3c423,
0xcfba9599, 0xb8bda50f, 0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924,
0x2f6f7c87, 0x58684c11, 0xc1611dab, 0xb6662d3d, 0x76dc4190, 0x01db7106,
0x98d220bc, 0xefd5102a, 0x71b18589, 0x06b6b51f, 0x9fbfe4a5, 0xe8b8d433,
0x7807c9a2, 0x0f00f934, 0x9609a88e, 0xe10e9818, 0x7f6a0dbb, 0x086d3d2d,
0x91646c97, 0xe6635c01, 0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e,
0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457, 0x65b0d9c6, 0x12b7e950,
0x8bbeb8ea, 0xfcb9887c, 0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65,
0x4db26158, 0x3ab551ce, 0xa3bc0074, 0xd4bb30e2, 0x4adfa541, 0x3dd895d7,
0xa4d1c46d, 0xd3d6f4fb, 0x4369e96a, 0x346ed9fc, 0xad678846, 0xda60b8d0,
0x44042d73, 0x33031de5, 0xaa0a4c5f, 0xdd0d7cc9, 0x5005713c, 0x270241aa,
0xbe0b1010, 0xc90c2086, 0x5768b525, 0x206f85b3, 0xb966d409, 0xce61e49f,
0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4, 0x59b33d17, 0x2eb40d81,
0xb7bd5c3b, 0xc0ba6cad, 0xedb88320, 0x9abfb3b6, 0x03b6e20c, 0x74b1d29a,
0xead54739, 0x9dd277af, 0x04db2615, 0x73dc1683, 0xe3630b12, 0x94643b84,
0x0d6d6a3e, 0x7a6a5aa8, 0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1,
0xf00f9344, 0x8708a3d2, 0x1e01f268, 0x6906c2fe, 0xf762575d, 0x806567cb,
0x196c3671, 0x6e6b06e7, 0xfed41b76, 0x89d32be0, 0x10da7a5a, 0x67dd4acc,
0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5, 0xd6d6a3e8, 0xa1d1937e,
0x38d8c2c4, 0x4fdff252, 0xd1bb67f1, 0xa6bc5767, 0x3fb506dd, 0x48b2364b,
0xd80d2bda, 0xaf0a1b4c, 0x36034af6, 0x41047a60, 0xdf60efc3, 0xa867df55,
0x316e8eef, 0x4669be79, 0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236,
0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f, 0xc5ba3bbe, 0xb2bd0b28,
0x2bb45a92, 0x5cb36a04, 0xc2d7ffa7, 0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d,
0x9b64c2b0, 0xec63f226, 0x756aa39c, 0x026d930a, 0x9c0906a9, 0xeb0e363f,
0x72076785, 0x05005713, 0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38,
0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7, 0x0bdbdf21, 0x86d3d2d4, 0xf1d4e242,
0x68ddb3f8, 0x1fda836e, 0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777,
0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c, 0x8f659eff, 0xf862ae69,
0x616bffd3, 0x166ccf45, 0xa00ae278, 0xd70dd2ee, 0x4e048354, 0x3903b3c2,
0xa7672661, 0xd06016f7, 0x4969474d, 0x3e6e77db, 0xaed16a4a, 0xd9d65adc,
0x40df0b66, 0x37d83bf0, 0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9,
0xbdbdf21c, 0xcabac28a, 0x53b39330, 0x24b4a3a6, 0xbad03605, 0xcdd70693,
0x54de5729, 0x23d967bf, 0xb3667a2e, 0xc4614ab8, 0x5d681b02, 0x2a6f2b94,
0xb40bbe37, 0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d
};

extern void bkt_progress_callback(int progress, int result);

void cleared_pg_reset(void){
    memset(cleared_pg, 0, sizeof(uint8_t)*PG_REC_CNT);
//    sd_flash_protect(0,0,0,0);
    
    #ifndef TEST_APP
    ret_code_t rc = nrf_fstorage_init(&fstorage, &nrf_fstorage_sd, NULL);
    #else
    ret_code_t rc = nrf_fstorage_init(&fstorage, &nrf_fstorage_nvmc, NULL);
    #endif
    APP_ERROR_CHECK(rc);
    
}

static void cleared_pg_set(uint32_t addr){
    if(addr > MAX_ADDR)
        return;
    
    uint32_t pg_num = addr >> 12;
    int n = pg_num >> 3;
    int m = pg_num & 0x07;
    cleared_pg[n] |= (1<<m);
}

static uint8_t cleared_pg_get(uint32_t addr){
    if(addr > MAX_ADDR)
        return 1;
    
    uint32_t pg_num = addr >> 12;
    int n = pg_num >> 3;
    int m = pg_num & 0x07;
    
    return cleared_pg[n] & (1<<m);
}

#ifdef TEST_APP
    cfg_t cfg = {
        .main_addr = {
            .rfu = 0,
            .train = 0x17bd,
            .subTrain = 1
        },
        .chan = 36,
        .radio_timeslot_ms = 5,
        .max_dev_indx = 11,
        .indx = 0,
        .mode = 3,
        .tx_pwr = RADIO_TXPOWER_TXPOWER_0dBm,
        .tx_period_us = 300,
        .retries_cnt = 2,
        .last_ppru_delay_ms = 5
    };
#endif

static void flash_page_erase(uint32_t * page_address)
{
    // Turn on flash erase enable and wait until the NVMC is ready:
    NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Een << NVMC_CONFIG_WEN_Pos);

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }

    // Erase page:
    NRF_NVMC->ERASEPAGE = (uint32_t)page_address;

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }

    // Turn off flash erase enable and wait until the NVMC is ready:
    NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Ren << NVMC_CONFIG_WEN_Pos);

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }
}

static void flash_word_write(uint32_t * address, uint32_t value)
{
    // Turn on flash write enable and wait until the NVMC is ready:
    NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Wen << NVMC_CONFIG_WEN_Pos);

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }

    *address = value;

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }

    // Turn off flash write enable and wait until the NVMC is ready:
    NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Ren << NVMC_CONFIG_WEN_Pos);

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }
}

static void uicr_word_write(int index, uint32_t value)
{
    // Turn on flash write enable and wait until the NVMC is ready:
    NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Wen << NVMC_CONFIG_WEN_Pos);

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }

    NRF_UICR->CUSTOMER[index] = value;

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }

    // Turn off flash write enable and wait until the NVMC is ready:
    NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Ren << NVMC_CONFIG_WEN_Pos);

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }
}

void flash_clear_page(uint32_t* page_address){
    if(cleared_pg_get((uint32_t)page_address))
        return;
    
//    flash_page_erase(page_address);
//    uint32_t page_num = (uint32_t)page_address >> 12;
//    sd_flash_page_erase(page_num);
    
    ret_code_t rc = nrf_fstorage_erase(&fstorage, (uint32_t)page_address, 1, NULL);
    APP_ERROR_CHECK(rc);
    while (nrf_fstorage_is_busy(&fstorage))
    {
        __NOP;
    }
    
    cleared_pg_set((uint32_t)page_address);
}

int32_t flash_write(uint32_t* addr, uint32_t* data, int32_t len)
{
    uint32_t ptr_src;
    uint32_t ptr_dst;
    
    ptr_src = (uint32_t)data;
    ptr_dst = (uint32_t)addr;
    
    uint8_t len_remn = len & 3;
    if(len_remn){
        len = (len & 0xFFFFFFFC) + 4;
    }
    
//    flash_page_erase(addr);
//    for(int i=0; i<70000; i++) (void)i;
//    sd_flash_write(ptr_dst, ptr_src, len);
    
    flash_clear_page((uint32_t*)ptr_dst);
    
    ret_code_t rc = nrf_fstorage_write(&fstorage, ptr_dst, (uint32_t*)ptr_src, len, NULL);
    if(rc != NRF_SUCCESS){
        APP_ERROR_CHECK(rc);
    }
    while (nrf_fstorage_is_busy(&fstorage))
    {
        __NOP;
    }
    
    return len;
}

uint32_t translate_addr(uint32_t addr){
    if(tmp_start_addr == 0x7FFFFFFF){
        tmp_start_addr = addr;
    }
    if(fw_start_addr == 0x7FFFFFFF){
        fw_start_addr = (uint32_t)addr;
    }
    if(flash_type == TYPE_FIRMWARE){
        return addr;
    }
    
    static uint32_t new_addr;
    new_addr = ((int32_t)addr - (int32_t)fw_start_addr + (int32_t)tmp_start_addr);
    return new_addr;
}

void app_init_bootloader(void){
//    if(*(uint32_t*)MBR_PARAM_PAGE_ADDR == 0xFFFFFFFF){
//        flash_word_write((uint32_t*)MBR_PARAM_PAGE_ADDR, MBR_PARAM_PAGE);
//    }
//    if(*(uint32_t*)MBR_BOOTLOADER_ADDR == 0xFFFFFFFF){
//        flash_word_write((uint32_t*)MBR_BOOTLOADER_ADDR, BOOTLOADERADDR);//
//    }
    
    #ifdef TEST_APP
    cleared_pg_reset();
    for(int i=0; i<100000; i++);
    flash_write((uint32_t*)SETTINGS_ADDR, (uint32_t*)&cfg, sizeof(cfg_t));
    
    #endif
}

//void app_run_bootloader(void){
//    if(*(uint32_t*)MBR_PARAM_PAGE_ADDR == 0xFFFFFFFF){
//        flash_word_write((uint32_t*)MBR_PARAM_PAGE_ADDR, MBR_PARAM_PAGE);
//    }
//    
//    sd_mbr_command_t mbr_cmd;
//    mbr_cmd.command = SD_MBR_COMMAND_VECTOR_TABLE_BASE_SET;
//    mbr_cmd.params.base_set.address = BOOTLOADERADDR;
//    sd_mbr_command(&mbr_cmd);

//    NVIC_SystemReset();
//}

static void fstorage_evt_handler(nrf_fstorage_evt_t * p_evt)
{
    if (p_evt->result != NRF_SUCCESS)
    {
        return;
    }

    switch (p_evt->id)
    {
        case NRF_FSTORAGE_EVT_WRITE_RESULT:
        {
        } break;

        case NRF_FSTORAGE_EVT_ERASE_RESULT:
        {
        } break;

        default:
            break;
    }
}


void init_bootloader(void){
    flash_size = NRF_FICR->CODESIZE * NRF_FICR->CODEPAGESIZE;
    
//    if(*(uint32_t*)MBR_PARAM_PAGE_ADDR == 0xFFFFFFFF){
//        flash_word_write((uint32_t*)MBR_PARAM_PAGE_ADDR, MBR_PARAM_PAGE);
//    }
    if(*(uint32_t*)MBR_BOOTLOADER_ADDR == 0xFFFFFFFF){
        flash_word_write((uint32_t*)MBR_BOOTLOADER_ADDR, BOOTLOADERADDR);//
    }
    
    sd_mbr_command_t mbr_cmd;
    mbr_cmd.command = SD_MBR_COMMAND_INIT_SD;
    sd_mbr_command(&mbr_cmd);
    
    sd_softdevice_vector_table_base_set(BOOTLOADERADDR);
    
}

uint8_t calc_crc(uint8_t* data, int len){
    uint8_t crc = 0;
    for(int i=0; i<len; i++){
        crc ^= data[i];
    }
    return crc;
}

uint32_t crc32buf(char *buf, uint32_t len){
      uint32_t oldcrc32 = 0xFFFFFFFF;

      for ( ; len; --len, ++buf)
      {
            oldcrc32 = UPDC32(*buf, oldcrc32);
      }

      return ~oldcrc32;
      
}

void flash_set_type(uint8_t type){
    flash_type = type;
    flashed_words = 0;
    
    switch(type){
        case TYPE_FIRMWARE:
            //addr_shift = 0;
            tmp_start_addr = 0x7FFFFFFF;
            break;
        case TYPE_BOOT:
            //addr_shift = -0xA000;
            tmp_start_addr = NEW_BOOT_ADDR;
            break;
        case TYPE_SD:
            //addr_shift = 0x5B000;
            tmp_start_addr = NEW_SD_ADDR;
            break;
        case TYPE_RS485:   
            //addr_shift = APP_CODE_BASE + FW_MAX_SIZE;            
            tmp_start_addr = NEW_RS485_ADDR;
            break;
        case TYPE_SETTS:   
            //addr_shift = APP_CODE_BASE + FW_MAX_SIZE;            
            tmp_start_addr = SETTINGS_ADDR;
            break;
    }
    
    fw_start_addr = 0x7FFFFFFF;
}

void start_firmware(void){
//    sd_mbr_command_t mbr_cmd;
//    mbr_cmd.command = SD_MBR_COMMAND_INIT_SD;
//    sd_mbr_command(&mbr_cmd);

    app_timer_stop_all();
    app_uart_close();
    sd_softdevice_disable();
    
    nrf_gpio_pin_clear(14);
    nrf_gpio_pin_clear(15);
    
    NRF_RTC1->TASKS_STOP = 1;
    NVIC_DisableIRQ(RTC1_IRQn);
    NVIC_ClearPendingIRQ(RTC1_IRQn);
    
    NVIC_DisableIRQ(SysTick_IRQn);
    NVIC_ClearPendingIRQ(SysTick_IRQn);
    
    NVIC_DisableIRQ(SWI2_IRQn);
    NVIC_ClearPendingIRQ(SWI2_IRQn);
    
    NRF_CLOCK->TASKS_HFCLKSTOP = 1;
    NRF_CLOCK->TASKS_LFCLKSTOP = 1;
    nrf_drv_clock_uninit();
    
    __disable_irq();
    
//    sd_softdevice_vector_table_base_set(APP_CODE_BASE);
    SCB->VTOR = APP_CODE_BASE;
    __set_MSP(*(uint32_t*)(APP_CODE_BASE)-0x10);
    
    fw_entry = (firmware_entry_t)*(uint32_t*)(APP_CODE_BASE+4);
    fw_entry();
}

static sd_mbr_command_t mbr_cmd;
static void copy_bootloader(void){
    mbr_cmd.command = SD_MBR_COMMAND_COPY_BL;
    mbr_cmd.params.copy_bl.bl_src = (uint32_t*)NEW_BOOT_ADDR;//((int32_t)BOOTLOADERADDR + addr_shift);
    mbr_cmd.params.copy_bl.bl_len = flashed_words;
    uint32_t res = sd_mbr_command(&mbr_cmd);
    APP_ERROR_CHECK(res);
    
//    if(res != NRF_SUCCESS){
//        NVIC_SystemReset();
//    }
}

static void copy_softdevice(void){
    mbr_cmd.command = SD_MBR_COMMAND_COPY_SD;
    mbr_cmd.params.copy_sd.src = (uint32_t*)NEW_SD_ADDR;//(addr_shift + MBR_SIZE);
    mbr_cmd.params.copy_sd.dst = (uint32_t*)MBR_SIZE;
    mbr_cmd.params.copy_bl.bl_len = flashed_words - MBR_PAGE_SIZE_IN_WORDS;
    uint32_t res = sd_mbr_command(&mbr_cmd);
    APP_ERROR_CHECK(res);
//    if(res != NRF_SUCCESS){
//        NVIC_SystemReset();
//    }
}

void bootloader_complete(void){
    switch(flash_type){
        case TYPE_FIRMWARE:
            //start_firmware();
            NVIC_SystemReset();
            break;
        case TYPE_BOOT:
            NVIC_SystemReset();
//            copy_bootloader();
            break;
        case TYPE_SD:
            NVIC_SystemReset();
//            copy_softdevice();
            break;
        case TYPE_RS485:
            NVIC_SystemReset();
            break;
    }
}

static pending_copy_rec_t* recs;
static pending_copy_rec_t new_rec;
static void write_pending_copy(uint32_t src, uint32_t dst, uint32_t size){
    recs = (pending_copy_rec_t*)MBR_PARAM_PAGE;
    
    for(int i=0; i<10; i++){
        if(recs[i].src==0xFFFFFFFF && recs[i].dst==0xFFFFFFFF && recs[i].size==0xFFFFFFFF){
            new_rec.src = src;
            new_rec.dst = dst;
            new_rec.size = size;
            new_rec.crc32 = crc32buf((char*)&new_rec, sizeof(uint32_t)*3);
            
            flash_write((uint32_t*)&recs[i], (uint32_t*)&new_rec, sizeof(pending_copy_rec_t));
            
//            flash_write(&recs[i].src, &src, sizeof(uint32_t));
//            flash_write(&recs[i].dst, &dst, sizeof(uint32_t));
//            flash_write(&recs[i].size, &size, sizeof(uint32_t));
//            uint32_t crc = crc32buf((char*)&recs[i].src, sizeof(uint32_t)*3);
//            flash_write(&recs[i].crc32, &crc, sizeof(uint32_t));
            break;
        }
    }
    
//    for(int i=0; i<10; i++){
//        if(NRF_UICR->CUSTOMER[i*3+0]==0xFFFFFFFF && NRF_UICR->CUSTOMER[i*3+1]==0xFFFFFFFF && NRF_UICR->CUSTOMER[i*3+2]==0xFFFFFFFF){
//            uicr_word_write(i*3+0, src);
//            uicr_word_write(i*3+1, dst);
//            uicr_word_write(i*3+2, size);
//            break;
//        }
//    }
}

void pending_copy_for_type(){
    switch(flash_type){
        case TYPE_BOOT:
        case TYPE_SD:
            write_pending_copy(tmp_start_addr, fw_start_addr, flashed_words*sizeof(uint32_t));
            break;
        
        case TYPE_RS485:
            FwUpdaterInit(bkt_progress_callback);
            writeBKT((uint8_t*)NEW_RS485_ADDR, flashed_words*4);
            break;
    }
}