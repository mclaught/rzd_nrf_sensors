#ifndef SYSTEM_TIMER_H
#define SYSTEM_TIMER_H

#include <stdint.h>

typedef uint32_t system_time_t;

extern volatile system_time_t system_timer_value;

extern void          system_timer_init(void);
extern void          system_timer_deinit(void);
extern void          system_timer_init_poweroff(void);
extern system_time_t system_timer_diff(system_time_t old_time);
extern void          system_timer_wait(system_time_t time);

extern void system_delay_ms(uint16_t ms);

#endif /* SYSTEM_TIMER_H */
