#include "system_timer.h"

#include "nrf52.h"
#include "nrf52_bitfields.h"
#include "nrf_drv_clock.h"
#include "nrf_rtc.h"
#include "app_timer.h"

#define LFCLK_FREQUENCY           (32768UL)
#define RTC_FREQUENCY             (1000UL)
#define COMPARE_COUNTERTIME       (5)

#define COUNTER_PRESCALER         ((LFCLK_FREQUENCY/RTC_FREQUENCY) - 1)

APP_TIMER_DEF(system_timer);
volatile system_time_t system_timer_value = 0;

static void system_timer_handler(void *p_context){
    system_timer_value++;
}

system_time_t system_timer_diff(system_time_t old_time)
{
    return system_timer_value - old_time;
}

void system_timer_init(void)
{
    ret_code_t err_code;
    err_code = app_timer_create(&system_timer, APP_TIMER_MODE_REPEATED, system_timer_handler);
    APP_ERROR_CHECK(err_code);
    
    err_code = app_timer_start(system_timer, 16, NULL);
    APP_ERROR_CHECK(err_code);
}

void system_timer_deinit(void)
{
    app_timer_stop(system_timer);
}

void system_timer_wait(system_time_t time)
{
    static system_time_t start;

    start = system_timer_value;
    while (system_timer_value - start < time)
        __WFE();
}

void system_delay_ms(uint16_t ms)
{
    while (ms--)
    {
        volatile uint32_t a;
        for (a = 0; a < 7143; a++)
            (void)a;
    }
}
