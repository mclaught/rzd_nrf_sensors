#include "fwupdater.h"
#include "string.h"
#include "app_uart.h"
#include "system_timer.h"
#include "nrf_gpio.h"
#include "bsp.h"
#include "wdt.h"

uart_packet_t uart_packet = {
    .header = {
        .sign = 0xBBAA
    },
    .type = 17
};

static chunk_info_t chunkInfo;
static firmware_info_t firmware_info;
static bkt_progress_callback_t bkt_progress_callback = NULL;

bool uart_sent_fl = false;

const unsigned short Crc16Table[256] = {
    0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241,
    0xC601, 0x06C0, 0x0780, 0xC741, 0x0500, 0xC5C1, 0xC481, 0x0440,
    0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81, 0x0E40,
    0x0A00, 0xCAC1, 0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841,
    0xD801, 0x18C0, 0x1980, 0xD941, 0x1B00, 0xDBC1, 0xDA81, 0x1A40,
    0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41,
    0x1400, 0xD4C1, 0xD581, 0x1540, 0xD701, 0x17C0, 0x1680, 0xD641,
    0xD201, 0x12C0, 0x1380, 0xD341, 0x1100, 0xD1C1, 0xD081, 0x1040,
    0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240,
    0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0, 0x3480, 0xF441,
    0x3C00, 0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41,
    0xFA01, 0x3AC0, 0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840,
    0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41,
    0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00, 0xEDC1, 0xEC81, 0x2C40,
    0xE401, 0x24C0, 0x2580, 0xE541, 0x2700, 0xE7C1, 0xE681, 0x2640,
    0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041,
    0xA001, 0x60C0, 0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240,
    0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480, 0xA441,
    0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41,
    0xAA01, 0x6AC0, 0x6B80, 0xAB41, 0x6900, 0xA9C1, 0xA881, 0x6840,
    0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41,
    0xBE01, 0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40,
    0xB401, 0x74C0, 0x7580, 0xB541, 0x7700, 0xB7C1, 0xB681, 0x7640,
    0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080, 0xB041,
    0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0, 0x5280, 0x9241,
    0x9601, 0x56C0, 0x5780, 0x9741, 0x5500, 0x95C1, 0x9481, 0x5440,
    0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40,
    0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 0x59C0, 0x5880, 0x9841,
    0x8801, 0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1, 0x8A81, 0x4A40,
    0x4E00, 0x8EC1, 0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41,
    0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 0x8641,
    0x8201, 0x42C0, 0x4380, 0x8341, 0x4100, 0x81C1, 0x8081, 0x4040
};

static char* crc_chck_str = "123456789";
static uint16_t crc_check;

static uint16_t crc16(const unsigned char* data_p, unsigned short length){
    unsigned short crc = 0x0000;

    while (length--)
        crc = (crc >> 8) ^ Crc16Table[(crc & 0xFF) ^ *data_p++];

    return crc;
    
//    unsigned char x;
//    unsigned short crc = 0x0000;

//    while (length--){
//        x = crc >> 8 ^ *data_p++;
//        x ^= x>>4;
//        crc = (crc << 8) ^ ((unsigned short)(x << 12)) ^ ((unsigned short)(x <<5)) ^ ((unsigned short)x);
//    }
//    return crc;
}

void FwUpdaterInit(bkt_progress_callback_t callback)
{
    chunkInfo.len = 0;
    chunkInfo.maxLen = 0;
    chunkInfo.offset = 0;
    bkt_progress_callback = callback;
    
//    crc_check = crc16(crc_chck_str, 9);
}

static uint8_t get_fw_info (uint8_t *out)
{
    *out = GET_FW_INFO;
    return 1;
}

static uint8_t set_fw_boot_mode (uint8_t *out)
{
    *out = FW_BOOT_MODE;
    return 1;
}

static uint8_t set_fw_reset (uint8_t *out)
{
    *out = FW_RESET;
    return 1;
}

static uint8_t set_fw_start (uint8_t *out)
{
    *out = FW_START_MAIN;
    return 1;
}

static uint8_t set_fw_file_info (uint8_t *out, fw_file_info_t *fi)
{
    *out = FW_FILE_INFO;

    (*((fw_file_info_t *)(out + 1))).fCRC32 = fi->fCRC32;
    (*((fw_file_info_t *)(out + 1))).fLen = fi->fLen;
    (*((fw_file_info_t *)(out + 1))).fType = fi->fType;

    return 1 + sizeof(fw_file_info_t);
}

static chunk_hdr_t *chdr;
uint8_t fw_file_info_response (const uint8_t *in)
{
    chdr = (chunk_hdr_t *)(in+1);

    //Устройство отвергает файл прошивки
    if( !chdr->len )
    {return 0;}

    chunkInfo.maxLen = chdr->len;
    chunkInfo.len = chunkInfo.maxLen;
    chunkInfo.offset = chdr->offset;

    //Файл принят
    return 1;
}

static uint8_t create_chunk (const uint8_t *fwFile, uint8_t *out)
{
    *out = FW_GET_FILE;
    (*((chunk_hdr_t *)(out + 1))).len = chunkInfo.len;
    (*((chunk_hdr_t *)(out + 1))).offset = chunkInfo.offset;

    memcpy((out + 1 + sizeof(chunk_hdr_t)), (fwFile + chunkInfo.offset), chunkInfo.len);

    return (chunkInfo.len + sizeof(chunk_hdr_t) + 1);
}

static int create_chunk_response (const uint8_t *in, uint32_t fsize)
{chunk_hdr_t *chdr;

    chdr = (chunk_hdr_t *)(in+1);

    //Некорректный запрос
    if( (!chdr->len && (chdr->offset < fsize)) || (chdr->offset > fsize) || (chdr->len > chunkInfo.maxLen) )
    {return -1;}

    //Загрузка продолжается
    if( (chdr->offset < fsize) && chdr->len )
    {
        chunkInfo.len = chdr->len;
        chunkInfo.offset = chdr->offset;

        return ((chunkInfo.offset * 100UL) / fsize);
    }

    //Загрузка завершена (100%)
    return 100;
}

static uint8_t get_fw_update_status (uint8_t *out)
{
    *out = FW_UPDATE_STATUS;
    return 1;
}

static int update_status_response (const uint8_t *in)
{fw_status_t *status;

    status = (fw_status_t *)(in+1);

    //Обновление успешно
    if( status->err == FW_OK && status->state == FW_FLASHED )
    {return 1;}

    //Ошибка обновления
    return 0;
}

static void app_uart_send(uint8_t* data, int len){
    uint32_t err_code;
    
    for (uint32_t i = 0; i < len; i++)
    {
        do
        {
            uart_sent_fl = false;
            err_code = app_uart_put(data[i]);
            if ((err_code != NRF_SUCCESS) && (err_code != NRF_ERROR_BUSY))
            {
//                NRF_LOG_ERROR("Failed receiving NUS message. Error 0x%x. ", err_code);
                APP_ERROR_CHECK(err_code);
            }
            WDT_Step();
        } while (err_code == NRF_ERROR_BUSY);
    }
    
    uint32_t to = 70000;
    while(!uart_sent_fl && to){WDT_Step(); to--;}
}

static volatile uint8_t answer[MAX_UART_PACKET_LEN];
static volatile int answer_index = 0;
static volatile int answer_cnt = 0;
static volatile bool uart_recv_fl = false;
void uartAddAnswerByte(uint8_t byte){
    answer[answer_index++] = byte;
    uart_packet_t* pack = (uart_packet_t*)answer;
    
    switch(answer_index){
        case 1:
            if(byte != 0xAA){
                answer_index = 0;
            }
            break;
            
        case 2:
            if(byte != 0xBB){
                answer_index = 0;
            }
            break;
            
        case 4:
            if(((uart_packet_t*)answer)->header.len > (MAX_UART_PACKET_LEN-sizeof(uint16_t))){
                answer_index = 0;
            }
            break;
            
        default:
            if(answer_index == UART_PACK_SZ(*pack)){
                if(*(uint16_t*)(&pack->data[pack->header.len-1]) == crc16(&pack->type, pack->header.len)){
                    uart_recv_fl = true;
                }
                answer_cnt = answer_index;
                answer_index = 0;
            }
    }
}

static bool uartSend (uint16_t len, system_time_t timeout){
    #ifdef RS485_DE
    nrf_gpio_pin_set(RS485_DE);
    #endif
    
    for(int i=0; i<100; i++){WDT_Step();}
    
    uart_recv_fl = false;
    answer_index = 0;
    
    uart_packet.header.len = len;
    *(uint16_t*)&uart_packet.data[len-1] = crc16(&uart_packet.type, len);
    
    app_uart_send((uint8_t*)&uart_packet, UART_PACK_SZ(uart_packet));
    
//    for(int i=0; i<10; i++){WDT_Step();}
    
    #ifdef RS485_DE
    nrf_gpio_pin_clear(RS485_DE);
    #endif
    
    if(timeout == 0)
        return true;
    
    system_time_t tm_start = system_timer_value;
    uint32_t loop_cnt = 1000*timeout;
    while(!uart_recv_fl && system_timer_diff(tm_start) < timeout){//loop_cnt
        WDT_Step();
        loop_cnt--;
    }
    return uart_recv_fl;
}

static void callback(int progress, int result){
    if(bkt_progress_callback)
        bkt_progress_callback(progress, result);
}

extern uint32_t crc32buf(char *buf, uint32_t len);

bool writeBKT(uint8_t* data, uint32_t len){
    uart_packet_t* ans = (uart_packet_t*)answer;
    
    callback(0, BKT_PROGRESS);
    
    //Firmware info
    if(!uartSend(1 + get_fw_info(uart_packet.data), 50)){
        callback(0, BKT_ERR_TIMEOUT);
//        return false;
    }
    
    memcpy(&firmware_info, ans->data, sizeof(firmware_info_t));
    firmware_info.version[ans->header.len] = 0;
    
    //Boot mode
    int boot_mode_cnt = 10;
    while(boot_mode_cnt){
        if(uartSend(1 + set_fw_boot_mode(uart_packet.data), 100) && (ans->type & 0x1F)==17){
            memcpy(&firmware_info, ans->data, sizeof(firmware_info_t));
            firmware_info.version[ans->header.len] = 0;
            break;
        }else{
            boot_mode_cnt--;
            for(int i=0; i<100000; i++){WDT_Step();}
        }
    }
    if(boot_mode_cnt == 0){
        callback(0, BKT_ERR_NOBOOT);
        return false;
    }
    
    //Send file info
    fw_file_info_t fi;
    fi.fLen = len;
    fi.fCRC32 = crc32buf((char*)data, len);
    fi.fType = 0;//bin, hex загрузчик не поддерживает
    if(!uartSend(1 + set_fw_file_info(uart_packet.data, &fi), 100)){
        callback(0, BKT_ERR_TIMEOUT);
        return false;
    }
    if( !fw_file_info_response((const uint8_t *)ans->data) )
    {
        callback(0, BKT_ERR_REFUSED);
        return false;
    }
    
    static int currProgress = 0;
    do{
        int rpt = 10;
        while(!uartSend(1 + create_chunk(data, uart_packet.data), 100) && rpt){
            rpt--;
        }
    
        if( (currProgress = create_chunk_response((const uint8_t *)ans->data, len)) < 0 )
        {
            callback(0, BKT_ERR_WRONGREQ);
            return false;
        }
        
        callback(currProgress, 0);
        
    }while(currProgress < 100);
    
    system_time_t tm0 = system_timer_value;
    while(system_timer_diff(tm0)<1500){WDT_Step();}
    
    uint8_t update_ok = false;
    for(int i=0; i<100; i++){
        if(uartSend(1 + get_fw_update_status(uart_packet.data), 200)){
            if( update_status_response((const uint8_t *)ans->data) ){
                update_ok = true;
                break;
            }
        }
    }
    

    if(update_ok){
        uartSend(1 + set_fw_start(uart_packet.data), 0);
        callback(100, BKT_COMPLETE);
    }else{
        callback(0, BKT_ERR_UPDFAIL);
    }
    
    return true;
}