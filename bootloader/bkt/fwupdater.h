#ifndef FWUPDATER_H
#define FWUPDATER_H

#include "types.h"
#include <stdbool.h>

#define MAX_UART_PACKET_LEN 256

#define BKT_PROGRESS 0
#define BKT_COMPLETE 1
#define BKT_ERR_TIMEOUT    -1
#define BKT_ERR_NOBOOT  -2
#define BKT_ERR_REFUSED -3
#define BKT_ERR_WRONGREQ -4
#define BKT_ERR_UPDFAIL -5

typedef __packed struct {
    uint16_t sign;              //сигнатура пакета = 0xAABB
    uint16_t  len;
}uart_header_t;

typedef __packed struct {
    uart_header_t header;
    uint8_t type;
    uint8_t data[MAX_UART_PACKET_LEN];
}uart_packet_t;
#define UART_PACK_SZ(pack) (sizeof(uart_header_t) + sizeof(uint8_t)*2 + (pack).header.len)
    
typedef __packed struct {
    char code;
    char UID[12];
    uint32_t max_size;
    char version[200];
}firmware_info_t;

typedef void (*bkt_progress_callback_t)(int progress, int result);

void FwUpdaterInit(bkt_progress_callback_t callback);
void uartAddAnswerByte(uint8_t byte);
bool writeBKT(uint8_t* data, uint32_t len);
//uint8_t get_fw_info (uint8_t *out);
//uint8_t set_fw_boot_mode (uint8_t *out);
//uint8_t set_fw_reset (uint8_t *out);
//uint8_t set_fw_start (uint8_t *out);
//uint8_t set_fw_file_info (uint8_t *out, fw_file_info_t *fi);
//uint8_t fw_file_info_response (const uint8_t *in);
//uint8_t create_chunk (const uint8_t *fwFile, uint8_t *out);
//int create_chunk_response (const uint8_t *in, uint32_t fsize);
//uint8_t get_fw_update_status (uint8_t *out);
//int update_status_response (const uint8_t *in);


#endif // FWUPDATER_H
