#ifndef TYPES_H
#define TYPES_H

#include "stdint.h"

typedef enum {
    FW_OK = 0, FW_FILE_TYPE_ERR, FW_FILE_SIZE_ERR, FW_FILE_CRC_ERR
}fw_err_t;

typedef enum {
  FW_WAITING = 0, FW_LOADING, FW_LOADED, FW_FLASHING, FW_FLASHED
}fw_state_t;

typedef enum {
  _FILE_BIN = 0, _FILE_HEX
}fw_file_t;

typedef enum {
  GET_FW_INFO = 0, FW_BOOT_MODE, FW_FILE_INFO, FW_GET_FILE, FW_UPDATE_STATUS, FW_START_MAIN, FW_RESET
}fw_t;

typedef __packed struct
{
  uint8_t state;
  uint8_t err;
}fw_status_t;

typedef __packed struct
{
  uint32_t offset;
  uint32_t len;
}chunk_hdr_t;

typedef __packed struct
{
    uint32_t maxLen;
    uint32_t len;
    uint32_t offset;
}chunk_info_t;

typedef __packed struct
{
  uint16_t syn;
  uint16_t len;
}u485_hdr_t;


typedef __packed struct
{
  uint8_t request:      5;//тип запроса (radio_pack_type_t)
  uint8_t func_en:      1;//bkt_func_bit_t
  uint8_t _485_answer:  1;//485_answ_bit_t
  uint8_t tx_data_mode: 1;//data_mode_t
}req_t;

typedef __packed struct
{
  const uint8_t fw_id;
  const char date[12];
  const char time[9];
}fw_release_info_t;

typedef __packed struct
{
  uint32_t fLen;
  uint32_t fCRC32;
  uint8_t fType;
}fw_file_info_t;

#endif // TYPES_H
