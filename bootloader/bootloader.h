#ifndef BOOTLOADER_H
#define BOOTLOADER_H

#include <stdint.h>
#include "nrf52.h"

extern uint32_t flash_size;

//#define FLASH_SIZE               (flash_size)
#define SETTINGS_ADDR            (FLASH_SIZE - 0x1000)
#define MBR_PARAM_PAGE           (SETTINGS_ADDR - 0x1000)
#define BOOTLOADERADDR           (MBR_PARAM_PAGE - 0xA000)
#define NEW_BOOT_ADDR			 (BOOTLOADERADDR - 0xA000)
#define NEW_SD_ADDR			     (BOOTLOADERADDR - 0x19000)
#define NEW_RS485_ADDR           (BOOTLOADERADDR - 0x19000)

#define APP_CODE_BASE            (0x19000)
#define APP_CODE_MAIN            (0x1D000)
#define BOOTLOADR_TIMEOUT        (16384*2)   //2000 ms
#define DISCONNECT_RESET_TIMEOUT (16384*15)  //15 sec
#define RESET_TIMEOUT            (16384/10)    //100 ms
#define BOOT_VERSION			 (*(uint32_t*)(BOOTLOADERADDR + 0x24))
#define NEW_BOOT_VERSION		 (*(uint32_t*)(NEW_BOOT_ADDR + 0x24))
#define FW_MAX_SIZE              (0x10000)

void app_init_bootloader(void);
void app_run_bootloader(void);

void init_bootloader(void);

#define CMD_WRITE       0
#define CMD_CLEAR       1
#define CMD_READ        2
#define CMD_FLASHTYPE   3
#define CMD_RESET       4
#define CMD_CRC         5
#define CMD_INFO        6

#define ANS_OK           0
#define ANS_READDATA     1
#define ERR_CRC          2
#define ERR_BADREQ       3
#define ERR_MEMCRC       4
#define ANS_INFO         5
#define ANS_BKT_PROGRESS 6

#define TYPE_FIRMWARE   0
#define TYPE_BOOT       1
#define TYPE_SD         2
#define TYPE_RS485      3
#define TYPE_SETTS      4

typedef void (*firmware_entry_t)(void);

typedef struct {
    uint32_t src;
    uint32_t dst;
    uint32_t size;
    uint32_t crc32;   
}pending_copy_rec_t;

void ble_cmd(uint8_t* data, int len);
uint8_t calc_crc(uint8_t* data, int len);
uint32_t crc32buf(char *buf, uint32_t len);
void cleared_pg_reset(void);
void flash_set_type(uint8_t type);
void start_firmware(void);
void bootloader_complete(void);
void flash_clear_page(uint32_t* page_address);
int32_t flash_write(uint32_t* addr, uint32_t* data, int32_t len);
uint32_t translate_addr(uint32_t addr);
void pending_copy_for_type();

#ifdef TEST_APP
typedef struct
{
  uint32_t rfu:         8;//не исрользуется (0)
  uint32_t train:       14;//номер электрички (рейса)
  uint32_t subTrain:    2;//для разделения электричек одного маршрута 
  uint32_t system:      8;//номер системы (0x24)
}acc_addr_t;
typedef struct
{
  acc_addr_t main_addr;//access адрес Нордика
  uint32_t tx_period_us;//пауза в микросекундах до следующего ретрансмита
  uint8_t retries_cnt;//кол-во ретрансмитов
  uint8_t max_dev_indx;//максимальный индекс устройства (Кол-во устройств - 1)
  uint8_t radio_timeslot_ms;//величина таймслота ретранслятора
  uint8_t mode;//0x03 не настраивается пользователем
  uint8_t chan;//номер канала
  uint8_t indx;//индекс
  uint8_t tx_pwr;//мощность (записывается прямо в регистр, поэтому нужно преобразование, напр. -20дБм->0xEC, и т.д.) 
  uint8_t last_ppru_delay_ms;//время на ответ для последнего ППРУ
}cfg_t;
#endif

#endif
